module "alb" {
  source = "../AWS/load-balancer"

  business_unit = var.business_unit
  environment   = var.environment

  name   = ""
  scheme = "public"
  type   = "ALB"

  create_lb          = true
  internal           = false
  load_balancer_type = "application"
  security_groups    = [module.security-grp.security_group_id]
  subnets            = module.logicloud-practice-vpc.private_subnets
  tags               = local.tags

  create_http_listener = true
  http_tcp_listeners = [
    {
      port              = 80
      protocol          = "HTTP"
      load_balancer_arn = module.alb.lb_arn
      action_type       = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    },
  ]


  create_https_listener = true
  https_listeners = [
    {
      port            = 443
      protocol        = "HTTPS"
      certificate_arn = var.cert_arn
    }
  ]

  target_group_arn = join("", module.alb.target_group_arns)

  create_tg         = true
  target_group_tags = local.tags
  target_groups = [
    {
      name             = "tf-${var.name}-tg"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = var.target_type
      vpc_id           = module.logicloud-practice-vpc.vpc_id
    },
  ]
  vpc_id = module.logicloud-practice-vpc.vpc_id
}