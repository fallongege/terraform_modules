module "security-grp" {
  source = "../AWS/security-group"

  business_unit = var.business_unit
  environment   = var.environment

  name = "${var.name}-sg"

  create_sg   = true
  description = "${var.name}-sg"
  vpc_id      = module.logicloud-practice-vpc.vpc_id
  tags        = local.tags

  create_ingress_with_cidr_blocks = true
  ingress_with_cidr_blocks = [
    {
      rule        = "ssh from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
    },
    {
      rule        = "http from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
    },
    {
      rule        = "https from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
    },
    {
      rule        = "mysql from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
    }
  ]
  create_egress_with_cidr_blocks = true
  egress_with_cidr_blocks = [
    {
      rule        = "All IPv4 traffic"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "All IPv4 traffic"
    }
  ]
}
