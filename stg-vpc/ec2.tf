module "ec2_intance" {
  source = "../AWS/ec2"

  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  name                   = "tf-${var.name}-instance"
  subnet_id              = module.logicloud-practice-vpc.public_subnets[0]
  tags                   = local.tags
  vpc_security_group_ids = [module.security-grp.security_group_id]
}
