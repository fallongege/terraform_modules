# Create a Database Subnet group; a collection of subnets that you create in a VPC and that you then designate for your DB instances.
resource "aws_db_subnet_group" "database-subnt-grp" {
  name       = "tf-${var.name}-subnet-group"
  subnet_ids = [module.logicloud-practice-vpc.private_subnets[0], module.logicloud-practice-vpc.private_subnets[1]]
  tags = local.tags
}

# Create RDS Instance
resource "aws_db_instance" "databaseinstance" {
  allocated_storage      = 10
  storage_type         = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  name                   = "tflogiclouddb"
  username               = var.db_username
  password               = var.db_password
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.database-subnt-grp.name
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = [module.security-grp.security_group_id]
}
