##################
# Global variables
##################
variable "business_unit" {
  default = "logicloud"
}
variable "environment" {
  default = "dev"
}

##################
# VPC variables
##################

variable "name" {
  description = "The name of the stack"
  type        = string
  default     = "logicloud-stg"
}

variable "cidr_block" {
  default = "10.2.0.0/16"
}


variable "az_count" {
  type    = number
  default = 2
}

variable "private_subnets" {
  
  default = ["10.2.1.0/24", "10.2.2.0/24"]
}

variable "public_subnets" {

   default = ["10.2.3.0/24", "10.2.4.0/24"]
}

variable "create_key_pair" {
  default = false
}

##################
# EC2 variables
##################
variable "ami" {
  default = "ami-08e2d37b6a0129927"
}

variable "key_name" {
  default = "kingsley"
}

variable "instance_type" {
  default = "t2.micro"
}

##################
# DB variables
##################

variable "db_username" {
  default = "admin"
}

variable "db_password" {
  default = "logicloud123"
}

################
# ALB variables
################
variable "cert_arn" {
  type        = string
  default     = "arn:aws:acm:us-west-2:939919218449:certificate/754e0683-ca62-4375-b523-82c16e97f7a2"
}

variable "target_type" {
  type        = string
  default     = "ip"
}

variable "port" {
  type        = number
  default     = 80
}