##############
# VPC output
##############

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.logicloud-practice-vpc.vpc_id
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = module.logicloud-practice-vpc.igw_id
}

output "private_subnets_id" {
  description = "List of IDs of private subnets"
  value       = module.logicloud-practice-vpc.private_subnets
}

output "public_subnets_id" {
  description = "List of IDs of public subnets"
  value       = module.logicloud-practice-vpc.public_subnets
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = module.logicloud-practice-vpc.public_route_table_ids
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = module.logicloud-practice-vpc.private_route_table_ids
}
##############
# SG output
##############

output "security_group_id" {
  description = "The ID of the security group"
  value       = module.security-grp.security_group_id
}

##############
# EC2 output
##############
output "instance_arn" {
  description = "Arn of the new ec2 instance"
  value       = module.ec2_intance.instance_arn
}

output "instance_id" {
  description = "ID of the ec2 instance"
  value       = module.ec2_intance.instance_id
}
output "private_ip" {
  description = "Private ip of the ec2 instance"
  value       = module.ec2_intance.instance_private_ip
}

##############
# DB output
##############
output "subnet_grp_id" {
  description = "Name of subnet group"
  value       = aws_db_subnet_group.database-subnt-grp.name
}

############################
# alb output
###########################
output "alb_arn" {
  description = "Arn of the application load balancer"
  value       = module.alb.lb_arn
}