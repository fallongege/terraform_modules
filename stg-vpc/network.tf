module "logicloud-practice-vpc" {
  source = "../AWS/vpc"

  environment       = var.environment
  business_unit     = var.business_unit

  az_count          = var.az_count
  cidr_block        = var.cidr_block
  name              = "tf-${var.business_unit}-${var.environment}"
  create_internal_dns_zone = false
  enable_s3_endpoint       = false
  private_subnets   = var.private_subnets
  public_subnets    = var.public_subnets
  create_key_pair   = var.create_key_pair

  tags = local.tags
}
  