locals {
  tags = {
    Brand       = "Logicloud"
    Environment = "stg"
    Managed     = "Terraform"
    Team        = "Devops"
  }
}
