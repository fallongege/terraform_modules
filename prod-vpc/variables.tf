##################
# Global variables
##################
variable "business_unit" {
  default = "logicloud"
}
variable "environment" {
  default = "logicloud-prod"
}

##################
# VPC variables
##################

variable "name" {
  description = "The name of the stack"
  type        = string
  default     = "logicloud-stg"
}

variable "cidr_block" {
  default = "10.20.0.0/16"
}


variable "az_count" {
  type    = number
  default = 2
}

variable "private_subnets" {
  
  default = ["10.20.1.0/24", "10.20.2.0/24"]
}

variable "public_subnets" {

   default = ["10.20.3.0/24", "10.20.4.0/24"]
}

variable "create_key_pair" {
  default = false
}