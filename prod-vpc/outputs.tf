##############
# VPC output
##############

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.logicloud-practice-vpc.vpc_id
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = module.logicloud-practice-vpc.igw_id
}

output "private_subnets_id" {
  description = "List of IDs of private subnets"
  value       = module.logicloud-practice-vpc.private_subnets
}

output "public_subnets_id" {
  description = "List of IDs of public subnets"
  value       = module.logicloud-practice-vpc.public_subnets
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = module.logicloud-practice-vpc.public_route_table_ids
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = module.logicloud-practice-vpc.private_route_table_ids
}
