locals {
  tags = {
    Brand       = "Logicloud"
    Environment = "prod"
    Managed     = "Terraform"
    Team        = "Devops"
  }
}