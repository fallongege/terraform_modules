module "logicloud-practice-ecs-cluster" {
  source = "../AWS/ecs-cluster"

  aws_region    = var.aws_region
  business_unit = var.business_unit
  environment   = var.environment

  cluster_tags = {
    Brand       = "Logicloud"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "Devops"
  }
  ecs_cluster_name = var.ecs_cluster_name
}
  