##################
# Global variables
##################
variable "business_unit" {
  default = "logicloud"
}

variable "environment" {
  default = "dev"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "aws_account_id" {
  default = "939919218449"
}

##################
# ECS variables
##################

variable "ecs_cluster_name" {
  description = "The name of the ECS cluster"
  type        = string
  default     = "practice"
}
 #################
 # ACM variables
 #################
 variable "domain_name" {
  description = "A domain name for which the certificate should be issued"
  type        = string
  default     = "stg.logicloudtechnologies.com"
}

variable "zone_id" {
  description = "The ID of the hosted zone to contain this record."
  type        = string
  default     = "Z05337572BKDOTAVZBHHO"
}

