module "logicloud_acm" {
  source = "../AWS/acm"

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  business_unit  = var.business_unit
  environment    = var.environment

  domain_name               = var.domain_name
  zone_id                   = var.zone_id
  subject_alternative_names = ["*.${var.domain_name}"]
  wait_for_validation       = true
  tags = {
    Name        = var.domain_name
    Brand       = "Logicloud"
    Environment = "stg"
    Managed     = "Terraform"
    Team        = "Devops"
  }
}
