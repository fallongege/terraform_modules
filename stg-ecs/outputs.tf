#####################
# ECS Cluster outputs
####################

output "ecs_cluster_arn" {
  description = "The ARN of the ECS cluster"
  value       = module.logicloud-practice-ecs-cluster.ecs_cluster_arn
}

output "ecs_cluster_id" {
  description = "The ID of the ECS cluster"
  value       = module.logicloud-practice-ecs-cluster.ecs_cluster_id
}
#######################
# Rote53 zone outputs
######################
output "hosted_zone_id" {
  value = aws_route53_zone.stg-logicloudtechnologies-com.id
}

output "hosted_zone_name" {
  value = aws_route53_zone.stg-logicloudtechnologies-com.name
}
