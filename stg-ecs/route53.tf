resource "aws_route53_zone" "stg-logicloudtechnologies-com" {  
name = "stg.logicloudtechnologies.com"
 
 tags = {
    Brand       = "Logicloud"
    Environment = "stg"
    Managed     = "Terraform"
    Team        = "Devops"
    Type        = "Public"
  }
}

resource "aws_route53_zone" "stg-logicloud-internal" {  
name = "stg.logicloudtechnologies.internal"
vpc {
    vpc_id = data.aws_vpc.logicloud-dev-vpc.id
}
 
 tags = {
    Brand       = "Logicloud"
    Environment = "stg"
    Managed     = "Terraform"
    Team        = "Devops"
    Type        = "Private"
  }
}

