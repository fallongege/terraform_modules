data "aws_vpc" "logicloud-dev-vpc" {
  filter {
    name = "tag:Name"
    values = ["tf-logicloud-dev-vpc"]
  }
}
