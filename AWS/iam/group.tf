################
# Default Groups
################
resource "aws_iam_group" "default-iam-group" {
  count = var.create_default_groups ? length(var.default_group) : 0

  name = lookup(var.default_group[count.index], "name")
}

resource "aws_iam_group_membership" "default-iam-group-membership" {
  count = var.create_default_groups ? length(var.default_group) : 0

  group = lookup(var.default_group[count.index], "name")
  name  = lookup(var.default_group[count.index], "name")
  users = split(",", lookup(var.default_group[count.index], "users"))

  depends_on = [aws_iam_group.default-iam-group]
}

resource "aws_iam_group_policy_attachment" "force-mfa-iam-policy-attachment" {
  count = var.create_default_groups ? length(var.default_group) : 0

  group      = lookup(var.default_group[count.index], "name")
  policy_arn = lookup(var.default_group[count.index], "policy_arn")

  depends_on = [aws_iam_group.default-iam-group]
}

################
# IAM Group
################
resource "aws_iam_group" "iam-group" {
  count = var.create_group ? 1 : 0

  name = var.iam_group_name
}

resource "aws_iam_group_membership" "iam-group-membership" {
  count = var.create_group ? 1 : 0

  group = aws_iam_group.iam-group[0].name
  name  = var.iam_group_name
  users = var.group_users
}

################################
# IAM group policy attachements
################################
resource "aws_iam_group_policy_attachment" "iam-policy-attachment-custom-arns" {
  count = length(var.group_policy_arns)

  group      = aws_iam_group.iam-group[0].name
  policy_arn = element(var.group_policy_arns, count.index)
}

resource "aws_iam_group_policy_attachment" "iam-policy-attachment" {
  count = length(var.custom_group_policies) > 0 ? length(var.custom_group_policies) : 0

  group      = aws_iam_group.iam-group[0].name
  policy_arn = element(aws_iam_policy.custom-group-policy.*.arn, count.index)
}

###############
# IAM policies
###############
resource "aws_iam_policy" "custom-group-policy" {
  count = length(var.custom_group_policies) > 0 ? length(var.custom_group_policies) : 0

  name        = var.custom_group_policies[count.index]["name"]
  policy      = var.custom_group_policies[count.index]["policy"]
  description = lookup(var.custom_group_policies[count.index], "description", null)
}
