resource "aws_iam_user" "iam-user" {
  count = var.create_user ? length(var.iam_user) : 0

  name          = lookup(var.iam_user[count.index], "user_name")
  path          = var.path
  force_destroy = var.force_destroy
}

resource "aws_iam_user_login_profile" "iam-user-login-profile" {
  count = var.create_user && var.create_iam_user_login_profile ? length(var.iam_user) : 0

  password_length = var.minimum_password_length
  pgp_key         = lookup(var.iam_user[count.index], "pgp_key")
  user            = lookup(var.iam_user[count.index], "user_name")
}

resource "aws_iam_access_key" "iam-user-access-key" {
  count = var.create_user && var.create_iam_access_key ? length(var.iam_user) : 0

  user    = aws_iam_user.iam-user[count.index].name
  pgp_key = lookup(var.iam_user[count.index], "pgp_key")
}

resource "aws_iam_user_ssh_key" "iam-user-ssh-key" {
  count = var.create_user && var.upload_iam_user_ssh_key ? length(var.iam_user) : 0

  username   = aws_iam_user.iam-user[count.index].name
  encoding   = var.ssh_key_encoding
  public_key = lookup(var.iam_user[count.index], "public_key")
}
