################
# Global variables
################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Account variables
################

variable "create_account_password_policy" {
  description = "Whether to create AWS IAM account password policy"
  type        = bool
  default     = false
}

variable "max_password_age" {
  description = "The number of days that an user password is valid"
  type        = number
  default     = 365
}

variable "minimum_password_length" {
  description = "Minimum length to require for user passwords"
  type        = number
  default     = 10
}

variable "allow_users_to_change_password" {
  description = "Whether to allow users to change their own password"
  type        = bool
  default     = true
}

variable "hard_expiry" {
  description = "Whether users are prevented from setting a new password after their password has expired (i.e. require administrator reset)"
  type        = bool
  default     = false
}

variable "password_reuse_prevention" {
  description = "The number of previous passwords that users are prevented from reusing"
  type        = number
  default     = 4
}

variable "require_lowercase_characters" {
  description = "Whether to require lowercase characters for user passwords"
  type        = bool
  default     = true
}

variable "require_uppercase_characters" {
  description = "Whether to require uppercase characters for user passwords"
  type        = bool
  default     = true
}

variable "require_numbers" {
  description = "Whether to require numbers for user passwords"
  type        = bool
  default     = true
}

variable "require_symbols" {
  description = "Whether to require symbols for user passwords"
  type        = bool
  default     = true
}

################
# Default Group variables
################

variable "create_default_groups" {
  description = "Whether the default IAM groups should be created"
  type        = bool
  default     = false
}

variable "default_group" {
  description = "List of IAM group settings including name, users, policy_arn"
  type        = list(map(string))
  default     = []
}

################
# Group variables
################

variable "create_group" {
  description = "Whether an IAM group should be created"
  type        = bool
  default     = false
}

variable "group_users" {
  description = "List of IAM user names in an IAM group"
  type        = list(string)
  default     = []
}

variable "iam_group_name" {
  description = "The name of the IAM group"
  type        = string
  default     = "IAM-Group"
}

variable "custom_group_policies" {
  description = "List of maps of inline IAM policies to attach to the IAM group. Should have `name` and `policy` keys in each element"
  type        = list(map(string))
  default     = []
}

variable "group_policy_arns" {
  description = "List of maps of inline IAM policies to attach to the IAM group that already exist. Should have `name` and `policy` keys in each element"
  type        = list(string)
  default     = []
}

################
# Policy variables
################

variable "create_iam_policy" {
  description = "Whether the IAM policy should be created"
  type        = bool
  default     = false
}

variable "description" {
  description = "The description of the policy"
  type        = string
  default     = ""
}

variable "path" {
  description = "The path of the policy in IAM"
  type        = string
  default     = "/"
}

variable "policy" {
  description = "The rendered IAM policy"
  default     = ""
}

variable "policy_name" {
  description = "The name of the policy"
  type        = string
  default     = ""
}

################
# Role variables
################

variable "create_role" {
  description = "Whether to create an IAM role"
  type        = bool
  default     = false
}

variable "assume_role_policy" {
  description = "The policy that grants an entity permission to assume the role"
  type        = string
  default     = ""
}

variable "create_instance_profile" {
  description = "Whether to create an instance profile"
  type        = bool
  default     = false
}

variable "custom_role_policy_arns" {
  description = "List of ARNs of IAM policies to attach to IAM role"
  type        = list(string)
  default     = []
}

variable "role_description" {
  description = "The IAM Role description"
  type        = string
  default     = ""
}

variable "role_name" {
  description = "The IAM role name"
  type        = string
  default     = ""
}

variable "force_detach_policies" {
  description = "Whether policies should be detached from this role when destroying"
  type        = bool
  default     = false
}

variable "max_session_duration" {
  description = "Maximum CLI/API session duration in seconds between 3600 and 43200"
  type        = number
  default     = 3600
}

variable "role_path" {
  description = "The path of the IAM role"
  type        = string
  default     = "/"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

################
# User variables
################

variable "create_user" {
  description = "Whether an IAM user should be created"
  type        = bool
  default     = false
}

variable "create_iam_access_key" {
  description = "Whether to create IAM access key"
  type        = bool
  default     = false
}

variable "create_iam_user_login_profile" {
  description = "Whether to create IAM user login profile"
  type        = bool
  default     = false
}

variable "force_destroy" {
  description = "When destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices. Without force_destroy a user with non-Terraform-managed access keys and login profile will fail to be destroyed."
  type        = bool
  default     = false
}

variable "iam_user" {
  description = "List of IAM user settings including user_name, pgp_key, public_key"
  type        = list(map(string))
  default     = []
}

variable "upload_iam_user_ssh_key" {
  description = "Whether to upload a public ssh key to the IAM user"
  type        = bool
  default     = false
}

variable "ssh_key_encoding" {
  description = "Specifies the public key encoding format to use in the response. To retrieve the public key in ssh-rsa format, use SSH. To retrieve the public key in PEM format, use PEM"
  type        = string
  default     = "SSH"
}
