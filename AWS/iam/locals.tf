locals {
  policy_name = "tf-${var.business_unit}-${var.environment}-${var.policy_name}-policy"
  role_name   = "tf-${var.business_unit}-${var.environment}-${var.role_name}-role"
}
