resource "aws_iam_policy" "iam-policy" {
  count = var.create_iam_policy ? 1 : 0

  name = local.policy_name

  description = var.description
  path        = var.path
  policy      = var.policy
}
