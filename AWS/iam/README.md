## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### allow\_users\_to\_change\_password

Description: Whether to allow users to change their own password

Type: `bool`

Default: `true`

### assume\_role\_policy

Description: The policy that grants an entity permission to assume the role

Type: `string`

Default: `""`

### create\_account\_password\_policy

Description: Whether to create AWS IAM account password policy

Type: `bool`

Default: `false`

### create\_default\_groups

Description: Whether the default IAM groups should be created

Type: `bool`

Default: `false`

### create\_group

Description: Whether an IAM group should be created

Type: `bool`

Default: `false`

### create\_iam\_access\_key

Description: Whether to create IAM access key

Type: `bool`

Default: `false`

### create\_iam\_policy

Description: Whether the IAM policy should be created

Type: `bool`

Default: `false`

### create\_iam\_user\_login\_profile

Description: Whether to create IAM user login profile

Type: `bool`

Default: `false`

### create\_instance\_profile

Description: Whether to create an instance profile

Type: `bool`

Default: `false`

### create\_role

Description: Whether to create an IAM role

Type: `bool`

Default: `false`

### create\_user

Description: Whether an IAM user should be created

Type: `bool`

Default: `false`

### custom\_group\_policies

Description: List of maps of inline IAM policies to attach to the IAM group. Should have `name` and `policy` keys in each element

Type: `list(map(string))`

Default: `[]`

### custom\_role\_policy\_arns

Description: List of ARNs of IAM policies to attach to IAM role

Type: `list(string)`

Default: `[]`

### default\_group

Description: List of IAM group settings including name, users, policy\_arn

Type: `list(map(string))`

Default: `[]`

### description

Description: The description of the policy

Type: `string`

Default: `""`

### force\_destroy

Description: When destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices. Without force\_destroy a user with non-Terraform-managed access keys and login profile will fail to be destroyed.

Type: `bool`

Default: `false`

### force\_detach\_policies

Description: Whether policies should be detached from this role when destroying

Type: `bool`

Default: `false`

### group\_users

Description: List of IAM user names in an IAM group

Type: `list(string)`

Default: `[]`

### hard\_expiry

Description: Whether users are prevented from setting a new password after their password has expired (i.e. require administrator reset)

Type: `bool`

Default: `false`

### iam\_group\_name

Description: The name of the IAM group

Type: `string`

Default: `"IAM-Group"`

### iam\_user

Description: List of IAM user settings including user\_name, pgp\_key, public\_key

Type: `list(map(string))`

Default: `[]`

### max\_password\_age

Description: The number of days that an user password is valid

Type: `number`

Default: `365`

### max\_session\_duration

Description: Maximum CLI/API session duration in seconds between 3600 and 43200

Type: `number`

Default: `3600`

### minimum\_password\_length

Description: Minimum length to require for user passwords

Type: `number`

Default: `10`

### password\_reuse\_prevention

Description: The number of previous passwords that users are prevented from reusing

Type: `number`

Default: `4`

### path

Description: The path of the policy in IAM

Type: `string`

Default: `"/"`

### policy

Description: The rendered IAM policy

Type: `string`

Default: `""`

### policy\_name

Description: The name of the policy

Type: `string`

Default: `""`

### require\_lowercase\_characters

Description: Whether to require lowercase characters for user passwords

Type: `bool`

Default: `true`

### require\_numbers

Description: Whether to require numbers for user passwords

Type: `bool`

Default: `true`

### require\_symbols

Description: Whether to require symbols for user passwords

Type: `bool`

Default: `true`

### require\_uppercase\_characters

Description: Whether to require uppercase characters for user passwords

Type: `bool`

Default: `true`

### role\_description

Description: The IAM Role description

Type: `string`

Default: `""`

### role\_name

Description: The IAM role name

Type: `string`

Default: `""`

### role\_path

Description: The path of the IAM role

Type: `string`

Default: `"/"`

### ssh\_key\_encoding

Description: Specifies the public key encoding format to use in the response. To retrieve the public key in ssh-rsa format, use SSH. To retrieve the public key in PEM format, use PEM

Type: `string`

Default: `"SSH"`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

### upload\_iam\_user\_ssh\_key

Description: Whether to upload a public ssh key to the IAM user

Type: `bool`

Default: `true`

## Outputs

The following outputs are exported:

### default\_group\_name

Description: The names of the default IAM groups

### default\_group\_users

Description: List of IAM users in the default IAM groups

### group\_name

Description: The name of the IAM group

### group\_users

Description: List of IAM users in IAM group

### iam\_access\_key\_encrypted\_secret

Description: The encrypted secret, base64 encoded

### iam\_access\_key\_id

Description: The access key ID of the IAM user(s)

### iam\_access\_key\_key\_fingerprint

Description: The fingerprint of the PGP key used to encrypt the secret

### iam\_access\_key\_secret

Description: The access key secret of the IAM user(s)

### iam\_instance\_profile\_arn

Description: The ARN of the IAM instance profile

### iam\_instance\_profile\_name

Description: The name of the IAM instance profile

### iam\_instance\_profile\_path

Description: The path of IAM instance profile

### iam\_role\_arn

Description: The ARN of the IAM role

### iam\_role\_name

Description: The name of IAM role

### iam\_role\_path

Description: The path of IAM role

### iam\_user\_arn

Description: The ARN of the IAM user(s)

### iam\_user\_login\_profile\_encrypted\_password

Description: The encrypted password, base64 encoded

### iam\_user\_login\_profile\_key\_fingerprint

Description: The fingerprint of the PGP key used to encrypt the password

### iam\_user\_name

Description: The IAM user name(s)

### iam\_user\_ssh\_key\_fingerprint

Description: The MD5 message digest of the SSH public key

### iam\_user\_ssh\_key\_ssh\_public\_key\_id

Description: The unique identifier for the SSH public key

### iam\_user\_unique\_id

Description: The unique ID of the IAM user(s)

### policy

Description: The IAM policy document

### policy\_arn

Description: The ARN of the IAM policy

### policy\_description

Description: The description of the IAM policy

### policy\_id

Description: The ID of the IAM policy

### policy\_name

Description: The name of the IAM policy

