resource "aws_iam_role" "iam-role" {
  count = var.create_role ? 1 : 0

  name = local.role_name

  assume_role_policy    = var.assume_role_policy
  description           = var.role_description
  force_detach_policies = var.force_detach_policies
  max_session_duration  = var.max_session_duration
  path                  = var.role_path

  tags = merge(
    {
      "Name" = var.role_name
    },
    var.tags
  )
}


resource "aws_iam_role_policy_attachment" "iam-role-policy-attachment" {
  count = var.create_role && length(var.custom_role_policy_arns) > 0 ? length(var.custom_role_policy_arns) : 0

  role       = aws_iam_role.iam-role[0].name
  policy_arn = element(var.custom_role_policy_arns, count.index)
}

resource "aws_iam_instance_profile" "iam-instance-profile" {
  count = var.create_role && var.create_instance_profile ? 1 : 0
  name  = var.role_name
  path  = var.role_path
  role  = aws_iam_role.iam-role[0].name
}
