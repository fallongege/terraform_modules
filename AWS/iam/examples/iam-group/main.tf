data "aws_iam_policy_document" "sample" {
  statement {
    actions = [
      "s3:ListBuckets",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "sample2" {
  statement {
    actions = [
      "s3:GetBuckets",
    ]

    resources = ["*"]
  }
}

module "iam-group" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/iam/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  create_group   = true
  group_users    = module.iam-group.iam_user_name
  iam_group_name = "test"
  custom_group_policies = [
    {
      name   = "AllowS3Listing"
      policy = data.aws_iam_policy_document.sample.json
    },
    {
      name   = "AllowS3Get"
      policy = data.aws_iam_policy_document.sample2.json
    },
  ]

  create_user                   = true
  create_iam_access_key         = true
  create_iam_user_login_profile = true
  iam_user = [
    {
      user_name  = "pgp_pub"
      pgp_key    = "keybase:testuser"
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwTAIqCtVbTJse/Bkz8SPcv+Y6J3XWXHO4mlWX645daqnOukmbP3fO+aLwnjzngP/pXna60iaXkYMAbClCE4cIjZgtyQwrRvVCLomMFrMeVLWC4aWm7yXP5sp4dmGyBtZvgcuWnVAv0x2X9Xlt4dloCmuulg/lYY5hEf8TTpMFLDKuMzwCsBbYJ1D4IfaqGXYmPH2CJYMuTWQcUHuRzlqCb6fBy+F3inJ9QAP2QGewHfJlYltJ/OG5IZwPPML8n0onA0h966KI0xZFuHCyVxEfJ6cOZiOCYGU98e5/gX+oyNxUm/tn3eCFFDrZDpdZ1hQd7qqgSgAbgBe9GEL4QMdX"
    },
  ]
}
