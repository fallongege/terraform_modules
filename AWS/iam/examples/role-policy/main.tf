data "template_file" "fargate-policy" {
  template = file("${path.module}/fargate.tpl")
}

module "fargate-iam-role" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/iam/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  create_iam_policy = true
  description       = "Fargate IAM policy"
  policy            = data.template_file.fargate-policy.rendered
  policy_name       = "Fargate"

  create_role             = true
  assume_role_policy      = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ecs.amazonaws.com",
          "ecs-tasks.amazonaws.com",
          "application-autoscaling.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  role_name               = "Fargate"
  custom_role_policy_arns = [module.fargate-iam-role.policy_arn]
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
