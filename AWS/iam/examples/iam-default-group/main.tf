data "aws_caller_identity" "current" {}

data "template_file" "force-mfa" {
  template = file("${path.module}/force-mfa.tpl")

  vars = {
    ACCOUNT_ID = data.aws_caller_identity.current.account_id
  }
}

module "aws-iam-default" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/iam/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  create_account_password_policy = true

  create_default_groups = true
  default_group = [
    {
      name       = "Admin"
      users      = "pgp_pub"
      policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
    },
    {
      name       = "PowerUser"
      users      = "pgp_pub2"
      policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
    },
    {
      name       = "ForceMFA"
      users      = "pgp_pub3"
      policy_arn = module.aws-iam-default.policy_arn
    },
    {
      name       = "ReadOnly"
      users      = "pgp_pub4"
      policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
    },
  ]

  create_user                   = true
  create_iam_access_key         = true
  create_iam_user_login_profile = true
  iam_user = [
    {
      user_name  = "pgp_pub"
      pgp_key    = "keybase:testuser"
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwTAIqCtVbTJse/Bkz8SPcv+Y6J2LWXHO4mlWX645mcbr8dkmbP3fO+aLwnjzngP/pXna60iaXkYMAbClCE4cIjZgtyQwrRvVCLomMFrMeVLWC4aWm7yXP5sp4dmGyBtZvgcuWnVAv0x2X9Xlt4dloCmuulg/lYY5hEf8TTpMFLDKuMzwCsBbYJ1D4IfaqGXYmPH2CJYMuTWQcUHuRzlqCb6fBy+F3inJ9QAP2QGewHfJlYltJ/OG5IZwPPML8n0onA0h966KI0xZFuHCyVxEfJ6cOZiOCYGU98e5/gX+oyNxUm/tn3eCFFDrZDpdZ1hQd7qqgSgAbgBe9GEL4QMdX"
    },
    {
      user_name  = "pgp_pub2"
      pgp_key    = "keybase:testuser"
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwTAIqCtVbTJse/Bkz8SPcv+Y6J2LWXHO4mlWX645mcbr8dkmbP3fO+aLwnjzngP/pXna60iaXkYMAbClCE4cIjZgtyQwrRvVCLomMFrMeVLWC4aWm7yXP5sp4dmGyBtZvgcuWnVAv0x2X9Xlt4dloCmuulg/lYY5hEf8TTpMFLDKuMzwCsBbYJ1D4IfaqGXYmPH2CJYMuTWQcUHuRzlqCb6fBy+F3inJ9QAP2QGewHfJlYltJ/OG5IZwPPML8n0onA0h966KI0xZFuHCyVxEfJ6cOZiOCYGU98e5/gX+oyNxUm/tn3eCFFDrZDpdZ1hQd7qqgSgAbgBe9GEL4QMdX"
    },
    {
      user_name  = "pgp_pub3"
      pgp_key    = "keybase:testuser"
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwTAIqCtVbTJse/Bkz8SPcv+Y6J2LWXHO4mlWX645mcbr8dkmbP3fO+aLwnjzngP/pXna60iaXkYMAbClCE4cIjZgtyQwrRvVCLomMFrMeVLWC4aWm7yXP5sp4dmGyBtZvgcuWnVAv0x2X9Xlt4dloCmuulg/lYY5hEf8TTpMFLDKuMzwCsBbYJ1D4IfaqGXYmPH2CJYMuTWQcUHuRzlqCb6fBy+F3inJ9QAP2QGewHfJlYltJ/OG5IZwPPML8n0onA0h966KI0xZFuHCyVxEfJ6cOZiOCYGU98e5/gX+oyNxUm/tn3eCFFDrZDpdZ1hQd7qqgSgAbgBe9GEL4QMdX"
    },
    {
      user_name  = "pgp_pub4"
      pgp_key    = "keybase:testuser"
      public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwTAIqCtVbTJse/Bkz8SPcv+Y6J2LWXHO4mlWX645mcbr8dkmbP3fO+aLwnjzngP/pXna60iaXkYMAbClCE4cIjZgtyQwrRvVCLomMFrMeVLWC4aWm7yXP5sp4dmGyBtZvgcuWnVAv0x2X9Xlt4dloCmuulg/lYY5hEf8TTpMFLDKuMzwCsBbYJ1D4IfaqGXYmPH2CJYMuTWQcUHuRzlqCb6fBy+F3inJ9QAP2QGewHfJlYltJ/OG5IZwPPML8n0onA0h966KI0xZFuHCyVxEfJ6cOZiOCYGU98e5/gX+oyNxUm/tn3eCFFDrZDpdZ1hQd7qqgSgAbgBe9GEL4QMdX"
    },
  ]

  create_iam_policy = true
  policy_name       = "force-mfa"
  description       = "IAM policy that requires MFA authentication to interact with AWS"
  policy            = data.template_file.force-mfa.rendered
}
