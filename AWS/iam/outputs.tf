################
# Default Group outputs
################

output "default_group_users" {
  description = "List of IAM users in the default IAM groups"
  value       = flatten(aws_iam_group_membership.default-iam-group-membership.*.users)
}

output "default_group_name" {
  description = "The names of the default IAM groups"
  value       = element(concat(aws_iam_group.default-iam-group.*.name, [var.iam_group_name]), 0)
}

################
# Group outputs
################

output "group_users" {
  description = "List of IAM users in IAM group"
  value       = flatten(aws_iam_group_membership.iam-group-membership.*.users)
}

output "group_name" {
  description = "The name of the IAM group"
  value       = element(concat(aws_iam_group.iam-group.*.name, [var.iam_group_name]), 0)
}

################
# Policy outputs
################

output "policy_id" {
  description = "The ID of the IAM policy"
  value       = element(concat(aws_iam_policy.iam-policy.*.id, [""]), 0)
}

output "policy_arn" {
  description = "The ARN of the IAM policy"
  value       = element(concat(aws_iam_policy.iam-policy.*.arn, [""]), 0)
}

output "policy_description" {
  description = "The description of the IAM policy"
  value       = element(concat(aws_iam_policy.iam-policy.*.description, [""]), 0)
}

output "policy_name" {
  description = "The name of the IAM policy"
  value       = element(concat(aws_iam_policy.iam-policy.*.name, [""]), 0)
}

output "policy" {
  description = "The IAM policy document"
  value       = element(concat(aws_iam_policy.iam-policy.*.policy, [""]), 0)
}

################
# Role outputs
################

output "iam_role_arn" {
  description = "The ARN of the IAM role"
  value       = element(concat(aws_iam_role.iam-role.*.arn, [""]), 0)
}

output "iam_role_name" {
  description = "The name of IAM role"
  value       = element(concat(aws_iam_role.iam-role.*.name, [""]), 0)
}

output "iam_role_path" {
  description = "The path of IAM role"
  value       = element(concat(aws_iam_role.iam-role.*.path, [""]), 0)
}

output "iam_instance_profile_arn" {
  description = "The ARN of the IAM instance profile"
  value       = element(concat(aws_iam_instance_profile.iam-instance-profile.*.arn, [""]), 0)
}

output "iam_instance_profile_name" {
  description = "The name of the IAM instance profile"
  value       = element(concat(aws_iam_instance_profile.iam-instance-profile.*.name, [""]), 0)
}

output "iam_instance_profile_path" {
  description = "The path of IAM instance profile"
  value       = element(concat(aws_iam_instance_profile.iam-instance-profile.*.path, [""]), 0)
}

################
# User outputs
################

output "iam_user_name" {
  description = "The IAM user name(s)"
  value       = concat(aws_iam_user.iam-user.*.name, [""])
}

output "iam_user_arn" {
  description = "The ARN of the IAM user(s)"
  value       = concat(aws_iam_user.iam-user.*.arn, [""])
}

output "iam_user_unique_id" {
  description = "The unique ID of the IAM user(s)"
  value       = concat(aws_iam_user.iam-user.*.unique_id, [""])
}

output "iam_user_login_profile_key_fingerprint" {
  description = "The fingerprint of the PGP key used to encrypt the password"
  value = element(
    concat(aws_iam_user_login_profile.iam-user-login-profile.*.key_fingerprint, [""]),
    0,
  )
}

output "iam_user_login_profile_encrypted_password" {
  description = "The encrypted password, base64 encoded"
  value = element(
    concat(aws_iam_user_login_profile.iam-user-login-profile.*.encrypted_password, [""]),
    0,
  )
}

output "iam_access_key_id" {
  description = "The access key ID of the IAM user(s)"
  value = element(
    concat(
      aws_iam_access_key.iam-user-access-key.*.id,
      [""],
    ),
    0,
  )
}

output "iam_access_key_key_fingerprint" {
  description = "The fingerprint of the PGP key used to encrypt the secret"
  value       = element(concat(aws_iam_access_key.iam-user-access-key.*.key_fingerprint, [""]), 0)
}

output "iam_access_key_encrypted_secret" {
  description = "The encrypted secret, base64 encoded"
  value       = element(concat(aws_iam_access_key.iam-user-access-key.*.encrypted_secret, [""]), 0)
}

output "iam_user_ssh_key_ssh_public_key_id" {
  description = "The unique identifier for the SSH public key"
  value = element(
    concat(aws_iam_user_ssh_key.iam-user-ssh-key.*.ssh_public_key_id, [""]),
    0,
  )
}

output "iam_user_ssh_key_fingerprint" {
  description = "The MD5 message digest of the SSH public key"
  value       = element(concat(aws_iam_user_ssh_key.iam-user-ssh-key.*.fingerprint, [""]), 0)
}
