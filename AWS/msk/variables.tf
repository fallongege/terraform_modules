#############################
# MSK Configuration Variables
#############################

variable "create_msk_configuration" {
  description = "Whether the MSK configuration should be created"
  type        = bool
  default     = false
}

variable "msk_configuration_description" {
  description = "A description of the MSK configuration"
  type        = string
  default     = false
}

variable "msk_configuration_name" {
  description = "The name of the MSK configuration"
  type        = string
  default     = false
}

variable "msk_server_properties" {
  description = <<EOF
  Contents of the server.properties file. Supported properties are documented in the
  [MSK Developer Guide](https://docs.aws.amazon.com/msk/latest/developerguide/msk-configuration-properties.html)
  EOF
  type        = map(string)
  default     = {}
}

#######################
# MSK Cluster Variables
#######################

variable "create_msk_cluster" {
  description = "Whether the MSK cluster should be created"
  type        = bool
  default     = false
}

variable "broker_instance_type" {
  description = "The instance type to use for the Kafka brokers"
  type        = string
  default     = "kafka.t3.small"
}

variable "broker_volume_size" {
  description = "The size in GiB of the EBS volume for the data drive on each broker node"
  type        = number
  default     = 1000
}

variable "certificate_authority_arns" {
  description = "List of ACM Certificate Authority Amazon Resource Names (ARNs) to be used for TLS client authentication"
  type        = list(string)
  default     = []
}

variable "client_tls_auth_enabled" {
  description = "Whether Client TLS Authentication is enabled"
  type        = bool
  default     = false
}

variable "client_broker" {
  description = "Encryption setting for data in transit between clients and brokers. Valid values: `TLS`, `TLS_PLAINTEXT`, and `PLAINTEXT`"
  type        = string
  default     = "TLS"
}

variable "cloudwatch_logs_enabled" {
  description = "Whether to enable the streaming broker logs to Cloudwatch Logs"
  type        = bool
  default     = false
}

variable "cloudwatch_logs_log_group" {
  description = "The name of the Cloudwatch Log Group to deliver logs to"
  type        = string
  default     = ""
}

variable "encryption_at_rest_kms_key_arn" {
  description = "The ARN or short ID of a KMS key to use for encrypting data at rest"
  type        = string
  default     = ""
}

variable "encryption_in_cluster" {
  description = "Whether data communication among broker nodes is encrypted"
  type        = bool
  default     = true
}

variable "enhanced_monitoring" {
  description = "Specify the desired enhanced MSK CloudWatch monitoring level. Valid values: `DEFAULT`, `PER_BROKER`, and `PER_TOPIC_PER_BROKER`"
  type        = string
  default     = "DEFAULT"
}

variable "firehose_logs_enabled" {
  description = "Whether to enable the streaming broker logs to Kinesis Data Firehose"
  type        = bool
  default     = false
}

variable "firehose_delivery_stream" {
  description = "The name of the Kinesis Data Firehose delivery stream to deliver logs to"
  type        = string
  default     = ""
}

variable "jmx_exporter_enabled" {
  description = "Whether to enable the JMX Exporter"
  type        = bool
  default     = false
}

variable "kafka_version" {
  description = "The desired Kafka software version"
  type        = string
  default     = "2.2.1"
}

variable "msk_configuration_arn" {
  description = "Amazon Resource Name (ARN) of the MSK Configuration to use in the cluster."
  type        = string
  default     = ""
}

variable "msk_configuration_revision" {
  description = "Revision of the MSK Configuration to use in the cluster."
  type        = string
  default     = ""
}

variable "msk_cluster_name" {
  description = "The name of the MSK cluster"
  type        = string
  default     = ""
}

variable "node_exporter_enabled" {
  description = "Whether to enable the Node Exporter"
  type        = bool
  default     = false
}

variable "number_of_broker_nodes" {
  description = "The desired total number of broker nodes in the kafka cluster. It must be a multiple of the number of specified client subnets"
  type        = number
  default     = 1
}

variable "s3_logs_enabled" {
  description = "Whether to enable or disable streaming broker logs to S3"
  type        = bool
  default     = false
}

variable "s3_logs_bucket" {
  description = "The name of the S3 bucket to deliver logs to"
  type        = string
  default     = ""
}

variable "s3_logs_prefix" {
  description = "The prefix to append to the S3 folder name logs are delivered to"
  type        = string
  default     = ""
}

variable "security_group_ids" {
  description = "List of Security Group IDs that are allowed ingress to the MSK Cluster"
  type        = list(string)
  default     = []
}

variable "subnet_ids" {
  description = "List of VPC subnet IDs to place MSK cluster in"
  type        = any
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}
