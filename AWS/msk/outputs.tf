output "bootstrap_brokers" {
  description = "A comma separated list of one or more hostname:port pairs of kafka brokers suitable to boostrap connectivity to the kafka cluster"
  value       = join("", aws_msk_cluster.msk-cluster.*.bootstrap_brokers)
}

output "bootstrap_broker_tls" {
  description = "A comma separated list of one or more DNS names (or IPs) and TLS port pairs kafka brokers suitable to boostrap connectivity to the kafka cluster"
  value       = join("", aws_msk_cluster.msk-cluster.*.bootstrap_brokers_tls)
}

output "cluster_arn" {
  description = "Amazon Resource Name (ARN) of the MSK cluster"
  value       = join("", aws_msk_cluster.msk-cluster.*.arn)
}

output "cluster_name" {
  description = "MSK Cluster name"
  value       = join("", aws_msk_cluster.msk-cluster.*.cluster_name)
}

output "config_arn" {
  description = "Amazon Resource Name (ARN) of the configuration"
  value       = join("", aws_msk_configuration.msk-configuration.*.arn)
}

output "current_version" {
  description = "Current version of the MSK Cluster used for updates"
  value       = join("", aws_msk_cluster.msk-cluster.*.current_version)
}

output "latest_revision" {
  description = "Latest revision of the configuration"
  value       = join("", aws_msk_configuration.msk-configuration.*.latest_revision)
}

output "zookeeper_connect_string" {
  description = "A comma separated list of one or more hostname:port pairs to use to connect to the Apache Zookeeper cluster"
  value       = join("", aws_msk_cluster.msk-cluster.*.zookeeper_connect_string)
}
