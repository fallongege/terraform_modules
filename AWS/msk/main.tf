resource "aws_msk_cluster" "msk-cluster" {
  count = var.create_msk_cluster ? 1 : 0

  cluster_name           = var.msk_cluster_name
  enhanced_monitoring    = var.enhanced_monitoring
  kafka_version          = var.kafka_version
  number_of_broker_nodes = var.number_of_broker_nodes

  broker_node_group_info {
    client_subnets  = var.subnet_ids
    ebs_volume_size = var.broker_volume_size
    instance_type   = var.broker_instance_type
    security_groups = var.security_group_ids
  }

  configuration_info {
    arn      = var.msk_configuration_arn
    revision = var.msk_configuration_revision
  }

  encryption_info {
    encryption_in_transit {
      client_broker = var.client_broker
      in_cluster    = var.encryption_in_cluster
    }
    encryption_at_rest_kms_key_arn = var.encryption_at_rest_kms_key_arn
  }

  dynamic "client_authentication" {
    for_each = var.client_tls_auth_enabled ? [1] : []

    content {
      tls {
        certificate_authority_arns = var.certificate_authority_arns
      }
    }
  }

  open_monitoring {
    prometheus {
      jmx_exporter {
        enabled_in_broker = var.jmx_exporter_enabled
      }
      node_exporter {
        enabled_in_broker = var.node_exporter_enabled
      }
    }
  }

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled   = var.cloudwatch_logs_enabled
        log_group = var.cloudwatch_logs_log_group
      }
      firehose {
        enabled         = var.firehose_logs_enabled
        delivery_stream = var.firehose_delivery_stream
      }
      s3 {
        enabled = var.s3_logs_enabled
        bucket  = var.s3_logs_bucket
        prefix  = var.s3_logs_prefix
      }
    }
  }

  tags = merge(
    {
      "Name" = var.msk_cluster_name
    },
    var.tags
  )
}
