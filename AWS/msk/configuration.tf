resource "aws_msk_configuration" "msk-configuration" {
  count = var.create_msk_configuration ? 1 : 0

  description       = var.msk_configuration_description
  kafka_versions    = [var.kafka_version]
  name              = var.msk_configuration_name
  server_properties = join("\n", [for k in keys(var.msk_server_properties) : format("%s = %s", k, var.msk_server_properties[k])])
}
