resource "aws_elasticache_parameter_group" "redis" {
  count = var.create_redis ? 1 : 0

  name = local.elasticache_parameter_group_name

  description = local.elasticache_parameter_group_name
  family      = var.elasticache_parameter_group_family

  dynamic "parameter" {
    for_each = var.cluster_mode_enabled ? concat([{ name = "cluster-enabled", value = "yes" }], var.parameter) : var.parameter
    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
}
