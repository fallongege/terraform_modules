## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### apply\_immediately

Description: Whether to apply changes immediately

Type: `bool`

Default: `true`

### at\_rest\_encryption\_enabled

Description: Whether to enable encryption at rest

Type: `bool`

Default: `false`

### auth\_token

Description: The Auth token for password protecting redis, `transit_encryption_enabled` must be set to `true`. Password must be longer than 16 chars

Type: `string`

Default: `null`

### automatic\_failover\_enabled

Description: Whether automatic failover (Not available for T1/T2 instances) is enabled

Type: `bool`

Default: `false`

### availability\_zones

Description: A list of vailability zone IDs

Type: `list(string)`

Default: `[]`

### cluster\_mode\_enabled

Description: Whether to enable creation of a native redis cluster. `automatic_failover_enabled` must be set to `true`

Type: `bool`

Default: `false`

### cluster\_mode\_num\_node\_groups

Description: The number of node groups (shards) for this Redis replication group. Changing this number will trigger an online resizing operation before other settings modifications

Type: `number`

Default: `0`

### cluster\_mode\_replicas\_per\_node\_group

Description: The number of replica nodes in each node group. Valid values are 0 to 5. Changing this number will force a new resource

Type: `number`

Default: `0`

### cluster\_size

Description: The number of nodes in cluster. \*Ignored when `cluster_mode_enabled` == `true`\*

Type: `number`

Default: `1`

### create\_redis

Description: Whether to create Redis

Type: `bool`

Default: `true`

### elasticache\_parameter\_group\_family

Description: The name of the elasticache parameter group family

Type: `string`

Default: `""`

### elasticache\_parameter\_group\_name

Description: The name of the elasticache parameter group

Type: `string`

Default: `""`

### engine\_version

Description: The Redis engine version

Type: `string`

Default: `"5.0.6"`

### instance\_type

Description: The Elasticache instance type

Type: `string`

Default: `"cache.t2.micro"`

### kms\_key\_id

Description: The ARN of the key that you wish to use if encrypting at rest. If not supplied, uses service managed encryption. `at_rest_encryption_enabled` must be set to `true`

Type: `any`

Default: `null`

### maintenance\_window

Description: The maintenance window

Type: `string`

Default: `"wed:03:00-wed:04:00"`

### notification\_topic\_arn

Description: The SNS notification topic arn

Type: `string`

Default: `""`

### parameter

Description: A list of Redis parameters to apply. Note that parameters may differ from one Redis family to another

Type:

```hcl
list(object({
    name  = string
    value = string
  }))
```

Default: `[]`

### port

Description: The Redis port

Type: `number`

Default: `6379`

### redis\_subnet\_group\_name

Description: The name of the redis subnet group

Type: `string`

Default: `""`

### replication\_group\_description

Description: A short description of the purpose of the Redis Replication Group

Type: `string`

Default: `""`

### replication\_group\_id

Description: Replication group ID with the following constraints:   
A name must contain from 1 to 20 alphanumeric characters or hyphens.   
 The first character must be a letter.   
 A name cannot end with a hyphen or contain two consecutive hyphens.

Type: `string`

Default: `""`

### security\_group\_ids

Description: List of Security Group IDs that are allowed ingress to the cluster's Security Group created in the module

Type: `list(string)`

Default: `[]`

### snapshot\_retention\_limit

Description: The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them.

Type: `number`

Default: `7`

### snapshot\_window

Description: The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster.

Type: `string`

Default: `"06:30-07:30"`

### subnets

Description: List of subnet IDs

Type: `any`

Default: `[]`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

### transit\_encryption\_enabled

Description: Whether to enable TLS

Type: `bool`

Default: `false`

## Outputs

The following outputs are exported:

### redis\_endpoint

Description: The Redis primary endpoint

### redis\_id

Description: The Redis cluster ID

### redis\_port

Description: The Redis port

