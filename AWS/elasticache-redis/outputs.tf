output "redis_endpoint" {
  description = "The Redis primary endpoint"
  value       = var.cluster_mode_enabled ? join("", aws_elasticache_replication_group.redis.*.configuration_endpoint_address) : join("", aws_elasticache_replication_group.redis.*.primary_endpoint_address)
}

output "redis_id" {
  description = "The Redis cluster ID"
  value       = join("", aws_elasticache_replication_group.redis.*.id)
}

output "redis_port" {
  description = "The Redis port"
  value       = join("", aws_elasticache_replication_group.redis.*.port)
}
