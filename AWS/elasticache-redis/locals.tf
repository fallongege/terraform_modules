locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  elasticache_parameter_group_name = "${local.name_prefix}-${var.elasticache_parameter_group_name}"
  redis_subnet_group_name          = "${local.name_prefix}-${var.redis_subnet_group_name}"

  member_clusters_count = (var.cluster_mode_enabled
    ?
    (var.cluster_mode_num_node_groups * (var.cluster_mode_replicas_per_node_group + 1))
    :
    var.cluster_size
  )

  elasticache_member_clusters = var.create_redis ? tolist(aws_elasticache_replication_group.redis.0.member_clusters) : []
  replication_group_id        = "${local.name_prefix}-${var.replication_group_id}"
}
