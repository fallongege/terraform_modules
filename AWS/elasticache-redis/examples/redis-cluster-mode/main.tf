module "redis" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/elasticache-redis/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  automatic_failover_enabled           = true
  availability_zones                   = data.aws_availability_zones.azs.zone_ids
  cluster_mode_enabled                 = true
  cluster_mode_num_node_groups         = var.cluster_mode_num_node_groups
  cluster_mode_replicas_per_node_group = lookup(var.cluster_mode_replicas_per_node_group, var.environment)
  elasticache_parameter_group_name     = "redis50"
  elasticache_parameter_group_family   = "redis5.0"
  engine_version                       = var.redis_engine_version
  instance_type                        = lookup(var.redis_instance_type, var.environment)
  redis_subnet_group_name              = "redis-subnet-group"
  replication_group_description        = "Redis replication group"
  replication_group_id                 = "redis"
  security_group_ids                   = [module.redis-sg.security_group_id]
  subnets                              = ["subnet-3143541"]
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = var.environment
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
