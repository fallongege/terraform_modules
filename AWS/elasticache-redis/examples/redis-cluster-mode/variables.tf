variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}


#####################
# Redis Variables
#####################
variable "redis_cluster_size" {
  description = "A map of the environments specifying the number of nodes in the redis cluster"
  type        = map(number)

  default = {
    stg  = 2
    prod = 3
  }
}

variable "redis_instance_type" {
  description = "A map of the environments specifying the redis instance type"
  type        = map(string)

  default = {
    stg  = "cache.t2.micro"
    prod = "cache.t2.small"
  }
}

variable "redis_engine_version" {
  description = "The Redis engine version"
  default     = "5.0.6"
}

variable "cluster_mode_num_node_groups" {
  description = "The number of node groups (shards) for this Redis replication group. Changing this number will trigger an online resizing operation before other settings modifications"
  default     = 1
}

variable "cluster_mode_replicas_per_node_group" {
  description = "The number of replicas per node group in the Redis cluster"
  type        = map(number)

  default = {
    stg  = 1
    prod = 2
  }
}

