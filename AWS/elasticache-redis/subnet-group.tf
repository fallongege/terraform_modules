resource "aws_elasticache_subnet_group" "redis" {
  count = var.create_redis ? 1 : 0

  name = local.redis_subnet_group_name

  description = local.redis_subnet_group_name
  subnet_ids  = var.subnets
}
