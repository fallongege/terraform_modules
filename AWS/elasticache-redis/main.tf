resource "aws_elasticache_replication_group" "redis" {
  count = var.create_redis ? 1 : 0

  at_rest_encryption_enabled    = var.at_rest_encryption_enabled
  apply_immediately             = var.apply_immediately
  auth_token                    = var.transit_encryption_enabled ? var.auth_token : null
  automatic_failover_enabled    = var.automatic_failover_enabled
  availability_zones            = var.cluster_mode_enabled ? null : slice(var.availability_zones, 0, var.cluster_size)
  engine_version                = var.engine_version
  kms_key_id                    = var.at_rest_encryption_enabled ? var.kms_key_id : null
  maintenance_window            = var.maintenance_window
  node_type                     = var.instance_type
  notification_topic_arn        = var.notification_topic_arn
  number_cache_clusters         = var.cluster_mode_enabled ? null : var.cluster_size
  parameter_group_name          = join("", aws_elasticache_parameter_group.redis.*.name)
  port                          = var.port
  replication_group_description = var.replication_group_description
  replication_group_id          = local.replication_group_id == "" ? var.create_redis : local.replication_group_id
  security_group_ids            = [join("", var.security_group_ids)]
  snapshot_retention_limit      = var.snapshot_retention_limit
  snapshot_window               = var.snapshot_window
  subnet_group_name             = join("", aws_elasticache_subnet_group.redis.*.name)
  transit_encryption_enabled    = var.transit_encryption_enabled

  tags = merge(
    {
      "Name" = local.replication_group_id == "" ? var.create_redis : local.replication_group_id
    },
    var.tags
  )

  dynamic "cluster_mode" {
    for_each = var.cluster_mode_enabled ? ["true"] : []
    content {
      num_node_groups         = var.cluster_mode_num_node_groups
      replicas_per_node_group = var.cluster_mode_replicas_per_node_group
    }
  }
}
