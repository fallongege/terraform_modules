##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

#################
# Redis variables
#################

variable "create_redis" {
  description = "Whether to create Redis"
  type        = bool
  default     = true
}

variable "apply_immediately" {
  description = "Whether to apply changes immediately"
  type        = bool
  default     = true
}

variable "at_rest_encryption_enabled" {
  description = "Whether to enable encryption at rest"
  type        = bool
  default     = false
}

variable "auth_token" {
  description = "The Auth token for password protecting redis, `transit_encryption_enabled` must be set to `true`. Password must be longer than 16 chars"
  type        = string
  default     = null
}

variable "automatic_failover_enabled" {
  description = "Whether automatic failover (Not available for T1/T2 instances) is enabled"
  type        = bool
  default     = false
}

variable "availability_zones" {
  description = "A list of vailability zone IDs"
  type        = list(string)
  default     = []
}

variable "cluster_mode_enabled" {
  description = "Whether to enable creation of a native redis cluster. `automatic_failover_enabled` must be set to `true`"
  type        = bool
  default     = false
}

variable "cluster_mode_num_node_groups" {
  description = "The number of node groups (shards) for this Redis replication group. Changing this number will trigger an online resizing operation before other settings modifications"
  type        = number
  default     = 0
}

variable "cluster_mode_replicas_per_node_group" {
  description = "The number of replica nodes in each node group. Valid values are 0 to 5. Changing this number will force a new resource"
  type        = number
  default     = 0
}

variable "cluster_size" {
  description = "The number of nodes in cluster. *Ignored when `cluster_mode_enabled` == `true`*"
  type        = number
  default     = 1
}

variable "elasticache_parameter_group_name" {
  description = "The name of the elasticache parameter group"
  type        = string
  default     = ""
}

variable "elasticache_parameter_group_family" {
  description = "The name of the elasticache parameter group family"
  type        = string
  default     = ""
}

variable "engine_version" {
  description = "The Redis engine version"
  type        = string
  default     = "5.0.6"
}

variable "instance_type" {
  description = "The Elasticache instance type"
  type        = string
  default     = "cache.t2.micro"
}

variable "kms_key_id" {
  description = "The ARN of the key that you wish to use if encrypting at rest. If not supplied, uses service managed encryption. `at_rest_encryption_enabled` must be set to `true`"
  type        = any
  default     = null
}

variable "maintenance_window" {
  description = "The maintenance window"
  type        = string
  default     = "wed:03:00-wed:04:00"
}

variable "notification_topic_arn" {
  description = "The SNS notification topic arn"
  type        = string
  default     = ""
}

variable "parameter" {
  description = "A list of Redis parameters to apply. Note that parameters may differ from one Redis family to another"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "port" {
  description = "The Redis port"
  type        = number
  default     = 6379
}

variable "redis_subnet_group_name" {
  description = "The name of the redis subnet group"
  type        = string
  default     = ""
}

variable "replication_group_description" {
  description = "A short description of the purpose of the Redis Replication Group"
  type        = string
  default     = ""
}

variable "replication_group_id" {
  description = "Replication group ID with the following constraints: \nA name must contain from 1 to 20 alphanumeric characters or hyphens. \n The first character must be a letter. \n A name cannot end with a hyphen or contain two consecutive hyphens."
  type        = string
  default     = ""
}

variable "security_group_ids" {
  description = "List of Security Group IDs that are allowed ingress to the cluster's Security Group created in the module"
  type        = list(string)
  default     = []
}

variable "snapshot_retention_limit" {
  description = "The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them."
  type        = number
  default     = 7
}

variable "snapshot_window" {
  description = "The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster."
  type        = string
  default     = "06:30-07:30"
}

variable "subnets" {
  description = "List of subnet IDs"
  type        = any
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "transit_encryption_enabled" {
  description = "Whether to enable TLS"
  type        = bool
  default     = false
}
