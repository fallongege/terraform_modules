## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### aws\_region

Description: The aws region that is being deployed into

Type: `string`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### cluster\_tags

Description: A map of tags to add to all resources

Type: `map(string)`

### ecs\_cluster\_name

Description: The name of the ECS cluster

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

No optional input.

## Outputs

The following outputs are exported:

### ecs\_cluster\_arn

Description: The ARN of the ECS cluster

### ecs\_cluster\_id

Description: The ID of the ECS cluster

### ecs\_cluster\_name

Description: The Name of the ECS cluster

