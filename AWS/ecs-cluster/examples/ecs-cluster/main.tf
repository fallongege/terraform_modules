module "fargate" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/ecs-cluster/?ref=main"

  aws_region    = var.aws_region
  business_unit = var.business_unit
  environment   = var.environment

  cluster_tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  ecs_cluster_name = "microservices"
}
