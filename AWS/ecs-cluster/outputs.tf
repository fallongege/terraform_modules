#####################
# ECS Cluster outputs
#####################

output "ecs_cluster_arn" {
  description = "The ARN of the ECS cluster"
  value       = aws_ecs_cluster.ecs-cluster.arn
}

output "ecs_cluster_id" {
  description = "The ID of the ECS cluster"
  value       = aws_ecs_cluster.ecs-cluster.id
}

output "ecs_cluster_name" {
  description = "The Name of the ECS cluster"
  value       = aws_ecs_cluster.ecs-cluster.name
}
