##################
# Global variables
##################
variable "aws_region" {
  description = "The aws region that is being deployed into"
  type        = string
}

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

#######################
# ECS Cluster variables
#######################
variable "cluster_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "ecs_cluster_name" {
  description = "The name of the ECS cluster"
  type        = string
}
