locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  cluster_name = "${local.name_prefix}-${var.ecs_cluster_name}-cluster-${var.aws_region}"
}
