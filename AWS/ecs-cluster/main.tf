resource "aws_ecs_cluster" "ecs-cluster" {
  name = local.cluster_name

  tags = merge(
    {
      "Name" = local.cluster_name
    },
    var.cluster_tags
  )
}
