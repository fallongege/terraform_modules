output "access_point_arns" {
  description = "The arns of the EFS access points"
  value       = { for arn in sort(keys(var.access_points)) : arn => aws_efs_access_point.efs-access-point[arn].arn }
}

output "access_point_ids" {
  description = "The ids of the EFS access points"
  value       = { for id in sort(keys(var.access_points)) : id => aws_efs_access_point.efs-access-point[id].id }
}

output "efs_file_system_arn" {
  description = "The arn of the EFS file system"
  value       = join("", aws_efs_file_system.efs-file-system.*.arn)
}

output "efs_file_system_id" {
  description = "The id of the EFS file system"
  value       = join("", aws_efs_file_system.efs-file-system.*.id)
}

output "efs_mount_target_dns_names" {
  description = "List of EFS mount target DNS names"
  value       = coalescelist(aws_efs_mount_target.efs-mount-target.*.dns_name, [""])
}

output "efs_mount_target_ids" {
  description = "List of EFS mount target IDs (one per Availability Zone)"
  value       = coalescelist(aws_efs_mount_target.efs-mount-target.*.id, [""])
}

output "efs_mount_target_ips" {
  description = "List of EFS mount target IPs (one per Availability Zone)"
  value       = coalescelist(aws_efs_mount_target.efs-mount-target.*.ip_address, [""])
}

output "efs_network_interface_ids" {
  description = "List of mount target network interface IDs"
  value       = coalescelist(aws_efs_mount_target.efs-mount-target.*.network_interface_id, [""])
}
