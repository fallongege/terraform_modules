resource "aws_efs_file_system" "efs-file-system" {
  count = var.create_efs_file_system ? 1 : 0

  creation_token                  = var.creation_token
  encrypted                       = var.encrypted
  kms_key_id                      = var.kms_key_id
  performance_mode                = var.performance_mode
  provisioned_throughput_in_mibps = var.provisioned_throughput_in_mibps
  throughput_mode                 = var.throughput_mode

  dynamic "lifecycle_policy" {
    for_each = var.transition_to_ia == "" ? [] : [1]
    content {
      transition_to_ia = var.transition_to_ia
    }
  }

  tags = merge(
    {
      "Name" = var.creation_token
    },
    var.tags
  )
}

resource "aws_efs_file_system_policy" "iam-policy" {
  count = var.create_efs_file_system_policy ? 1 : 0

  file_system_id = var.efs_file_system_id
  policy         = var.policy
}


resource "aws_efs_mount_target" "efs-mount-target" {
  count = var.create_efs_mount_target ? length(var.subnet_ids) : 0

  file_system_id  = var.efs_file_system_id
  ip_address      = var.mount_target_ip_address
  security_groups = var.security_group_ids
  subnet_id       = element(var.subnet_ids, count.index)
}

resource "aws_efs_access_point" "efs-access-point" {
  for_each = var.access_points

  file_system_id = var.efs_file_system_id

#  posix_user {
#    gid            = lookup(var.access_points[each.key]["posix_user"]["gid"], null)
#    uid            = lookup(var.access_points[each.key]["posix_user"]["uid"], null)
#    secondary_gids = lookup(lookup(var.access_points[each.key], "posix_user", {}), "secondary_gids", null) == null ? null : null
#  }

  root_directory {
    path = "/${each.key}"
#    creation_info {
#      owner_gid   = lookup(var.access_points[each.key]["creation_info"]["gid"], null)
#      owner_uid   = lookup(var.access_points[each.key]["creation_info"]["uid"], null)
#      permissions = lookup(var.access_points[each.key]["creation_info"]["permissions"], null)
#    }
  }

  tags = var.tags
}
