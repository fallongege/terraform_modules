###########################
# EFS File System Variables
###########################
variable "create_efs_file_system" {
  description = "Whether to create an efs file system"
  type        = bool
  default     = false
}

variable "creation_token" {
  description = "A unique name used as reference when creating the EFS to ensure idempotent file system creation"
  type        = string
  default     = ""
}

variable "encrypted" {
  description = "Whether the disk is encrypted"
  type        = bool
  default     = false
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying kms_key_id, encrypted needs to be set to true"
  type        = string
  default     = ""
}

variable "performance_mode" {
  description = "The file system performance mode. Can be either 'generalPurpose' or 'maxIO'"
  type        = string
  default     = "generalPurpose"
}

variable "provisioned_throughput_in_mibps" {
  description = "The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with throughput_mode set to provisioned"
  type        = number
  default     = null
}

variable "throughput_mode" {
  description = "Throughput mode for the file system. Defaults to bursting. Valid values: bursting, provisioned. When using provisioned, also set provisioned_throughput_in_mibps"
  type        = string
  default     = "bursting"
}

variable "transition_to_ia" {
  description = "Indicates how long it takes to transition files to the IA storage class. Valid values: AFTER_7_DAYS, AFTER_14_DAYS, AFTER_30_DAYS, AFTER_60_DAYS and AFTER_90_DAYS"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

##################################
# EFS File System Policy Variables
##################################
variable "create_efs_file_system_policy" {
  description = "Whether to create an efs file system policy"
  type        = bool
  default     = false
}

variable "efs_file_system_id" {
  description = "The id of the efs file system"
  type        = string
  default     = ""
}

variable "policy" {
  description = "The JSON formatted file system policy for the EFS file system"
  type        = string
  default     = ""
}

############################
# EFS Mount Target Variables
############################
variable "create_efs_mount_target" {
  description = "Whether to create an efs mount target"
  type        = bool
  default     = false
}

variable "mount_target_ip_address" {
  description = "The address (within the address range of the specified subnet) at which the file system may be mounted via the mount target"
  type        = string
  default     = null
}

variable "security_group_ids" {
  description = "Security group IDs to allow access to the EFS"
  type        = list(string)
  default     = []
}

variable "subnet_ids" {
  description = "The Subnet Ids that can be used for the mount"
  type        = any
  default     = []
}

############################
# EFS Access Point Variables
############################
variable "access_points" {
  description = "A map of the access points in the EFS volume"
  type        = map(map(any))
  default     = {}
}
