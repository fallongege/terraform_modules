## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### description

Description: Describes the purpose of the secret

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### kms\_key\_id

Description: The ID of the KMS key used to encrypt the secret

Type: `string`

### secret\_name

Description: Specifies the friendly name of the secret

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### recovery\_window\_in\_days

Description: Specifies the number of days that AWS Secrets Manager waits before it can delete the secret

Type: `number`

Default: `30`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

## Outputs

The following outputs are exported:

### secret\_arn

Description: n/a

### secret\_id

Description: n/a

### secret\_name

Description: n/a

