module "secret" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/secrets-manager/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  description = "Contains secrets"
  kms_key_id  = module.kms.kms_key_id
  secret_name = "secrets"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
