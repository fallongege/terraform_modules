variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}
