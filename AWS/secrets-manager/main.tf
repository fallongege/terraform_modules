resource "aws_secretsmanager_secret" "secret" {
  name = local.secret_name

  description             = var.description
  kms_key_id              = var.kms_key_id
  recovery_window_in_days = var.recovery_window_in_days

  tags = merge(
    {
      "Name" = local.secret_name
    },
    var.tags
  )
}
