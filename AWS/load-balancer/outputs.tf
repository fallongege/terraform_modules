#######################
# Load Balancer outputs
#######################

output "lb_id" {
  description = "The ID of the load balancer"
  value       = concat(aws_lb.lb.*.id, [""])[0]
}

output "lb_arn" {
  description = "The ARN of the load balancer"
  value       = concat(aws_lb.lb.*.arn, [""])[0]
}

output "lb_dns_name" {
  description = "The DNS name of the load balancer"
  value       = concat(aws_lb.lb.*.dns_name, [""])[0]
}

output "lb_arn_suffix" {
  description = "ARN suffix of the load balancer"
  value       = concat(aws_lb.lb.*.arn_suffix, [""])[0]
}

output "lb_zone_id" {
  description = "The zone_id of the load balancer"
  value       = concat(aws_lb.lb.*.zone_id, [""])[0]
}

##################
# Listener outputs
##################

output "http_tcp_listener_arns" {
  description = "The ARN of the TCP and HTTP load balancer listeners created"
  value       = aws_lb_listener.http-tcp-listener.*.arn
}

output "http_tcp_listener_ids" {
  description = "The IDs of the TCP and HTTP load balancer listeners created"
  value       = aws_lb_listener.http-tcp-listener.*.id
}

output "https_listener_arns" {
  description = "The ARNs of the HTTPS load balancer listeners created"
  value       = aws_lb_listener.https-listener.*.arn
}

output "https_listener_ids" {
  description = "The IDs of the load balancer listeners created"
  value       = aws_lb_listener.https-listener.*.id
}

#######################
# Target Group outputs
#######################

output "target_group_arns" {
  description = "The ARNs of the target groups"
  value       = aws_lb_target_group.lb-tg.*.arn
}

output "target_group_arn_suffixes" {
  description = "The ARN suffixes of the target group"
  value       = aws_lb_target_group.lb-tg.*.arn_suffix
}

output "target_group_names" {
  description = "Name of the target group."
  value       = aws_lb_target_group.lb-tg.*.name
}
