module "target-group" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/load-balancer/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Listener rule arguments
  create_listener_rule = true
  listener_arn         = module.alb.http_tcp_listener_arns[1]
  listener_rules = [
    {
      actions = [
        {
          priority = 100
          type     = "forward"
        }
      ],
      conditions = [{
        host_headers = [module.route53.route53_record_name[0]]
      }]
    }
  ]
  target_group_arn = module.target-group.target_group_arns[0]

  # Target Group arguments
  create_tg = true
  target_group_tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  target_groups = [
    {
      name             = "target-group"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = 80
        healthy_threshold   = 5
        unhealthy_threshold = 2
        timeout             = 30
        protocol            = "HTTP"
        matcher             = 200
      }
    },
  ]
  vpc_id = "vpc-13435434"
}
