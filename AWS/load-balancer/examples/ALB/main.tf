module "alb-sg" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/security-group/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  name = "alb"

  create_sg   = true
  description = "Security group that is attached to the public ecs alb"
  vpc_id      = "vpc-1344934513"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }

  create_ingress_with_cidr_blocks = true
  ingress_with_cidr_blocks = [
    {
      rule        = "http from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "http from public"
    },
    {
      rule        = "https from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "https from public"
    },
    {
      rule        = "3000 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 3000
      to_port     = 3000
      protocol    = "tcp"
      description = "3000 from public"
    },
    {
      rule        = "8000 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 8000
      to_port     = 8000
      protocol    = "tcp"
      description = "8000 from public"
    },
    {
      rule        = "8080 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "8080 from public"
    },
    {
      rule        = "all from VPN"
      cidr_blocks = "10.0.0.0/8"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "all from VPN"
    },
  ]

  create_ingress_with_ipv6_cidr_blocks = true
  ingress_with_ipv6_cidr_blocks = [
    {
      rule             = "http from public"
      ipv6_cidr_blocks = "::/0"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      description      = "http from public"
    },
    {
      rule             = "https from public"
      ipv6_cidr_blocks = "::/0"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      description      = "https from public"
    },
  ]

  create_egress_with_cidr_blocks = true
  egress_with_cidr_blocks = [
    {
      rule        = "All IPv4 traffic"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "All IPv4 traffic"
    }
  ]

  create_egress_with_ipv6_cidr_blocks = true
  egress_with_ipv6_cidr_blocks = [
    {
      rule        = "All IPv6 traffic"
      cidr_blocks = "::/0"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "All IPv6 traffic"
    }
  ]
}

module "alb" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/load-balancer/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  name   = "ecs"
  scheme = "public"
  type   = "ALB"

  create_lb          = true
  internal           = false
  load_balancer_type = "application"
  security_groups    = [module.alb-sg.security_group_id]
  subnets            = "subnet-1343930294"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }

  create_https_listener = true
  https_listeners = [
    {
      port            = 443
      protocol        = "HTTPS"
      certificate_arn = module.acm.acm_certificate_arn
    }
  ]
  target_group_arn = join("", module.alb.target_group_arns)

  create_tg = true
  target_group_tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  target_groups = [
    {
      name             = "ecs-tg"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "ip"
    },
  ]

  vpc_id = "vpc-1344934513"
}
