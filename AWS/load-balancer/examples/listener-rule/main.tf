module "listener-rule" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/load-balancer/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Listener rule arguments
  create_listener_rule = true
  listener_arn         = module.alb.https_listener_arns[0]
  listener_rules = [
    {
      actions = [
        {
          priority = 300
          type     = "forward"
        }
      ],
      conditions = [{
        host_headers = [module.route53.route53_record_name[0]]
      }]
    }
  ]
  target_group_arn = module.target-group-443.target_group_arns[0]
}
