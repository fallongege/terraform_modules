################
# Global variables
################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Locals
################

variable "name" {
  description = "The name of the load balancer"
  type        = string
  default     = ""
}

variable "scheme" {
  description = "The scheme of the load balancer. Valid values are public | private"
  type        = string
  default     = ""
}

variable "type" {
  description = "The type of load balalncer. Valid values are ALB | NLB"
  type        = string
  default     = ""
}

################
# LB variables
################

variable "create_lb" {
  description = "Whether the Load Balancer should be created"
  type        = bool
  default     = false
}

variable "access_logs" {
  description = "Map containing access logging configuration for the load balancer"
  type        = map(string)
  default     = {}
}

variable "drop_invalid_header_fields" {
  description = "Whether invalid header fields are dropped in application load balancers"
  type        = bool
  default     = false
}

variable "enable_cross_zone_load_balancing" {
  description = "Whether cross zone load balancing should be enabled in network load balancers"
  type        = bool
  default     = false
}

variable "enable_deletion_protection" {
  description = "Whether deletion of the load balancer is disabled via the AWS API"
  type        = bool
  default     = false
}

variable "enable_http2" {
  description = "Whether HTTP/2 is enabled in application load balancers"
  type        = bool
  default     = true
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle"
  type        = number
  default     = 60
}

variable "internal" {
  description = "Whether the load balancer is internal or external facing"
  type        = bool
  default     = true
}

variable "ip_address_type" {
  description = "The type of IP addresses used by the subnets for your load balancer"
  type        = string
  default     = "ipv4"
}

variable "load_balancer_type" {
  description = "The type of load balancer to create. Valid values are application | network"
  type        = string
  default     = "application"
}

variable "security_groups" {
  description = "The security groups to attach to the load balancer"
  type        = list(string)
  default     = []
}

variable "subnet_mapping" {
  description = "A list of subnet mapping blocks describing subnets to attach to the network load balancer"
  type        = list(map(string))
  default     = []
}

variable "subnets" {
  description = "A list of subnets to associate with the load balancer"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "A map of tags to add to the load balancer"
  type        = map(string)
  default     = {}
}

####################
# Listener variables
####################

variable "create_http_listener" {
  description = "Whether an http listener should be created on the load balancer"
  type        = bool
  default     = false
}

variable "create_https_listener" {
  description = "Whether an https listener should be created on the load balancer"
  type        = bool
  default     = false
}

variable "create_listener_rule" {
  description = "Whether to create a load balancer listener rule"
  type        = bool
  default     = false
}

variable "extra_ssl_certs" {
  description = "A list of maps describing any extra SSL certificates to apply to the HTTPS listeners. Required key/values: certificate_arn, https_listener_index (the index of the listener within https_listeners which the cert applies toward)."
  type        = list(map(string))
  default     = []
}

variable "http_tcp_listeners" {
  description = <<EOF
  A list of maps describing the HTTP listeners or TCP ports for this ALB.
  Required key/values: port, protocol.
  Optional key/values: target_group_index (defaults to http_tcp_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "https_listeners" {
  description = <<EOF
  A list of maps describing the HTTPS listeners for this ALB.
  Required key/values: port, certificate_arn.
  Optional key/values: ssl_policy (defaults to ELBSecurityPolicy-2016-08),
                       target_group_index (defaults to https_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "listener_arn" {
  description = "The ARN of the load balancer listener"
  type        = string
  default     = ""
}

variable "listener_rules" {
  description = <<EOF
  A list of maps describing the Listener Rules for this ALB.
  Required key/values: actions, conditions.
  Optional key/values: priority, https_listener_index (default to https_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "listener_ssl_policy_default" {
  description = "The security policy if using HTTPS externally on the load balancer"
  type        = string
  default     = "ELBSecurityPolicy-2016-08"
}

variable "target_group_arn" {
  description = "The arn of the target group"
  type        = any
  default     = ""
}

########################
# Target Group variables
########################

variable "create_tg" {
  description = "Whether to create a target group on the load balancer"
  type        = bool
  default     = false
}

variable "target_group_tags" {
  description = "A map of tags to add to all target groups"
  type        = map(string)
  default     = {}
}

variable "target_groups" {
  description = <<EOF
  A list of maps containing key/value pairs that define the target groups to be created.
  Order of these maps is important and the index of these are to be referenced in listener definitions.
  Required key/values: name, backend_protocol, backend_port
  EOF
  type        = any
  default     = []
}

variable "vpc_id" {
  description = "VPC id where the load balancer and other resources will be deployed"
  type        = string
  default     = null
}
