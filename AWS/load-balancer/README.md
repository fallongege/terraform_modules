## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### access\_logs

Description: Map containing access logging configuration for the load balancer

Type: `map(string)`

Default: `{}`

### create\_http\_listener

Description: Whether an http listener should be created on the load balancer

Type: `bool`

Default: `false`

### create\_https\_listener

Description: Whether an https listener should be created on the load balancer

Type: `bool`

Default: `false`

### create\_lb

Description: Whether the Load Balancer should be created

Type: `bool`

Default: `false`

### create\_listener\_rule

Description: Whether to create a load balancer listener rule

Type: `bool`

Default: `false`

### create\_tg

Description: Whether to create a target group on the load balancer

Type: `bool`

Default: `false`

### drop\_invalid\_header\_fields

Description: Whether invalid header fields are dropped in application load balancers

Type: `bool`

Default: `false`

### enable\_cross\_zone\_load\_balancing

Description: Whether cross zone load balancing should be enabled in network load balancers

Type: `bool`

Default: `false`

### enable\_deletion\_protection

Description: Whether deletion of the load balancer is disabled via the AWS API

Type: `bool`

Default: `false`

### enable\_http2

Description: Whether HTTP/2 is enabled in application load balancers

Type: `bool`

Default: `true`

### extra\_ssl\_certs

Description: A list of maps describing any extra SSL certificates to apply to the HTTPS listeners. Required key/values: certificate\_arn, https\_listener\_index (the index of the listener within https\_listeners which the cert applies toward).

Type: `list(map(string))`

Default: `[]`

### http\_tcp\_listeners

Description:   A list of maps describing the HTTP listeners or TCP ports for this ALB.  
  Required key/values: port, protocol.  
  Optional key/values: target\_group\_index (defaults to http\_tcp\_listeners[count.index])

Type: `any`

Default: `[]`

### https\_listeners

Description:   A list of maps describing the HTTPS listeners for this ALB.  
  Required key/values: port, certificate\_arn.  
  Optional key/values: ssl\_policy (defaults to ELBSecurityPolicy-2016-08),  
                       target\_group\_index (defaults to https\_listeners[count.index])

Type: `any`

Default: `[]`

### idle\_timeout

Description: The time in seconds that the connection is allowed to be idle

Type: `number`

Default: `60`

### internal

Description: Whether the load balancer is internal or external facing

Type: `bool`

Default: `true`

### ip\_address\_type

Description: The type of IP addresses used by the subnets for your load balancer

Type: `string`

Default: `"ipv4"`

### listener\_arn

Description: The ARN of the load balancer listener

Type: `string`

Default: `""`

### listener\_rules

Description:   A list of maps describing the Listener Rules for this ALB.  
  Required key/values: actions, conditions.  
  Optional key/values: priority, https\_listener\_index (default to https\_listeners[count.index])

Type: `any`

Default: `[]`

### listener\_ssl\_policy\_default

Description: The security policy if using HTTPS externally on the load balancer

Type: `string`

Default: `"ELBSecurityPolicy-2016-08"`

### load\_balancer\_type

Description: The type of load balancer to create. Valid values are application \| network

Type: `string`

Default: `"application"`

### name

Description: The name of the load balancer

Type: `string`

Default: `""`

### scheme

Description: The scheme of the load balancer. Valid values are public \| private

Type: `string`

Default: `""`

### security\_groups

Description: The security groups to attach to the load balancer

Type: `list(string)`

Default: `[]`

### subnet\_mapping

Description: A list of subnet mapping blocks describing subnets to attach to the network load balancer

Type: `list(map(string))`

Default: `[]`

### subnets

Description: A list of subnets to associate with the load balancer

Type: `list(string)`

Default: `[]`

### tags

Description: A map of tags to add to the load balancer

Type: `map(string)`

Default: `{}`

### target\_group\_arn

Description: The arn of the target group

Type: `any`

Default: `""`

### target\_group\_tags

Description: A map of tags to add to all target groups

Type: `map(string)`

Default: `{}`

### target\_groups

Description:   A list of maps containing key/value pairs that define the target groups to be created.  
  Order of these maps is important and the index of these are to be referenced in listener definitions.  
  Required key/values: name, backend\_protocol, backend\_port

Type: `any`

Default: `[]`

### type

Description: The type of load balalncer. Valid values are ALB \| NLB

Type: `string`

Default: `""`

### vpc\_id

Description: VPC id where the load balancer and other resources will be deployed

Type: `string`

Default: `null`

## Outputs

The following outputs are exported:

### http\_tcp\_listener\_arns

Description: The ARN of the TCP and HTTP load balancer listeners created

### http\_tcp\_listener\_ids

Description: The IDs of the TCP and HTTP load balancer listeners created

### https\_listener\_arns

Description: The ARNs of the HTTPS load balancer listeners created

### https\_listener\_ids

Description: The IDs of the load balancer listeners created

### lb\_arn

Description: The ARN of the load balancer

### lb\_arn\_suffix

Description: ARN suffix of the load balancer

### lb\_dns\_name

Description: The DNS name of the load balancer

### lb\_id

Description: The ID of the load balancer

### lb\_zone\_id

Description: The zone\_id of the load balancer

### target\_group\_arn\_suffixes

Description: The ARN suffixes of the target group

### target\_group\_arns

Description: The ARNs of the target groups

### target\_group\_names

Description: Name of the target group.

