##################
# Global variables
##################

variable "aws_account_id" {
  description = "The aws account id that is being deployed into"
  type        = string
}

variable "aws_region" {
  description = "The aws region that is being deployed into"
  type        = string
}

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

###############
# KMS variables
###############

variable "deletion_window_in_days" {
  description = "Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days"
  type        = number
  default     = 30
}

variable "description" {
  description = "The description of the key as viewed in AWS console"
  type        = string
}

variable "enable_key_rotation" {
  description = "Specifies whether key rotation is enabled"
  type        = bool
  default     = false
}

variable "is_enabled" {
  description = "Specifies whether the key is enabled"
  type        = bool
  default     = true
}

variable "key_name" {
  description = "The name of the kms key"
  type        = string
}

variable "key_usage" {
  description = "Specifies the intended use of the key. Valid values: ENCRYPT_DECRYPT or SIGN_VERIFY"
  type        = string
  default     = "ENCRYPT_DECRYPT"
}

variable "operations" {
  description = <<EOF
  A list of operations that the grant permits.
  The permitted values are:
  Decrypt, Encrypt, GenerateDataKey, GenerateDataKeyWithoutPlaintext,
  ReEncryptFrom, ReEncryptTo, CreateGrant, RetireGrant, DescribeKey
  EOF
  type        = list(string)
  default     = ["Encrypt", "Decrypt", "GenerateDataKey", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "CreateGrant", "RetireGrant", "DescribeKey"]
}

variable "policy" {
  description = "The rendered kms policy"
  type        = any
  default     = ""
}

variable "retire_on_delete" {
  description = "If set to false (the default) the grants will be revoked upon deletion, and if set to true the grants will try to be retired upon deletion"
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}
