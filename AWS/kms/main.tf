resource "aws_kms_key" "kms" {
  deletion_window_in_days = var.deletion_window_in_days
  description             = var.description
  enable_key_rotation     = var.enable_key_rotation
  is_enabled              = var.is_enabled
  key_usage               = var.key_usage
  policy                  = var.policy

  tags = merge(
    {
      "Name" = local.key_name
    },
    var.tags
  )

  depends_on = [aws_iam_role.kms]
}

resource "aws_kms_alias" "kms" {
  name = local.alias_name

  target_key_id = aws_kms_key.kms.id
}

resource "aws_kms_grant" "kms" {
  name = local.grant_name

  grantee_principal = aws_iam_role.kms.arn
  key_id            = aws_kms_key.kms.id
  operations        = var.operations
  retire_on_delete  = var.retire_on_delete

  depends_on = [aws_iam_role.kms]
}
