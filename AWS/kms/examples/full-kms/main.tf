module "kms" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/kms/?ref=main"

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  business_unit  = var.business_unit
  environment    = var.environment

  description = "KMS key"
  key_name    = "kms"
  policy      = data.aws_iam_policy_document.kms.json
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}

data "aws_iam_policy_document" "kms" {
  policy_id = "kms-policy"
  version   = "2012-10-17"

  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${var.aws_account_id}:root",
      ]
    }

    actions = [
      "kms:*",
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid    = "Allow access for Key Administrators"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        module.kms.kms_iam_role_arn,
        "arn:aws:iam::${var.aws_account_id}:user/terraformer-read-only"
      ]
    }

    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid    = "Allow use of the key"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        module.kms.kms_iam_role_arn,
        "arn:aws:iam::${var.aws_account_id}:user/terraformer-read-only",
        module.iam-role.iam_role_arn
      ]
    }

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        module.kms.kms_iam_role_arn,
        "arn:aws:iam::${var.aws_account_id}:user/terraformer-read-only",
        module.iam-role.iam_role_arn
      ]
    }

    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]

    resources = [
      "*",
    ]

    condition {
      test     = "Bool"
      values   = ["true"]
      variable = "kms:GrantIsForAWSResource"
    }
  }
}
