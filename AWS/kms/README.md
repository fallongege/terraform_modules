## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### aws\_account\_id

Description: The aws account id that is being deployed into

Type: `string`

### aws\_region

Description: The aws region that is being deployed into

Type: `string`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### description

Description: The description of the key as viewed in AWS console

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### key\_name

Description: The name of the kms key

Type: `string`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

## Optional Inputs

The following input variables are optional (have default values):

### deletion\_window\_in\_days

Description: Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days

Type: `number`

Default: `30`

### enable\_key\_rotation

Description: Specifies whether key rotation is enabled

Type: `bool`

Default: `false`

### is\_enabled

Description: Specifies whether the key is enabled

Type: `bool`

Default: `true`

### key\_usage

Description: Specifies the intended use of the key. Valid values: ENCRYPT\_DECRYPT or SIGN\_VERIFY

Type: `string`

Default: `"ENCRYPT_DECRYPT"`

### operations

Description:   A list of operations that the grant permits.  
  The permitted values are:  
  Decrypt, Encrypt, GenerateDataKey, GenerateDataKeyWithoutPlaintext,  
  ReEncryptFrom, ReEncryptTo, CreateGrant, RetireGrant, DescribeKey

Type: `list(string)`

Default:

```json
[
  "Encrypt",
  "Decrypt",
  "GenerateDataKey",
  "GenerateDataKeyWithoutPlaintext",
  "ReEncryptFrom",
  "ReEncryptTo",
  "CreateGrant",
  "RetireGrant",
  "DescribeKey"
]
```

### policy

Description: The rendered kms policy

Type: `any`

Default: `""`

### retire\_on\_delete

Description: If set to false (the default) the grants will be revoked upon deletion, and if set to true the grants will try to be retired upon deletion

Type: `bool`

Default: `false`

## Outputs

The following outputs are exported:

### kms\_alias\_arn

Description: The ARN of the KMS alias

### kms\_alias\_id

Description: The ID of the KMS alias

### kms\_iam\_role\_arn

Description: The ARN of the kms IAM role

### kms\_key\_arn

Description: The ARN of the KMS key

### kms\_key\_id

Description: The ID of the KMS key

### kms\_key\_name

Description: The Name of the KMS key

### kms\_key\_policy

Description: The Policy of the KMS key

