locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}-${var.key_name}"

  alias_name = "alias/${local.key_name}"
  grant_name = "${local.name_prefix}-kms-grant"
  key_name   = "${local.name_prefix}-kms-key-${var.aws_region}"
  role_name  = "tf-${var.business_unit}-${var.environment}-${var.key_name}-kms-role"
}
