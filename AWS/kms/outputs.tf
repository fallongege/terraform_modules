output "kms_alias_arn" {
  description = "The ARN of the KMS alias"
  value       = concat(aws_kms_alias.kms.*.arn, [""])[0]
}

output "kms_alias_id" {
  description = "The ID of the KMS alias"
  value       = concat(aws_kms_alias.kms.*.arn, [""])[0]
}

output "kms_iam_role_arn" {
  description = "The ARN of the kms IAM role"
  value       = concat(aws_iam_role.kms.*.arn, [""])[0]
}

output "kms_key_arn" {
  description = "The ARN of the KMS key"
  value       = concat(aws_kms_key.kms.*.arn, [""])[0]
}

output "kms_key_id" {
  description = "The ID of the KMS key"
  value       = concat(aws_kms_key.kms.*.id, [""])[0]
}

output "kms_key_name" {
  description = "The Name of the KMS key"
  value       = concat(aws_kms_alias.kms.*.name, [""])[0]
}

output "kms_key_policy" {
  description = "The Policy of the KMS key"
  value       = concat(aws_kms_key.kms.*.policy, [""])[0]
}
