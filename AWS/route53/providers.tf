provider "aws" {
  region      = var.aws_region
  max_retries = "50"

  assume_role {
    role_arn     = "arn:aws:iam::${var.aws_account_id}:role/Devops-Admin"
    session_name = "${var.business_unit}-${var.environment}"
    external_id  = "${var.business_unit}-${var.environment}"
  }
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
