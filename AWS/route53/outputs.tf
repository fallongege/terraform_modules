output "route53_hosted_zone_id" {
  description = "The Route53 Hosted Zone ID"
  value       = [for k, v in aws_route53_zone.route53-zone : v.zone_id]
}

output "route53_zone_name_servers" {
  description = "The Name Servers of the Route53 Hosted Zone"
  value       = [for k, v in aws_route53_zone.route53-zone : v.name_servers]
}

output "route53_record_name" {
  description = "The name of the record"
  value       = [for k, v in aws_route53_record.route53-record : v.name]
}

output "route53_record_fqdn" {
  description = "FQDN built using the zone domain and name"
  value       = [for k, v in aws_route53_record.route53-record : v.fqdn]
}

output "route53_record_name_ignore_changes" {
  description = "The name of the record"
  value       = [for k, v in aws_route53_record.route53-record-ignore-record-changes : v.name]
}

output "route53_record_fqdn_ignore_changes" {
  description = "FQDN built using the zone domain and name"
  value       = [for k, v in aws_route53_record.route53-record-ignore-record-changes : v.fqdn]
}
