## Requirements

The following requirements are needed by this module:

- aws (2.70.0)

## Providers

The following providers are used by this module:

- aws (2.70.0)

## Required Inputs

The following input variables are required:

### aws\_account\_id

Description: The aws account id that is being deployed into

Type: `string`

### aws\_region

Description: The aws region that is being deployed into

Type: `string`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### create\_route53\_record

Description: Whether the Route53 record(s) should be created

Type: `bool`

Default: `false`

### create\_route53\_record\_ignore\_record\_changes

Description: Whether the Route53 record(s) should be created and ignore changes to the record

Type: `bool`

Default: `false`

### create\_route53\_zone

Description: Whether the Route53 hsoted zone should be created

Type: `bool`

Default: `false`

### private\_zone

Description: Whether the Route53 zone is private

Type: `bool`

Default: `false`

### records

Description: List of maps of DNS records

Type: `any`

Default: `[]`

### route53\_zone\_id

Description: The Route53 zone id where the record is created

Type: `any`

Default: `null`

### route53\_zone\_name

Description: The Route53 zone name where the record is created

Type: `any`

Default: `null`

### tags

Description: A map of tags to add to the Route53 hosted zone

Type: `map(string)`

Default: `{}`

### vpc

Description: The vpc configuration for the Route53 hosted zone. Settings are: vpc\_id

Type:

```hcl
object({
    vpc_id = any
  })
```

Default:

```json
{
  "vpc_id": ""
}
```

### zones

Description: Map of Route53 zone parameters

Type: `map(any)`

Default: `{}`

## Outputs

The following outputs are exported:

### route53\_hosted\_zone\_id

Description: The Route53 Hosted Zone ID

### route53\_record\_fqdn

Description: FQDN built using the zone domain and name

### route53\_record\_fqdn\_ignore\_changes

Description: FQDN built using the zone domain and name

### route53\_record\_name

Description: The name of the record

### route53\_record\_name\_ignore\_changes

Description: The name of the record

### route53\_zone\_name\_servers

Description: The Name Servers of the Route53 Hosted Zone

