################
# Global variables
################

variable "aws_account_id" {
  description = "The aws account id that is being deployed into"
  type        = string
}

variable "aws_region" {
  description = "The aws region that is being deployed into"
  type        = string
}

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

##########################
# Route53 Record variables
##########################

variable "create_route53_record" {
  description = "Whether the Route53 record(s) should be created"
  type        = bool
  default     = false
}

variable "create_route53_record_ignore_record_changes" {
  description = "Whether the Route53 record(s) should be created and ignore changes to the record"
  type        = bool
  default     = false
}

variable "private_zone" {
  description = "Whether the Route53 zone is private"
  type        = bool
  default     = false
}

variable "records" {
  description = "List of maps of DNS records"
  type        = any
  default     = []
}

variable "route53_zone_id" {
  description = "The Route53 zone id where the record is created"
  type        = any
  default     = null
}

variable "route53_zone_name" {
  description = "The Route53 zone name where the record is created"
  type        = any
  default     = null
}

########################
# Route53 Zone variables
########################

variable "create_route53_zone" {
  description = "Whether the Route53 hsoted zone should be created"
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to the Route53 hosted zone"
  type        = map(string)
  default     = {}
}

variable "vpc" {
  description = "The vpc configuration for the Route53 hosted zone. Settings are: vpc_id"
  type = object({
    vpc_id = any
  })
  default = {
    vpc_id = ""
  }
}

variable "zones" {
  description = "Map of Route53 zone parameters"
  type        = map(any)
  default     = {}
}
