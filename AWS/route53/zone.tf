resource "aws_route53_zone" "route53-zone" {
  for_each = var.create_route53_zone ? var.zones : {}

  name = each.key

  dynamic "vpc" {
    for_each = length(keys(var.vpc)) == 0 ? [] : [var.vpc]

    content {
      vpc_id = lookup(vpc.value, "vpc_id", null)
    }
  }

  tags = merge(
    {
      "Name" = each.key
    },
    var.tags
  )
}
