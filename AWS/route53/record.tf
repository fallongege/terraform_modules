locals {
  recordsets = { for rs in var.records : "${rs.name} ${rs.type}" => rs }
}

data "aws_route53_zone" "route53-zone" {
  count = (var.create_route53_record || var.create_route53_record_ignore_record_changes) && (var.route53_zone_id != null || var.route53_zone_name != null) ? 1 : 0

  zone_id      = var.route53_zone_id
  name         = var.route53_zone_name
  private_zone = var.private_zone
}

resource "aws_route53_record" "route53-record" {
  for_each = var.create_route53_record && (var.route53_zone_id != null || var.route53_zone_name != null) ? local.recordsets : {}

  zone_id = data.aws_route53_zone.route53-zone[0].zone_id

  name    = each.value.name != "" ? "${each.value.name}.${data.aws_route53_zone.route53-zone[0].name}" : data.aws_route53_zone.route53-zone[0].name
  type    = each.value.type
  ttl     = lookup(each.value, "ttl", null)
  records = lookup(each.value, "records", null)

  dynamic "alias" {
    for_each = length(keys(lookup(each.value, "alias", {}))) == 0 ? [] : [true]

    content {
      name                   = each.value.alias.name
      zone_id                = each.value.alias.zone_id
      evaluate_target_health = lookup(each.value.alias, "evaluate_target_health", false)
    }
  }
}

resource "aws_route53_record" "route53-record-ignore-record-changes" {
  for_each = var.create_route53_record_ignore_record_changes && (var.route53_zone_id != null || var.route53_zone_name != null) ? local.recordsets : {}

  zone_id = data.aws_route53_zone.route53-zone[0].zone_id

  name    = each.value.name != "" ? "${each.value.name}.${data.aws_route53_zone.route53-zone[0].name}" : data.aws_route53_zone.route53-zone[0].name
  type    = each.value.type
  ttl     = lookup(each.value, "ttl", null)
  records = lookup(each.value, "records", null)

  dynamic "alias" {
    for_each = length(keys(lookup(each.value, "alias", {}))) == 0 ? [] : [true]

    content {
      name                   = each.value.alias.name
      zone_id                = each.value.alias.zone_id
      evaluate_target_health = lookup(each.value.alias, "evaluate_target_health", false)
    }
  }

  lifecycle {
    ignore_changes = [records]
  }
}
