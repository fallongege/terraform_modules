variable "aws_region" {
  default = "us-west-2"
}

variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

variable "short_ttl" {
  description = "The ttl for records that need to be updated quickly"
  default     = 300
}

variable "long_ttl" {
  description = "The ttl for records that don't need to be updated quickly"
  default     = 3600
}

variable "pmc_fastly_cname" {
  description = "The CNAME value for Fastly per environment"
  type        = map(string)

  default = {
    stg  = "u2.shared.global.fastly.net" // pmcdev.io
    prod = "a3.shared.global.fastly.net" // pmc.com
  }
}