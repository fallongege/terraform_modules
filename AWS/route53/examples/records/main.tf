locals {
  pmc_zone_id = {
    stg  = "Z1F912Z1W3JCBZ" // pmcdev.io
    prod = "Z2UNEV68ZLOFA6" // pmc.com
  }
}

module "route53" {
  source = "../_modules/AWS/route53"

  aws_account_id = var.account_id
  aws_region     = var.aws_region
  business_unit  = var.business_unit
  environment    = var.environment

  create_route53_record = true
  private_zone          = false
  route53_zone_id       = lookup(local.pmc_zone_id, var.environment)
  records = [
    {
      name = "www"
      type = "CNAME"
      ttl  = var.short_ttl
      records = [
        lookup(var.pmc_fastly_cname, var.environment)
      ]
    },
    {
      name = "admin"
      type = "CNAME"
      ttl  = var.short_ttl
      records = [
        lookup(var.pmc_fastly_cname, var.environment)
      ]
    },
  ]
}
