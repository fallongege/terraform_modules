locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  family_name  = "${local.name_prefix}-${var.service_name}"
  service_name = "${local.name_prefix}-${var.service_name}"
  secret_name  = "tf-${var.business_unit}-${var.environment}-${var.service_name}-secret"

  ecs_dimension = {
    ClusterName = var.ecs_cluster_name
    ServiceName = aws_ecs_service.fargate-service.name
  }

  cloudwatch_alarm = [
    {
      alarm_identifier    = "up"
      alarm_actions       = [join("", aws_appautoscaling_policy.autoscaling-policy-fargate-up.*.arn)]
      comparison_operator = "GreaterThanOrEqualToThreshold"
      metric_name         = "CPUUtilization"
      threshold           = var.scale_up_threshold
    },
    {
      alarm_identifier    = "down"
      alarm_actions       = [join("", aws_appautoscaling_policy.autoscaling-policy-fargate-down.*.arn)]
      comparison_operator = "LessThanOrEqualToThreshold"
      metric_name         = "CPUUtilization"
      threshold           = var.scale_down_threshold
    }
  ]

  port_mappings = var.multiple_port_mappings == true ? var.port_mappings : [{
    containerPort = var.container_port
    hostPort      = var.host_port
    protocol      = var.protocol
  }]

  container_definition = {
    name                   = local.service_name
    image                  = "${var.docker_repo}:${var.image_tag}"
    repositoryCredentials  = var.repository_credentials
    links                  = var.links
    portMappings           = local.port_mappings
    essential              = var.essential
    entryPoint             = var.entrypoint
    command                = var.command
    environment            = "environment_sentinel_value"
    environmentFiles       = var.environment_files
    mountPoints            = var.mount_points
    volumesFrom            = var.volumes_from
    linuxParameters        = var.linux_parameters
    secrets                = "secrets_sentinel_value"
    dependsOn              = var.depends_on_task
    startTimeout           = var.start_timeout
    stopTimeout            = var.stop_timeout
    hostname               = var.hostname
    user                   = var.user
    workingDirectory       = var.working_directory
    disableNetworking      = var.disable_networking
    privileged             = var.privileged
    readonlyRootFilesystem = var.read_only_root_file_system
    dnsServers             = var.dns_servers
    extraHosts             = var.extra_hosts
    dockerSecurityOptions  = var.docker_security_options
    interactive            = var.interactive
    pseudoTerminal         = var.pseudo_terminal
    dockerLabels           = var.docker_labels
    ulimits                = var.ulimits
    logConfiguration = {
      logDriver = var.log_driver
      options   = var.log_options
    }
    healthCheck           = var.health_check
    systemControls        = var.system_controls
    resourceRequirements  = var.resource_requirements
    firelensConfiguration = var.firelens_configuration
    volumes               = var.volumes
    placementConstraints  = var.placement_constraints
    tags                  = var.tags
    pidMode               = var.pid_mode
    ipcMode               = var.ipc_mode
    proxyConfiguration    = var.proxy_configuration
  }

  environment = var.environment_variables
  secrets     = var.secrets
}
