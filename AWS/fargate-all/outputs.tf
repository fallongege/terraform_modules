#########################
# Cloudwatch Logs outputs
#########################

output "cloudwatch_log_group_arn" {
  description = "The ARN of the Cloudwatch log group"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.arn)
}

output "cloudwatch_log_group_id" {
  description = "The ID of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.id)
}

output "cloudwatch_log_group_name" {
  description = "The Name of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.name)
}

output "cloudwatch_log_group_prefix" {
  description = "The Prefix of the Cloudwatch metric alarm"
  value       = length(aws_cloudwatch_log_group.cloudwatch-log-group) > 0 ? aws_cloudwatch_log_group.cloudwatch-log-group[0].id : null
}

#####################
# ECS Service outputs
#####################

output "fargate_service_id" {
  description = "The ID of the Fargate service"
  value       = aws_ecs_service.fargate-service.id
}

output "fargate_service_name" {
  description = "The Name of the Fargate service"
  value       = aws_ecs_service.fargate-service.name
}

#####################
# ECS Task Definition outputs
#####################

locals {
  encoded_environment_variables = jsonencode(local.environment)
  encoded_secrets               = length(local.secrets) > 0 ? jsonencode(local.secrets) : "null"
  encoded_container_definition  = replace(replace(replace(jsonencode(local.container_definition), "/(\\[\\]|\\[\"\"\\]|\"\"|{})/", "null"), "/\"(true|false)\"/", "$1"), "/\"([0-9]+\\.?[0-9]*)\"/", "$1")
  json_map                      = replace(replace(local.encoded_container_definition, "/\"environment_sentinel_value\"/", local.encoded_environment_variables), "/\"secrets_sentinel_value\"/", local.encoded_secrets)
}

output "json" {
  description = "JSON encoded container definitions for use with other terraform resources such as aws_ecs_task_definition."

  value = "[${local.json_map}]"
}

output "json_map" {
  description = "JSON encoded container definitions for use with other terraform resources such as aws_ecs_task_definition."

  value = local.json_map
}

#################
# Route53 outputs
#################

output "route53_record_name" {
  description = "The name of the record"
  value       = [for k, v in aws_route53_record.route53-record : v.name]
}

#########################
# Secrets Manager outputs
#########################

output "secret_name" {
  description = "The ARN of the secret"
  value       = aws_secretsmanager_secret.secret.name
}

output "secret_arn" {
  description = "The ARN of the secret"
  value       = aws_secretsmanager_secret.secret.arn
}
