locals {
  recordsets = { for rs in var.records : "${rs.name} ${rs.type}" => rs }
}

data "aws_route53_zone" "route53-zone" {
  name         = var.route53_zone_name

  zone_id      = var.route53_zone_id
  private_zone = var.private_zone
}

resource "aws_route53_record" "route53-record" {
  for_each = var.route53_zone_id != null || var.route53_zone_name != null ? local.recordsets : {}

  zone_id = data.aws_route53_zone.route53-zone.zone_id

  name    = each.value.name != "" ? "${each.value.name}.${data.aws_route53_zone.route53-zone.name}" : data.aws_route53_zone.route53-zone.name
  type    = each.value.type
  ttl     = lookup(each.value, "ttl", null)
  records = lookup(each.value, "records", null)

  dynamic "alias" {
    for_each = length(keys(lookup(each.value, "alias", {}))) == 0 ? [] : [true]

    content {
      name                   = each.value.alias.name
      zone_id                = each.value.alias.zone_id
      evaluate_target_health = lookup(each.value.alias, "evaluate_target_health", false)
    }
  }
}
