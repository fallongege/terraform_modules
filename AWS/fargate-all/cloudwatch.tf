resource "aws_cloudwatch_metric_alarm" "cloudwatch-alarm" {
  count = var.create_metric_alarm ? length(local.cloudwatch_alarm) : 0

  alarm_name = "${local.service_name}-${lookup(local.cloudwatch_alarm[count.index], "metric_name")}-${lookup(local.cloudwatch_alarm[count.index], "alarm_identifier")}"

  actions_enabled                       = var.actions_enabled
  alarm_actions                         = lookup(local.cloudwatch_alarm[count.index], "alarm_actions")
  alarm_description                     = "Alarm is triggered when ${var.namespace} ${lookup(local.cloudwatch_alarm[count.index], "metric_name")} is ${lookup(local.cloudwatch_alarm[count.index], "comparison_operator")} ${lookup(local.cloudwatch_alarm[count.index], "threshold")}"
  comparison_operator                   = lookup(local.cloudwatch_alarm[count.index], "comparison_operator")
  datapoints_to_alarm                   = var.datapoints_to_alarm
  evaluate_low_sample_count_percentiles = var.evaluate_low_sample_count_percentiles
  evaluation_periods                    = var.evaluation_periods
  insufficient_data_actions             = var.insufficient_data_actions
  metric_name                           = lookup(local.cloudwatch_alarm[count.index], "metric_name")
  namespace                             = var.namespace
  ok_actions                            = var.ok_actions
  period                                = var.period
  statistic                             = var.statistic
  threshold                             = lookup(local.cloudwatch_alarm[count.index], "threshold")
  treat_missing_data                    = var.treat_missing_data
  unit                                  = var.unit

  dimensions = local.ecs_dimension

  tags = merge(
  {
    "Name" = "${local.service_name}-${lookup(local.cloudwatch_alarm[count.index], "metric_name")}-${lookup(local.cloudwatch_alarm[count.index], "alarm_identifier")}"
  },
  var.tags
  )
}

resource "aws_cloudwatch_log_group" "cloudwatch-log-group" {
  count = length(var.cloudwatch_log_group)

  name = "${lookup(var.cloudwatch_log_group[count.index], "cloudwatch_stream_prefix")}${local.service_name}"

  retention_in_days = lookup(var.cloudwatch_log_group[count.index], "cloudwatch_retention")

  tags = merge(
  {
    "Name" = "${lookup(var.cloudwatch_log_group[count.index], "cloudwatch_stream_prefix")}${local.service_name}"
  },
  var.tags
  )
}
