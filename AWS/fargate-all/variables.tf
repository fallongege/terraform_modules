##################
# Global variables
##################

variable "aws_region" {
  description = "The aws region that is being deployed into"
  type        = string
}

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Locals
################

variable "service_name" {
  description = "The name of the service that is being monitored by the Cloudwatch alarm"
  type        = string
}

############################
# Cloudwatch Alarm variables
############################

variable "actions_enabled" {
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state"
  type        = bool
  default     = true
}

variable "create_metric_alarm" {
  description = "Whether to create the Cloudwatch metric alarm"
  type        = bool
  default     = false
}

variable "datapoints_to_alarm" {
  description = "The number of datapoints that must be breaching to trigger the alarm"
  type        = number
  default     = null
}

variable "dimensions" {
  description = "The dimensions for the alarm's associated metric"
  type        = any
  default     = null
}

variable "evaluate_low_sample_count_percentiles" {
  description = <<EOF
  Used only for alarms based on percentiles. If you specify ignore, the alarm state will not change during periods with
  too few data points to be statistically significant. If you specify evaluate or omit this parameter, the alarm will
  always be evaluated and possibly change state no matter how many data points are available. The following values are
  supported: ignore | evaluate
  EOF
  type        = string
  default     = null
}

variable "evaluation_periods" {
  description = "How often the alarm should be evaluated"
  type        = number
  default     = 1
}

variable "insufficient_data_actions" {
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state"
  type        = list(string)
  default     = null
}

variable "namespace" {
  description = "The namespace to alert on"
  type        = string
  default     = "AWS/ECS"
}

variable "ok_actions" {
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state"
  type        = list(string)
  default     = null
}

variable "period" {
  description = "The period in seconds to evaluate for the cloudwatch alarm"
  type        = number
  default     = 300
}

variable "scale_down_threshold" {
  description = "The threshold at which to scale the service down"
  type        = number
  default     = 40
}

variable "scale_up_threshold" {
  description = "The threshold at which to scale the service up"
  type        = number
  default     = 80
}

variable "statistic" {
  description = "The statistic to apply to the alarm's associated metric"
  type        = string
  default     = "Average"
}

variable "treat_missing_data" {
  description = "Sets how the alarm handles missing data points. Valid options are: missing | ignore | breaching | notBreaching"
  type        = string
  default     = "missing"
}

variable "unit" {
  description = "The unit for the alarm's associated metric"
  type        = string
  default     = null
}

############################
# Cloudwatch Logs variables
############################

variable "cloudwatch_log_group" {
  description = "List of Cloudwatch log group settings including: cloudwatch_retention, cloudwatch_stream_prefix"
  type        = list(map(string))
  default     = []
}

#######################
# Autoscaling variables
#######################

variable "adjustment_type" {
  description = "Specifies whether the adjustment is an absolute number or a percentage of the current capacity"
  type        = string
  default     = "ChangeInCapacity"
}

variable "cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start"
  type        = number
  default     = 60
}

variable "ecs_cluster_name" {
  description = "The name of the ECS/Fargate cluster"
  type        = any
  default     = ""
}

variable "metric_aggregation_type" {
  description = "The aggregation type for the policy's metrics. Valid values are Minimum, Maximum, and Average"
  type        = string
  default     = "Maximum"
}

variable "metric_interval_lower_bound" {
  description = "The lower bound for the difference between the alarm threshold and the CloudWatch metric"
  type        = number
  default     = 0
}

variable "metric_interval_upper_bound" {
  description = "The upper bound for the difference between the alarm threshold and the CloudWatch metric"
  type        = number
  default     = 0
}

variable "scaling_adjustment_down" {
  description = "The number of instances by which to scale down"
  type        = number
  default     = -1
}

variable "scaling_adjustment_up" {
  description = "The number of instances by which to scale up"
  type        = number
  default     = 1
}

###############################
# ECS Task Definition variables
###############################

variable "command" {
  description = "A string array representing the command that the container runs to determine if it is healthy"
  type        = list(string)
  default     = []
}

variable "container_port" {
  description = "The port that the container is running on"
  type        = number
  default     = 0
}

variable "depends_on_task" {
  description = "The dependencies defined for container startup and shutdown"
  type        = any
  default     = ""
}

variable "disable_networking" {
  description = "When this parameter is true, networking is disabled within the container"
  type        = bool
  default     = false
}

variable "dns_servers" {
  description = "A list of DNS servers that are presented to the container"
  type        = list(string)
  default     = []
}

variable "docker_labels" {
  description = "A key/value map of labels to add to the container"
  type        = map(string)
  default     = {}
}

variable "docker_repo" {
  description = "The name of the docker repository where the image is stored"
  type        = string
  default     = ""
}

variable "docker_security_options" {
  description = "A list of strings to provide custom labels for SELinux and AppArmor multi-level security systems"
  type        = list(map(string))
  default     = []
}

variable "entrypoint" {
  description = "The entry point that is passed to the container"
  type        = list(string)
  default     = []
}

variable "environment_files" {
  description = "A list of files containing the environment variables to pass to a container"
  type        = list(map(string))
  default     = []
}

variable "environment_variables" {
  description = "The environment variables to pass to a container"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "essential" {
  description = <<EOF
  If the essential parameter of a container is marked as true, and that container fails or stops for any reason,
  all other containers that are part of the task are stopped. If the essential parameter of a container is marked
  as false, then its failure does not affect the rest of the containers in a task.
  EOF
  type        = bool
  default     = true
}

variable "extra_hosts" {
  description = "A list of hostnames and IP address mappings to append to the /etc/hosts file on the container"
  type        = list(map(string))
  default     = []
}

variable "firelens_configuration" {
  description = "The FireLens configuration for the container"
  type        = any
  default     = []
}

variable "health_check" {
  description = "The container health check command and associated configuration parameters for the container"
  type        = list(string)
  default     = []
}

variable "host_port" {
  description = "The port number on the container instance to reserve for your container."
  type        = number
  default     = 0
}

variable "hostname" {
  description = "The hostname to use for your container"
  type        = string
  default     = ""
}

variable "image_tag" {
  description = "The tag of the docker image used to start the container"
  type        = string
  default     = ""
}

variable "interactive" {
  description = <<EOF
  When this parameter is true, this allows you to deploy containerized applications
  that require stdin or a tty to be allocated.
  EOF
  type        = bool
  default     = true
}

variable "ipc_mode" {
  description = <<EOF
  The IPC resource namespace to use for the containers in the task. The valid values are host, task, or none.
  If host is specified, then all containers within the tasks that specified the host IPC mode on the same container
  instance share the same IPC resources with the host Amazon EC2 instance. If task is specified, all containers within
  the specified task share the same IPC resources. If none is specified, then IPC resources within the containers of a
  task are private and not shared with other containers in a task or on the container instance.
  EOF
  type        = string
  default     = "none"
}

variable "links" {
  description = "Whether to allow containers to communicate with each other without the need for port mappings"
  type        = list(string)
  default     = []
}

variable "linux_parameters" {
  description = "Linux-specific options that are applied to the container, such as KernelCapabilities."
  type        = any
  default     = {}
}

variable "log_driver" {
  description = <<EOF
  The log driver to use for the container.
  Valid values: "awslogs","fluentd","gelf","json-file","journald","logentries",
  "splunk","syslog","awsfirelens
  If using Fargate launch type, only supported value is awslogs.
  EOF
  type        = string
  default     = "awslogs"
}

variable "log_options" {
  description = "The configuration options to send to the `log_driver`"
  type = object({
    awslogs-region        = any
    awslogs-group         = any
    awslogs-stream-prefix = any
  })
  default = {
    "awslogs-region"        = "us-west-2"
    "awslogs-group"         = "default"
    "awslogs-stream-prefix" = "default"
  }
}

variable "mount_points" {
  description = "The mount points for data volumes in your container"
  type        = list(map(string))
  default     = []
}

variable "multiple_port_mappings" {
  description = "If multiple port mappings are needed"
  type        = bool
  default     = false
}

variable "pid_mode" {
  description = <<EOF
  The process namespace to use for the containers in the task. The valid values are host or task.
  If host is specified, then all containers within the tasks that specified the host PID mode on the same container
  instance share the same process namespace with the host Amazon EC2 instance. If task is specified,
  all containers within the specified task share the same process namespace.
  EOF
  type        = string
  default     = "task"
}

variable "placement_constraints" {
  description = "The contstraints that customize how Amazon ECS places tasks"
  type        = list(string)
  default     = []
}

variable "port_mappings" {
  description = "The port mappings, if there are multiple port mappings"
  type        = list(map(string))
  default     = []
}

variable "privileged" {
  description = "When this parameter is true, the container is given elevated privileges on the host container instance"
  type        = bool
  default     = false
}

variable "protocol" {
  description = "The protocol used for the port mapping. Valid values are: tcp | udp"
  type        = string
  default     = "tcp"
}

variable "proxy_configuration" {
  description = "The configuration details for the App Mesh proxy."
  type        = any
  default     = []
}

variable "pseudo_terminal" {
  description = "When this parameter is true, a TTY is allocated"
  type        = bool
  default     = false
}

variable "read_only_root_file_system" {
  description = "When this parameter is true, the container is given read-only access to its root file system"
  type        = bool
  default     = false
}

variable "repository_credentials" {
  description = "The credentials that are used to authenticate with the docker repo"
  type        = map(any)
  default     = {}
}

variable "resource_requirements" {
  description = "The type and amount of a resource to assign to a container"
  type        = list(string)
  default     = []
}

variable "secrets" {
  description = "An object representing the secret to expose to your container"
  type        = list(map(string))
  default     = []
}

variable "start_timeout" {
  description = "Time duration (in seconds) to wait before giving up on resolving dependencies for a container"
  type        = number
  default     = 180
}

variable "stop_timeout" {
  description = "Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on its own"
  type        = number
  default     = 30
}

variable "system_controls" {
  description = "A list of namespaced kernel parameters to set in the container"
  type        = list(map(string))
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "ulimits" {
  description = "A list of ulimits to set in the container"
  type        = list(map(any))
  default     = []
}

variable "user" {
  description = "The user name to use inside the container."
  type        = string
  default     = ""
}

variable "volume" {
  description = "The volume configuration for the ecs task definition. Settings are: name, file_system_id, transit_encryption, access_point_id, iam"
  type        = list(any)
  default     = []
}

variable "volumes" {
  description = "A list of volumes to be passed to the Docker daemon on a container instance"
  type        = list(map(string))
  default     = []
}

variable "volumes_from" {
  description = "Data volumes to mount from another container"
  type        = map(string)
  default     = {}
}

variable "working_directory" {
  description = "The working directory in which to run commands inside the container"
  type        = string
  default     = ""
}

#######################
# ECS Service variables
#######################

variable "cluster_arn" {
  description = "The arn of the ecs/fargate cluster"
  type        = any
}

variable "cpu" {
  description = "The number of cpu units that the container needs"
  type        = number
}

variable "desired_count" {
  description = "The desired number of containers to be running"
  type        = number
}

variable "enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
  type        = bool
  default     = true
}

variable "execution_role_arn" {
  description = <<EOF
  The Amazon Resource Name (ARN) of the task execution role that grants the Amazon ECS container agent permission to
  make AWS API calls on your behalf
  EOF
  type        = any
}

variable "health_check_grace_period_seconds" {
  description = "Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown"
  type        = number
  default     = 120
}

variable "launch_type" {
  description = "The launch type on which to run your service. Valid options are EC2 | FARGATE"
  type        = string
  default     = "FARGATE"
}

variable "load_balancer" {
  description = "The load balancer configuration for the ecs service. Settings are: target_group_arn, container_name, container_port"
  type = list(object({
    target_group_arn = string
    container_name   = string
    container_port   = number
  }))
  default = []
}

variable "max_capacity" {
  description = "The max capacity of the scalable target"
  type        = number
}

variable "memory" {
  description = "The amount (in MiB) of memory to present to the container"
  type        = number
}

variable "min_capacity" {
  description = "The min capacity of the scalable target"
  type        = number
}

variable "network_configuration" {
  description = "The network configuration for the ecs service. Settings are: subnets, security_groups"
  type = list(object({
    assign_public_ip = any
    subnets          = any
    security_groups  = any
  }))
  default = []
}

variable "network_mode" {
  description = "The Docker networking mode to use for the containers in the task. Valid options are none | bridge | awsvpc | host"
  type        = string
  default     = "awsvpc"
}

variable "platform_version" {
  description = "The platform version on which to run your service. Only applicable for launch_type set to FARGATE"
  type        = string
  default     = "1.4.0"
}

variable "task_role_arn" {
  description = "The IAM role that allows the containers in the task permission to call the AWS APIs"
  type        = string
}

##########################
# Route53 Record variables
##########################

variable "private_zone" {
  description = "Whether the Route53 zone is private"
  type        = bool
  default     = false
}

variable "records" {
  description = "List of maps of DNS records"
  type        = any
  default     = []
}

variable "route53_zone_id" {
  description = "The Route53 zone id where the record is created"
  type        = any
  default     = null
}

variable "route53_zone_name" {
  description = "The Route53 zone name where the record is created"
  type        = any
  default     = null
}

###################
# Secrets variables
###################

variable "kms_key_id" {
  description = "The ID of the KMS key used to encrypt the secret"
  type        = string
}

variable "recovery_window_in_days" {
  description = "Specifies the number of days that AWS Secrets Manager waits before it can delete the secret"
  type        = number
  default     = 30
}
