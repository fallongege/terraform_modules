resource "aws_appautoscaling_target" "autoscaling-target-fargate" {
  max_capacity       = var.max_capacity
  min_capacity       = var.min_capacity
  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  depends_on = [aws_ecs_service.fargate-service]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-fargate-up" {
  name = "${local.family_name}-up"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = var.adjustment_type
    cooldown                = var.cooldown
    metric_aggregation_type = var.metric_aggregation_type

    step_adjustment {
      metric_interval_lower_bound = var.metric_interval_lower_bound
      scaling_adjustment          = var.scaling_adjustment_up
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-fargate]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-fargate-down" {
  name = "${local.family_name}-down"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = var.adjustment_type
    cooldown                = var.cooldown
    metric_aggregation_type = var.metric_aggregation_type

    step_adjustment {
      metric_interval_upper_bound = var.metric_interval_upper_bound
      scaling_adjustment          = var.scaling_adjustment_down
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-fargate]
}
