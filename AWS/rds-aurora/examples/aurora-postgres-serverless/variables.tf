variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

##################
# Aurora Variables
##################
variable "aurora_serverless_auto_pause" {
  description = "A map of the environments specifying if auto pause is enabled"
  type        = map(bool)

  default = {
    stg  = true
    prod = false
  }
}

variable "aurora_serverless_min_capacity" {
  description = "A map of the environments specifying minimum serverless capacity units"
  type        = map(number)

  default = {
    stg  = 2
    prod = 2
  }
}

variable "aurora_serverless_max_capacity" {
  description = "A map of the environments specifying the maximum serverless capacity units"
  type        = map(string)

  default = {
    stg  = 8
    prod = 16
  }
}
