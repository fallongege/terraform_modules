module "aurora-microservices-postgres-secret" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/secrets-manager/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  description = "The username and password of the ${local.name_prefix}-aurora-ms-postgres-secret-cluster"
  kms_key_id  = module.kms.kms_key_id
  secret_name = "${local.name_prefix}-aurora-ms-postgres-secret"
}

module "aurora-microservices-postgres-sg" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/security-group/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  name = "${local.name_prefix}-aurora-ms-postgres"

  create_sg   = true
  description = "Security group that is attached to the ${local.name_prefix}-aurora-ms-postgres-cluster"
  vpc_id      = data.terraform_remote_state.base.outputs.vpc_id
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }

  create_ingress_with_cidr_blocks = true
  ingress_with_cidr_blocks = [
    {
      rule        = "All from vpc cidr"
      cidr_blocks = data.terraform_remote_state.base.outputs.vpc_cidr_block
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "all from vpc cidr"
    },
  ]

  create_egress_with_cidr_blocks = true
  egress_with_cidr_blocks = [
    {
      rule        = "Allow all egress traffic to vpc cidr"
      cidr_blocks = data.terraform_remote_state.base.outputs.vpc_cidr_block
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "Allow all egress traffic to vpc cidr"
    }
  ]
}

locals {
  aurora_postgres_master_username = lookup(var.database_username, format("aurora-postgres%s%s", "-", var.environment))
}

module "aurora-microservices-postgres" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/rds-aurora/?ref=feature/WI-2372"

  business_unit = var.business_unit
  environment   = var.environment

  availability_zones       = slice(data.aws_availability_zones.azs.names, 0, 3)
  backup_retention_period  = lookup(var.backup_retention_period, var.environment)
  cluster_family           = "aurora-postgresql10"
  cluster_identifier       = "aurora-ms-postgres"
  database_name            = "defaultdb"
  deletion_protection      = lookup(var.deletion_protection, var.environment)
  engine                   = "aurora-postgresql"
  engine_mode              = "serverless"
  engine_version           = "10.7"
  local_exec_command       = <<EOT
      DB_PASS=$(openssl rand -base64 16)
      DB_USER=${local.aurora_postgres_master_username}
      SECRET_STRING="{\"username\":\"$${DB_USER}\",\"password\":\"$${DB_PASS}\"}"
      assume_role=$(aws sts assume-role --role-arn arn:aws:iam::${var.aws_account_id}:role/Devops-Admin --role-session-name ${var.business_unit}-${var.environment})
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
      unset AWS_SESSION_TOKEN
      export AWS_ACCESS_KEY_ID=$(echo $assume_role | jq -r '.Credentials.AccessKeyId')
      export AWS_SECRET_ACCESS_KEY=$(echo $assume_role | jq -r '.Credentials.SecretAccessKey')
      export AWS_SESSION_TOKEN=$(echo $assume_role | jq -r '.Credentials.SessionToken')
      aws secretsmanager put-secret-value --secret-id ${module.aurora-microservices-postgres-secret.secret_name} --region ${var.aws_region} --secret-string $SECRET_STRING
      aws docdb modify-db-cluster --db-cluster-identifier ${local.name_prefix}-aurora-ms-postgres-cluster --master-user-password $DB_PASS --apply-immediately --region ${var.aws_region}
    EOT
  master_username          = local.aurora_postgres_master_username
  master_password          = "password"
  rds_parameter_group_name = "aurora-postgres107"
  rds_subnet_group_name    = "aurora-postgres-subnet-group"
  scaling_configuration = {
    auto_pause               = lookup(var.aurora_serverless_auto_pause, var.environment)
    min_capacity             = lookup(var.aurora_serverless_min_capacity, var.environment)
    max_capacity             = lookup(var.aurora_serverless_max_capacity, var.environment)
    seconds_until_auto_pause = 600
    timeout_action           = "ForceApplyCapacityChange"
  }
  subnet_ids = data.terraform_remote_state.base.outputs.database_subnets
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  vpc_security_group_ids = [module.aurora-microservices-postgres-sg.security_group_id]
}
