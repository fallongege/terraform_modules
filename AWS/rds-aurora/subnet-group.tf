resource "aws_db_subnet_group" "rds-subnet-group" {
  name = local.rds_subnet_group_name

  description = local.rds_subnet_group_name
  subnet_ids  = var.subnet_ids

  tags = merge(
    {
      "Name" = local.rds_subnet_group_name
    },
    var.tags
  )
}
