locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  cluster_identifier       = "${local.name_prefix}-${var.cluster_identifier}-cluster"
  rds_parameter_group_name = "${local.name_prefix}-${var.rds_parameter_group_name}"
  rds_subnet_group_name    = "${local.name_prefix}-${var.rds_subnet_group_name}"

  backtrack_window             = (var.engine == "aurora-mysql" || var.engine == "aurora") && var.engine_mode != "serverless" ? var.backtrack_window : 0
  port                         = var.port == "" ? var.engine == "aurora-postgresql" ? "5432" : "3306" : var.port
  rds_enhanced_monitoring_arn  = var.create_monitoring_role ? join("", aws_iam_role.rds_enhanced_monitoring.*.arn) : var.monitoring_role_arn
  rds_enhanced_monitoring_name = join("", aws_iam_role.rds_enhanced_monitoring.*.name)
}
