resource "aws_rds_cluster" "rds" {
  cluster_identifier        = local.cluster_identifier
  global_cluster_identifier = var.global_cluster_identifier

  apply_immediately                   = var.apply_immediately
  availability_zones                  = var.availability_zones
  backtrack_window                    = local.backtrack_window
  backup_retention_period             = var.backup_retention_period
  database_name                       = var.database_name
  copy_tags_to_snapshot               = var.copy_tags_to_snapshot
  db_cluster_parameter_group_name     = join("", aws_rds_cluster_parameter_group.rds.*.name)
  db_subnet_group_name                = join("", aws_db_subnet_group.rds-subnet-group.*.name)
  deletion_protection                 = var.deletion_protection
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports
  enable_http_endpoint                = var.enable_http_endpoint
  engine                              = var.engine
  engine_mode                         = var.engine_mode
  engine_version                      = var.engine_version
  final_snapshot_identifier           = "${local.cluster_identifier}-final-snapshot-${random_id.snapshot_identifier.hex}"
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  iam_roles                           = var.iam_roles
  kms_key_id                          = var.kms_key_id
  master_username                     = var.master_username
  master_password                     = var.master_password
  port                                = local.port
  preferred_backup_window             = var.preferred_backup_window
  preferred_maintenance_window        = var.preferred_maintenance_window
  replication_source_identifier       = var.replication_source_identifier
  skip_final_snapshot                 = var.skip_final_snapshot
  snapshot_identifier                 = var.snapshot_identifier
  storage_encrypted                   = var.storage_encrypted
  source_region                       = var.source_region
  vpc_security_group_ids              = [join("", var.vpc_security_group_ids)]

  dynamic "scaling_configuration" {
    for_each = length(keys(var.scaling_configuration)) == 0 ? [] : [var.scaling_configuration]

    content {
      auto_pause               = lookup(scaling_configuration.value, "auto_pause", null)
      max_capacity             = lookup(scaling_configuration.value, "max_capacity", null)
      min_capacity             = lookup(scaling_configuration.value, "min_capacity", null)
      seconds_until_auto_pause = lookup(scaling_configuration.value, "seconds_until_auto_pause", null)
      timeout_action           = lookup(scaling_configuration.value, "timeout_action", null)
    }
  }

  tags = merge(
    {
      "Name" = local.cluster_identifier
    },
    var.tags
  )

  lifecycle {
    ignore_changes = [master_password]
  }

  provisioner "local-exec" {
    command     = var.local_exec_command
    interpreter = ["/bin/bash", "-c"]
  }
}

resource "aws_rds_cluster_instance" "rds" {
  count = var.replica_scale_enabled ? var.replica_scale_min : var.replica_count

  identifier = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "instance_name", "${local.name_prefix}-${var.cluster_identifier}-${count.index}") : "${local.name_prefix}-${var.cluster_identifier}-${count.index}"

  auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  apply_immediately               = var.apply_immediately
  ca_cert_identifier              = var.ca_cert_identifier
  cluster_identifier              = aws_rds_cluster.rds.id
  db_parameter_group_name         = join("", aws_rds_cluster_parameter_group.rds.*.name)
  db_subnet_group_name            = join("", aws_db_subnet_group.rds-subnet-group.*.name)
  engine                          = var.engine
  engine_version                  = var.engine_version
  instance_class                  = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "instance_type", var.instance_type) : count.index > 0 ? coalesce(var.instance_type_replica, var.instance_type) : var.instance_type
  monitoring_interval             = var.monitoring_interval
  monitoring_role_arn             = local.rds_enhanced_monitoring_arn
  performance_insights_enabled    = var.performance_insights_enabled
  performance_insights_kms_key_id = var.performance_insights_kms_key_id
  preferred_maintenance_window    = var.preferred_maintenance_window
  promotion_tier                  = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "instance_promotion_tier", count.index) : count.index
  publicly_accessible             = length(var.instances_parameters) > count.index ? lookup(var.instances_parameters[count.index], "publicly_accessible", var.publicly_accessible) : var.publicly_accessible

  tags = var.tags
}

resource "random_id" "snapshot_identifier" {
  keepers = {
    id = local.cluster_identifier != "" ? local.cluster_identifier : var.global_cluster_identifier
  }

  byte_length = 4
}
