output "rds_cluster_arn" {
  description = "The ARN of the RDS cluster"
  value       = aws_rds_cluster.rds.arn
}

output "rds_cluster_database_name" {
  description = "The name of the default database"
  value       = var.database_name
}

output "rds_cluster_endpoint" {
  description = "The endpoint of the RDS cluster"
  value       = aws_rds_cluster.rds.endpoint
}

output "rds_cluster_hosted_zone_id" {
  description = "Route53 hosted zone id of the created cluster"
  value       = aws_rds_cluster.rds.hosted_zone_id
}

output "rds_cluster_id" {
  description = "The ID of the RDS cluster"
  value       = aws_rds_cluster.rds.id
}

output "rds_cluster_instance_arns" {
  description = "A list of all cluster instance ids"
  value       = aws_rds_cluster_instance.rds.*.arn
}

output "rds_cluster_instance_endpoints" {
  description = "A list of all cluster instance endpoints"
  value       = aws_rds_cluster_instance.rds.*.endpoint
}

output "rds_cluster_instance_ids" {
  description = "A list of all cluster instance ids"
  value       = aws_rds_cluster_instance.rds.*.id
}

output "rds_cluster_master_username" {
  description = "The master username of the RDS cluster"
  value       = aws_rds_cluster.rds.master_username
  sensitive   = true
}

output "rds_cluster_port" {
  description = "The port of the RDS cluster"
  value       = aws_rds_cluster.rds.port
}

output "rds_cluster_reader_endpoint" {
  description = "The reader endpoint of the RDS cluster"
  value       = aws_rds_cluster.rds.reader_endpoint
}

output "rds_cluster_resource_id" {
  description = "The Resource ID of the RDS cluster"
  value       = aws_rds_cluster.rds.cluster_resource_id
}
