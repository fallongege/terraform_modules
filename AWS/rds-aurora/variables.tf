##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

######################
# Aurora variables
######################

variable "apply_immediately" {
  description = "Whether any cluster modifications are applied immediately, or during the next maintenance window"
  type        = bool
  default     = true
}

variable "auto_minor_version_upgrade" {
  description = "Whether minor engine upgrades will be performed automatically in the maintenance window"
  type        = bool
  default     = true
}

variable "availability_zones" {
  description = "A list of vailability zone IDs. The maximum number of AZ's that can be specified is 3"
  type        = list(string)
  default     = []
}

variable "backtrack_window" {
  description = "The target backtrack window, in seconds. Only available for aurora engine currently. To disable backtracking, set this value to 0. Defaults to 0. Must be between 0 and 259200 (72 hours)"
  type        = number
  default     = 0
}

variable "backup_retention_period" {
  description = "The number of days to retain backups for"
  type        = number
  default     = 7
}

variable "ca_cert_identifier" {
  description = "The identifier of the CA certificate for the DB instance"
  type        = string
  default     = "rds-ca-2019"
}

variable "cluster_family" {
  description = "The family of the Aurora cluster parameter group"
  type        = string
  default     = ""
}

variable "cluster_identifier" {
  description = "The identifier (name) of the cluster"
  type        = string
  default     = ""
}

variable "cluster_parameters" {
  description = "List of DB parameters to apply"
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = []
}

variable "copy_tags_to_snapshot" {
  description = "Whether to copy all Cluster tags to snapshots"
  type        = bool
  default     = true
}

variable "database_name" {
  description = "Name for an automatically created database on cluster creation"
  type        = string
  default     = ""
}

variable "deletion_protection" {
  description = "Whether the Aurora clsuter should have deletion protection enabled"
  type        = bool
  default     = false
}

variable "enable_http_endpoint" {
  description = "Whetherto enable the Data API for a serverless Aurora database engine"
  type        = bool
  default     = false
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: audit, profiler"
  type        = list(string)
  default     = []
}

variable "engine" {
  description = "The Aurora database engine type. Valid values are: aurora, aurora-mysql or aurora-postgresql"
  type        = string
  default     = "aurora"
}

variable "engine_mode" {
  description = "The database engine mode. Valid values: global, parallelquery, provisioned, serverless, multimaster."
  type        = string
  default     = "provisioned"
}

variable "engine_version" {
  description = "The version number of the Aurora database engine to use"
  type        = string
  default     = "5.6.10a"
}

variable "global_cluster_identifier" {
  description = "The global cluster identifier specified on aws_rds_global_cluster"
  type        = string
  default     = ""
}

variable "iam_database_authentication_enabled" {
  description = "Specifies whether IAM Database authentication should be enabled or not. Not all versions and instances are supported. Refer to the AWS documentation to see which versions are supported."
  type        = bool
  default     = false
}

variable "iam_roles" {
  description = "A List of ARNs for the IAM roles to associate to the RDS Cluster."
  type        = list(string)
  default     = []
}

variable "instances_parameters" {
  description = "Customized instance settings. Supported keys: instance_name, instance_type, instance_promotion_tier, publicly_accessible"
  type        = list(map(string))
  default     = []
}

variable "instance_type" {
  description = "Instance type to use at master instance. If instance_type_replica is not set it will use the same type for replica instances"
  type        = string
  default     = null
}

variable "instance_type_replica" {
  description = "Instance type to use at replica instance"
  type        = string
  default     = null
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying `kms_key_id`, `storage_encrypted` needs to be set to `true`"
  type        = any
  default     = ""
}

variable "local_exec_command" {
  description = "The local exec command that is to be executed on Aurora cluster creation, if needed"
  type        = any
  default     = ""
}

variable "master_username" {
  description = "Required unless a snapshot_identifier is provided. Username for the master DB user"
  type        = string
  default     = ""
}

variable "master_password" {
  description = "Required unless a snapshot_identifier is provided. Password for the master DB user"
  type        = string
  default     = ""
}

variable "performance_insights_enabled" {
  description = "Whether Performance Insights are enabled"
  type        = bool
  default     = false
}

variable "performance_insights_kms_key_id" {
  description = "The ARN for the KMS key to encrypt Performance Insights data"
  type        = string
  default     = ""
}

variable "predefined_metric_type" {
  description = "The metric type to scale on. Valid values are RDSReaderAverageCPUUtilization and RDSReaderAverageDatabaseConnections."
  type        = string
  default     = "RDSReaderAverageCPUUtilization"
}

variable "port" {
  description = "The port of the Aurora cluster"
  type        = string
  default     = ""
}

variable "preferred_backup_window" {
  description = "Daily time range during which the backups happen"
  type        = string
  default     = "07:00-09:00"
}

variable "preferred_maintenance_window" {
  description = "When to perform DB maintenance"
  type        = string
  default     = "sun:05:00-sun:06:00"
}

variable "publicly_accessible" {
  description = "Whether the DB should have a public IP address"
  type        = bool
  default     = false
}

variable "replica_count" {
  description = "Number of reader nodes to create.  If `replica_scale_enable` is `true`, the value of `replica_scale_min` is used instead."
  default     = 0
}

variable "replica_scale_connections" {
  description = "Average number of connections to trigger autoscaling at. Default value is 70% of db.r4.large's default max_connections"
  type        = number
  default     = 700
}

variable "replica_scale_cpu" {
  description = "CPU usage to trigger autoscaling at"
  type        = number
  default     = 70
}

variable "replica_scale_in_cooldown" {
  description = "Cooldown in seconds before allowing further scaling operations after a scale in"
  type        = number
  default     = 300
}

variable "replica_scale_out_cooldown" {
  description = "Cooldown in seconds before allowing further scaling operations after a scale out"
  type        = number
  default     = 300
}

variable "replica_scale_min" {
  description = "Minimum number of replicas to allow scaling for"
  type        = number
  default     = 2
}

variable "replica_scale_max" {
  description = "Maximum number of replicas to allow scaling for"
  type        = number
  default     = 4
}

variable "replica_scale_enabled" {
  description = "Whether to enable autoscaling for RDS Aurora (MySQL) read replicas"
  type        = bool
  default     = false
}

variable "replication_source_identifier" {
  description = "The ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica."
  default     = ""
}

variable "rds_parameter_group_name" {
  description = "The family of the RDS cluster parameter group"
  type        = string
  default     = ""
}

variable "rds_subnet_group_name" {
  description = "The name of the Aurora subnet group"
  type        = string
  default     = ""
}

variable "scaling_configuration" {
  description = "Map of nested attributes with scaling properties. Only valid when engine_mode is set to `serverless`. Valid values are: auto_pause, max_capacity, min_capacity, seconds_until_auto_pause, timeout_action"
  type        = map(string)
  default     = {}
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the Aurora cluster is deleted"
  type        = bool
  default     = false
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot"
  type        = string
  default     = ""
}

variable "source_region" {
  description = "The source region for an encrypted replica DB cluster"
  default     = ""
}

variable "storage_encrypted" {
  description = "Specifies whether the Aurora cluster is encrypted"
  type        = bool
  default     = true
}

variable "subnet_ids" {
  description = "List of VPC subnet IDs to place Aurora instances in"
  type        = any
  default     = []
}

variable "tags" {
  description = "A map of tags to add to the Aurora resources"
  type        = map(string)
  default     = {}
}

variable "vpc_security_group_ids" {
  description = "List of Security Groups to be allowed to connect to the Aurora cluster"
  type        = list(string)
  default     = []
}

######################
# IAM variables
######################

variable "create_monitoring_role" {
  description = "Whether to create the IAM role for RDS enhanced monitoring"
  type        = bool
  default     = true
}

variable "monitoring_interval" {
  description = "The interval (seconds) between points when Enhanced Monitoring metrics are collected"
  type        = number
  default     = 0
}

variable "monitoring_role_arn" {
  description = "The IAM role for RDS to send enhanced monitoring metrics to CloudWatch"
  type        = string
  default     = ""
}

variable "permissions_boundary" {
  description = "The ARN of the policy that is used to set the permissions boundary for the role."
  type        = string
  default     = null
}
