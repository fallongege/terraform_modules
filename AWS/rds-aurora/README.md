## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

- random

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### apply\_immediately

Description: Whether any cluster modifications are applied immediately, or during the next maintenance window

Type: `bool`

Default: `true`

### auto\_minor\_version\_upgrade

Description: Whether minor engine upgrades will be performed automatically in the maintenance window

Type: `bool`

Default: `true`

### availability\_zones

Description: A list of vailability zone IDs. The maximum number of AZ's that can be specified is 3

Type: `list(string)`

Default: `[]`

### backtrack\_window

Description: The target backtrack window, in seconds. Only available for aurora engine currently. To disable backtracking, set this value to 0. Defaults to 0. Must be between 0 and 259200 (72 hours)

Type: `number`

Default: `0`

### backup\_retention\_period

Description: The number of days to retain backups for

Type: `number`

Default: `7`

### ca\_cert\_identifier

Description: The identifier of the CA certificate for the DB instance

Type: `string`

Default: `"rds-ca-2019"`

### cluster\_family

Description: The family of the Aurora cluster parameter group

Type: `string`

Default: `""`

### cluster\_identifier

Description: The identifier (name) of the cluster

Type: `string`

Default: `""`

### cluster\_parameters

Description: List of DB parameters to apply

Type:

```hcl
list(object({
    apply_method = string
    name         = string
    value        = string
  }))
```

Default: `[]`

### copy\_tags\_to\_snapshot

Description: Whether to copy all Cluster tags to snapshots

Type: `bool`

Default: `true`

### create\_monitoring\_role

Description: Whether to create the IAM role for RDS enhanced monitoring

Type: `bool`

Default: `true`

### database\_name

Description: Name for an automatically created database on cluster creation

Type: `string`

Default: `""`

### deletion\_protection

Description: Whether the Aurora clsuter should have deletion protection enabled

Type: `bool`

Default: `false`

### enable\_http\_endpoint

Description: Whetherto enable the Data API for a serverless Aurora database engine

Type: `bool`

Default: `false`

### enabled\_cloudwatch\_logs\_exports

Description: List of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: audit, profiler

Type: `list(string)`

Default: `[]`

### engine

Description: The Aurora database engine type. Valid values are: aurora, aurora-mysql or aurora-postgresql

Type: `string`

Default: `"aurora"`

### engine\_mode

Description: The database engine mode. Valid values: global, parallelquery, provisioned, serverless, multimaster.

Type: `string`

Default: `"provisioned"`

### engine\_version

Description: The version number of the Aurora database engine to use

Type: `string`

Default: `"5.6.10a"`

### global\_cluster\_identifier

Description: The global cluster identifier specified on aws\_rds\_global\_cluster

Type: `string`

Default: `""`

### iam\_database\_authentication\_enabled

Description: Specifies whether IAM Database authentication should be enabled or not. Not all versions and instances are supported. Refer to the AWS documentation to see which versions are supported.

Type: `bool`

Default: `false`

### iam\_roles

Description: A List of ARNs for the IAM roles to associate to the RDS Cluster.

Type: `list(string)`

Default: `[]`

### instance\_type

Description: Instance type to use at master instance. If instance\_type\_replica is not set it will use the same type for replica instances

Type: `string`

Default: `null`

### instance\_type\_replica

Description: Instance type to use at replica instance

Type: `string`

Default: `null`

### instances\_parameters

Description: Customized instance settings. Supported keys: instance\_name, instance\_type, instance\_promotion\_tier, publicly\_accessible

Type: `list(map(string))`

Default: `[]`

### kms\_key\_id

Description: The ARN for the KMS encryption key. When specifying `kms_key_id`, `storage_encrypted` needs to be set to `true`

Type: `any`

Default: `""`

### local\_exec\_command

Description: The local exec command that is to be executed on Aurora cluster creation, if needed

Type: `any`

Default: `""`

### master\_password

Description: Required unless a snapshot\_identifier is provided. Password for the master DB user

Type: `string`

Default: `""`

### master\_username

Description: Required unless a snapshot\_identifier is provided. Username for the master DB user

Type: `string`

Default: `""`

### monitoring\_interval

Description: The interval (seconds) between points when Enhanced Monitoring metrics are collected

Type: `number`

Default: `0`

### monitoring\_role\_arn

Description: The IAM role for RDS to send enhanced monitoring metrics to CloudWatch

Type: `string`

Default: `""`

### performance\_insights\_enabled

Description: Whether Performance Insights are enabled

Type: `bool`

Default: `false`

### performance\_insights\_kms\_key\_id

Description: The ARN for the KMS key to encrypt Performance Insights data

Type: `string`

Default: `""`

### permissions\_boundary

Description: The ARN of the policy that is used to set the permissions boundary for the role.

Type: `string`

Default: `null`

### port

Description: The port of the Aurora cluster

Type: `string`

Default: `""`

### predefined\_metric\_type

Description: The metric type to scale on. Valid values are RDSReaderAverageCPUUtilization and RDSReaderAverageDatabaseConnections.

Type: `string`

Default: `"RDSReaderAverageCPUUtilization"`

### preferred\_backup\_window

Description: Daily time range during which the backups happen

Type: `string`

Default: `"07:00-09:00"`

### preferred\_maintenance\_window

Description: When to perform DB maintenance

Type: `string`

Default: `"sun:05:00-sun:06:00"`

### publicly\_accessible

Description: Whether the DB should have a public IP address

Type: `bool`

Default: `false`

### rds\_parameter\_group\_name

Description: The family of the RDS cluster parameter group

Type: `string`

Default: `""`

### rds\_subnet\_group\_name

Description: The name of the Aurora subnet group

Type: `string`

Default: `""`

### replica\_count

Description: Number of reader nodes to create.  If `replica_scale_enable` is `true`, the value of `replica_scale_min` is used instead.

Type: `number`

Default: `0`

### replica\_scale\_connections

Description: Average number of connections to trigger autoscaling at. Default value is 70% of db.r4.large's default max\_connections

Type: `number`

Default: `700`

### replica\_scale\_cpu

Description: CPU usage to trigger autoscaling at

Type: `number`

Default: `70`

### replica\_scale\_enabled

Description: Whether to enable autoscaling for RDS Aurora (MySQL) read replicas

Type: `bool`

Default: `false`

### replica\_scale\_in\_cooldown

Description: Cooldown in seconds before allowing further scaling operations after a scale in

Type: `number`

Default: `300`

### replica\_scale\_max

Description: Maximum number of replicas to allow scaling for

Type: `number`

Default: `4`

### replica\_scale\_min

Description: Minimum number of replicas to allow scaling for

Type: `number`

Default: `2`

### replica\_scale\_out\_cooldown

Description: Cooldown in seconds before allowing further scaling operations after a scale out

Type: `number`

Default: `300`

### replication\_source\_identifier

Description: The ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica.

Type: `string`

Default: `""`

### scaling\_configuration

Description: Map of nested attributes with scaling properties. Only valid when engine\_mode is set to `serverless`. Valid values are: auto\_pause, max\_capacity, min\_capacity, seconds\_until\_auto\_pause, timeout\_action

Type: `map(string)`

Default: `{}`

### skip\_final\_snapshot

Description: Determines whether a final DB snapshot is created before the Aurora cluster is deleted

Type: `bool`

Default: `false`

### snapshot\_identifier

Description: Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot

Type: `string`

Default: `""`

### source\_region

Description: The source region for an encrypted replica DB cluster

Type: `string`

Default: `""`

### storage\_encrypted

Description: Specifies whether the Aurora cluster is encrypted

Type: `bool`

Default: `false`

### subnet\_ids

Description: List of VPC subnet IDs to place Aurora instances in

Type: `any`

Default: `[]`

### tags

Description: A map of tags to add to the Aurora resources

Type: `map(string)`

Default: `{}`

### vpc\_security\_group\_ids

Description: List of Security Groups to be allowed to connect to the Aurora cluster

Type: `list(string)`

Default: `[]`

## Outputs

The following outputs are exported:

### rds\_cluster\_arn

Description: The ARN of the RDS cluster

### rds\_cluster\_database\_name

Description: The name of the default database

### rds\_cluster\_endpoint

Description: The endpoint of the RDS cluster

### rds\_cluster\_hosted\_zone\_id

Description: Route53 hosted zone id of the created cluster

### rds\_cluster\_id

Description: The ID of the RDS cluster

### rds\_cluster\_instance\_arns

Description: A list of all cluster instance ids

### rds\_cluster\_instance\_endpoints

Description: A list of all cluster instance endpoints

### rds\_cluster\_instance\_ids

Description: A list of all cluster instance ids

### rds\_cluster\_master\_username

Description: The master username of the RDS cluster

### rds\_cluster\_port

Description: The port of the RDS cluster

### rds\_cluster\_reader\_endpoint

Description: The reader endpoint of the RDS cluster

### rds\_cluster\_resource\_id

Description: The Resource ID of the RDS cluster

