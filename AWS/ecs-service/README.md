## Requirements

No requirements.

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws)

## Modules

No modules.

## Resources

The following resources are used by this module:

- [aws_appautoscaling_policy.autoscaling-policy-ecs-down](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) (resource)
- [aws_appautoscaling_policy.autoscaling-policy-ecs-up](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) (resource)
- [aws_appautoscaling_policy.autoscaling-policy-fargate-down](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) (resource)
- [aws_appautoscaling_policy.autoscaling-policy-fargate-up](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) (resource)
- [aws_appautoscaling_target.autoscaling-target-ecs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_target) (resource)
- [aws_appautoscaling_target.autoscaling-target-fargate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_target) (resource)
- [aws_ecs_service.ecs-service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) (resource)
- [aws_ecs_service.fargate-service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) (resource)
- [aws_ecs_task_definition.ecs-td](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) (resource)

## Required Inputs

The following input variables are required:

### <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region)

Description: The aws region that is being deployed into

Type: `string`

### <a name="input_business_unit"></a> [business\_unit](#input\_business\_unit)

Description: The name of the business unit. Valid options are sk | pmc

Type: `string`

### <a name="input_environment"></a> [environment](#input\_environment)

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_cluster_arn"></a> [cluster\_arn](#input\_cluster\_arn)

Description: The arn of the ecs/fargate cluster

Type: `any`

Default: `""`

### <a name="input_container_definition"></a> [container\_definition](#input\_container\_definition)

Description: The rendered task definition

Type: `any`

Default: `""`

### <a name="input_cpu"></a> [cpu](#input\_cpu)

Description: The number of cpu units that the container needs

Type: `number`

Default: `0`

### <a name="input_create_ecs_autoscaling"></a> [create\_ecs\_autoscaling](#input\_create\_ecs\_autoscaling)

Description: Whether to create an Autoscaling target and policy

Type: `bool`

Default: `false`

### <a name="input_create_ecs_service"></a> [create\_ecs\_service](#input\_create\_ecs\_service)

Description: Whether to create an ECS service

Type: `bool`

Default: `false`

### <a name="input_create_fargate_service"></a> [create\_fargate\_service](#input\_create\_fargate\_service)

Description: Whether to create a Fargate service

Type: `bool`

Default: `false`

### <a name="input_desired_count"></a> [desired\_count](#input\_desired\_count)

Description: The desired number of containers to be running

Type: `number`

Default: `1`

### <a name="input_ecs_cluster_name"></a> [ecs\_cluster\_name](#input\_ecs\_cluster\_name)

Description: The name of the ECS/Fargate cluster

Type: `any`

Default: `""`

### <a name="input_enable_execute_command"></a> [enable\_execute\_command](#input\_enable\_execute\_command)

Description: Specifies whether to enable Amazon ECS Exec for the tasks within the service

Type: `bool`

Default: `true`

### <a name="input_execution_role_arn"></a> [execution\_role\_arn](#input\_execution\_role\_arn)

Description:   The Amazon Resource Name (ARN) of the task execution role that grants the Amazon ECS container agent permission to  
  make AWS API calls on your behalf

Type: `any`

Default: `""`

### <a name="input_health_check_grace_period_seconds"></a> [health\_check\_grace\_period\_seconds](#input\_health\_check\_grace\_period\_seconds)

Description: Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown

Type: `number`

Default: `0`

### <a name="input_launch_type"></a> [launch\_type](#input\_launch\_type)

Description: The launch type on which to run your service. Valid options are EC2 | FARGATE

Type: `string`

Default: `""`

### <a name="input_load_balancer"></a> [load\_balancer](#input\_load\_balancer)

Description: The load balancer configuration for the ecs service. Settings are: target\_group\_arn, container\_name, container\_port

Type:

```hcl
list(object({
    target_group_arn = string
    container_name   = string
    container_port   = number
  }))
```

Default: `[]`

### <a name="input_max_capacity"></a> [max\_capacity](#input\_max\_capacity)

Description: The max capacity of the scalable target

Type: `number`

Default: `null`

### <a name="input_memory"></a> [memory](#input\_memory)

Description: The amount (in MiB) of memory to present to the container

Type: `number`

Default: `0`

### <a name="input_min_capacity"></a> [min\_capacity](#input\_min\_capacity)

Description: The min capacity of the scalable target

Type: `number`

Default: `null`

### <a name="input_network_configuration"></a> [network\_configuration](#input\_network\_configuration)

Description: The network configuration for the ecs service. Settings are: subnets, security\_groups

Type:

```hcl
list(object({
    assign_public_ip = any
    subnets          = any
    security_groups  = any
  }))
```

Default: `[]`

### <a name="input_network_mode"></a> [network\_mode](#input\_network\_mode)

Description: The Docker networking mode to use for the containers in the task. Valid options are none | bridge | awsvpc | host

Type: `string`

Default: `"awsvpc"`

### <a name="input_platform_version"></a> [platform\_version](#input\_platform\_version)

Description: The platform version on which to run your service. Only applicable for launch\_type set to FARGATE

Type: `string`

Default: `"1.4.0"`

### <a name="input_scaling_policy_down"></a> [scaling\_policy\_down](#input\_scaling\_policy\_down)

Description:   List of scaling policy settings including:  
  adjustment\_type  
  cooldown  
  metric\_aggregation\_type  
  metric\_interval\_upper\_bound  
  scaling\_adjustment  
  scaling\_identifier

Type:

```hcl
list(object({
    adjustment_type             = any
    cooldown                    = any
    metric_aggregation_type     = any
    metric_interval_upper_bound = any
    scaling_adjustment          = any
    scaling_identifier          = any
  }))
```

Default:

```json
[
  {
    "adjustment_type": "",
    "cooldown": "",
    "metric_aggregation_type": "",
    "metric_interval_upper_bound": "",
    "scaling_adjustment": "",
    "scaling_identifier": ""
  }
]
```

### <a name="input_scaling_policy_up"></a> [scaling\_policy\_up](#input\_scaling\_policy\_up)

Description:   List of scaling policy settings including:  
  adjustment\_type  
  cooldown  
  metric\_aggregation\_type  
  metric\_interval\_lower\_bound  
  scaling\_adjustment  
  scaling\_identifier

Type:

```hcl
list(object({
    adjustment_type             = any
    cooldown                    = any
    metric_aggregation_type     = any
    metric_interval_lower_bound = any
    scaling_adjustment          = any
    scaling_identifier          = any
  }))
```

Default:

```json
[
  {
    "adjustment_type": "",
    "cooldown": "",
    "metric_aggregation_type": "",
    "metric_interval_lower_bound": "",
    "scaling_adjustment": "",
    "scaling_identifier": ""
  }
]
```

### <a name="input_service_name"></a> [service\_name](#input\_service\_name)

Description: The name of the ecs service

Type: `string`

Default: `""`

### <a name="input_tags"></a> [tags](#input\_tags)

Description: A map of tags to add to all resources

Type: `any`

Default: `{}`

### <a name="input_task_role_arn"></a> [task\_role\_arn](#input\_task\_role\_arn)

Description: The IAM role that allows the containers in the task permission to call the AWS APIs

Type: `string`

Default: `""`

### <a name="input_volume"></a> [volume](#input\_volume)

Description: The volume configuration for the ecs task definition. Settings are: name, file\_system\_id, transit\_encryption, access\_point\_id, iam

Type: `list(any)`

Default: `[]`

## Outputs

The following outputs are exported:

### <a name="output_ecs_autoscaling_policy_down_arn"></a> [ecs\_autoscaling\_policy\_down\_arn](#output\_ecs\_autoscaling\_policy\_down\_arn)

Description: n/a

### <a name="output_ecs_autoscaling_policy_up_arn"></a> [ecs\_autoscaling\_policy\_up\_arn](#output\_ecs\_autoscaling\_policy\_up\_arn)

Description: n/a

### <a name="output_ecs_service_id"></a> [ecs\_service\_id](#output\_ecs\_service\_id)

Description: The ID of the ECS service

### <a name="output_ecs_service_name"></a> [ecs\_service\_name](#output\_ecs\_service\_name)

Description: The Name of the ECS service

### <a name="output_fargate_autoscaling_policy_down_arn"></a> [fargate\_autoscaling\_policy\_down\_arn](#output\_fargate\_autoscaling\_policy\_down\_arn)

Description: n/a

### <a name="output_fargate_autoscaling_policy_up_arn"></a> [fargate\_autoscaling\_policy\_up\_arn](#output\_fargate\_autoscaling\_policy\_up\_arn)

Description: n/a

### <a name="output_fargate_service_id"></a> [fargate\_service\_id](#output\_fargate\_service\_id)

Description: The ID of the Fargate service

### <a name="output_fargate_service_name"></a> [fargate\_service\_name](#output\_fargate\_service\_name)

Description: The Name of the Fargate service
