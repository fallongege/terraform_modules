resource "aws_ecs_service" "ecs-service" {
  count = var.create_ecs_service ? 1 : 0

  name = local.service_name

  cluster                           = var.cluster_arn
  desired_count                     = var.desired_count
  enable_execute_command            = var.enable_execute_command
  health_check_grace_period_seconds = var.health_check_grace_period_seconds
  launch_type                       = var.launch_type
  task_definition                   = aws_ecs_task_definition.ecs-td.arn

  dynamic "load_balancer" {
    for_each = var.load_balancer

    content {
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
      target_group_arn = lookup(load_balancer.value, "target_group_arn", null)
    }
  }

  dynamic "network_configuration" {
    for_each = var.network_configuration

    content {
      assign_public_ip = lookup(network_configuration.value, "assign_public_ip", null)
      security_groups  = lookup(network_configuration.value, "security_groups", null)
      subnets          = lookup(network_configuration.value, "subnets", null)
    }
  }

  tags = merge(
    {
      "Name" = local.service_name
    },
    var.tags
  )
}

resource "aws_ecs_service" "fargate-service" {
  count = var.create_fargate_service ? 1 : 0

  name = local.service_name

  cluster                           = var.cluster_arn
  desired_count                     = var.desired_count
  health_check_grace_period_seconds = var.health_check_grace_period_seconds
  launch_type                       = var.launch_type
  platform_version                  = var.platform_version
  task_definition                   = aws_ecs_task_definition.ecs-td.arn

  dynamic "load_balancer" {
    for_each = var.load_balancer

    content {
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
      target_group_arn = lookup(load_balancer.value, "target_group_arn", null)
    }
  }

  dynamic "network_configuration" {
    for_each = var.network_configuration

    content {
      assign_public_ip = lookup(network_configuration.value, "assign_public_ip", null)
      security_groups  = lookup(network_configuration.value, "security_groups", null)
      subnets          = lookup(network_configuration.value, "subnets", null)
    }
  }

  tags = merge(
    {
      "Name" = local.service_name
    },
    var.tags
  )
}

resource "aws_ecs_task_definition" "ecs-td" {
  family = local.family_name

  container_definitions    = var.container_definition
  cpu                      = var.cpu
  execution_role_arn       = var.execution_role_arn
  memory                   = var.memory
  network_mode             = var.network_mode
  requires_compatibilities = [var.launch_type]
  task_role_arn            = var.task_role_arn

  dynamic "volume" {
    for_each = var.volume

    content {
      name = volume.value.volume_name

      efs_volume_configuration {
        file_system_id     = lookup(volume.value, "file_system_id", null)
        transit_encryption = lookup(volume.value, "transit_encryption", null)

        authorization_config {
          access_point_id = lookup(volume.value, "access_point_id", null)
          iam             = lookup(volume.value, "iam", null)
        }
      }
    }
  }

  tags = merge(
    {
      "Name" = local.service_name
    },
    var.tags
  )
}
