locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  family_name  = "${local.name_prefix}-${var.service_name}"
  service_name = "${local.name_prefix}-${var.service_name}"
}
