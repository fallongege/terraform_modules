#####################
# ECS Service outputs
#####################

output "ecs_service_id" {
  description = "The ID of the ECS service"
  value       = concat(aws_ecs_service.ecs-service.*.id, [""])
}

output "ecs_service_name" {
  description = "The Name of the ECS service"
  value       = concat(aws_ecs_service.ecs-service.*.name, [""])
}

output "fargate_service_id" {
  description = "The ID of the Fargate service"
  value       = concat(aws_ecs_service.fargate-service.*.id, [""])
}

output "fargate_service_name" {
  description = "The Name of the Fargate service"
  value       = concat(aws_ecs_service.fargate-service.*.name, [""])
}

######################
# Auto Scaling outputs
######################

output "ecs_autoscaling_policy_up_arn" {
  value = join("", aws_appautoscaling_policy.autoscaling-policy-ecs-up.*.arn)
}

output "fargate_autoscaling_policy_up_arn" {
  value = join("", aws_appautoscaling_policy.autoscaling-policy-fargate-up.*.arn)
}

output "ecs_autoscaling_policy_down_arn" {
  value = join("", aws_appautoscaling_policy.autoscaling-policy-ecs-down.*.arn)
}

output "fargate_autoscaling_policy_down_arn" {
  value = join("", aws_appautoscaling_policy.autoscaling-policy-fargate-down.*.arn)
}
