module "ecs-service" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/ecs-service/?ref=main"

  # Global arguments
  aws_region    = var.aws_region
  business_unit = var.business_unit
  environment   = var.environment

  # Autoscaling arguments
  create_ecs_autoscaling = true
  ecs_cluster_name       = module.fargate.ecs_cluster_name
  scaling_policy_down = [
    {
      adjustment_type             = "ChangeInCapacity"
      cooldown                    = 60
      metric_aggregation_type     = "Maximum"
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
      scaling_identifier          = "scale-down"
    }
  ]
  scaling_policy_up = [
    {
      adjustment_type             = "ChangeInCapacity"
      cooldown                    = 60
      metric_aggregation_type     = "Maximum"
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
      scaling_identifier          = "scale-up"
    }
  ]

  # ECS Service arguments
  create_fargate_service            = true
  cluster_arn                       = module.fargate.ecs_cluster_arn
  container_definition              = module.task-definition.json
  cpu                               = 512
  desired_count                     = 2
  execution_role_arn                = module.fargate-iam-role.iam_role_arn
  health_check_grace_period_seconds = 300
  launch_type                       = "FARGATE"
  load_balancer = [
    {
      target_group_arn = join("", module.private-target-group-8080.target_group_arns)
      container_name   = "container"
      container_port   = 8080
    },
  ]
  max_capacity = 4
  memory       = 512
  min_capacity = 2
  network_configuration = [{
    assign_public_ip = false
    security_groups  = ["sg-13445345"]
    subnets          = "subnet-1343546"
  }]
  service_name = "container"
  tags = {
    Application = "container"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  task_role_arn = module.fargate-iam-role.iam_role_arn
}
