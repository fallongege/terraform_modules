resource "aws_appautoscaling_target" "autoscaling-target-ecs" {
  count = var.create_ecs_autoscaling && var.create_ecs_service ? 1 : 0

  max_capacity       = var.max_capacity
  min_capacity       = var.min_capacity
  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.ecs-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  depends_on = [aws_ecs_service.ecs-service]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-ecs-up" {
  count = var.create_ecs_autoscaling && var.create_ecs_service ? length(var.scaling_policy_up) : 0

  name = "${local.family_name}-${lookup(var.scaling_policy_up[count.index], "scaling_identifier")}"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.ecs-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = lookup(var.scaling_policy_up[count.index], "adjustment_type")
    cooldown                = lookup(var.scaling_policy_up[count.index], "cooldown")
    metric_aggregation_type = lookup(var.scaling_policy_up[count.index], "metric_aggregation_type")

    step_adjustment {
      metric_interval_lower_bound = lookup(var.scaling_policy_up[count.index], "metric_interval_lower_bound")
      scaling_adjustment          = lookup(var.scaling_policy_up[count.index], "scaling_adjustment")
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-ecs]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-ecs-down" {
  count = var.create_ecs_autoscaling && var.create_ecs_service ? length(var.scaling_policy_down) : 0

  name = "${local.family_name}-${lookup(var.scaling_policy_down[count.index], "scaling_identifier")}"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.ecs-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = lookup(var.scaling_policy_down[count.index], "adjustment_type")
    cooldown                = lookup(var.scaling_policy_down[count.index], "cooldown")
    metric_aggregation_type = lookup(var.scaling_policy_down[count.index], "metric_aggregation_type")

    step_adjustment {
      metric_interval_upper_bound = lookup(var.scaling_policy_down[count.index], "metric_interval_upper_bound")
      scaling_adjustment          = lookup(var.scaling_policy_down[count.index], "scaling_adjustment")
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-ecs]
}

resource "aws_appautoscaling_target" "autoscaling-target-fargate" {
  count = var.create_ecs_autoscaling && var.create_fargate_service ? 1 : 0

  max_capacity       = var.max_capacity
  min_capacity       = var.min_capacity
  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  depends_on = [aws_ecs_service.ecs-service]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-fargate-up" {
  count = var.create_ecs_autoscaling && var.create_fargate_service ? length(var.scaling_policy_up) : 0

  name = "${local.family_name}-${lookup(var.scaling_policy_up[count.index], "scaling_identifier")}"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = lookup(var.scaling_policy_up[count.index], "adjustment_type")
    cooldown                = lookup(var.scaling_policy_up[count.index], "cooldown")
    metric_aggregation_type = lookup(var.scaling_policy_up[count.index], "metric_aggregation_type")

    step_adjustment {
      metric_interval_lower_bound = lookup(var.scaling_policy_up[count.index], "metric_interval_lower_bound")
      scaling_adjustment          = lookup(var.scaling_policy_up[count.index], "scaling_adjustment")
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-fargate]
}

resource "aws_appautoscaling_policy" "autoscaling-policy-fargate-down" {
  count = var.create_ecs_autoscaling && var.create_fargate_service ? length(var.scaling_policy_down) : 0

  name = "${local.family_name}-${lookup(var.scaling_policy_down[count.index], "scaling_identifier")}"

  resource_id        = "service/${var.ecs_cluster_name}/${aws_ecs_service.fargate-service[0].name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = lookup(var.scaling_policy_down[count.index], "adjustment_type")
    cooldown                = lookup(var.scaling_policy_down[count.index], "cooldown")
    metric_aggregation_type = lookup(var.scaling_policy_down[count.index], "metric_aggregation_type")

    step_adjustment {
      metric_interval_upper_bound = lookup(var.scaling_policy_down[count.index], "metric_interval_upper_bound")
      scaling_adjustment          = lookup(var.scaling_policy_down[count.index], "scaling_adjustment")
    }
  }

  depends_on = [aws_appautoscaling_target.autoscaling-target-ecs]
}
