##################
# Global variables
##################
variable "aws_region" {
  description = "The aws region that is being deployed into"
  type        = string
}

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

#######################
# Autoscaling variables
#######################

variable "create_ecs_autoscaling" {
  description = "Whether to create an Autoscaling target and policy"
  type        = bool
  default     = false
}

variable "ecs_cluster_name" {
  description = "The name of the ECS/Fargate cluster"
  type        = any
  default     = ""
}

variable "scaling_policy_down" {
  description = <<EOF
  List of scaling policy settings including:
  adjustment_type
  cooldown
  metric_aggregation_type
  metric_interval_upper_bound
  scaling_adjustment
  scaling_identifier
  EOF
  type = list(object({
    adjustment_type             = any
    cooldown                    = any
    metric_aggregation_type     = any
    metric_interval_upper_bound = any
    scaling_adjustment          = any
    scaling_identifier          = any
  }))
  default = [
    {
      adjustment_type             = ""
      cooldown                    = ""
      metric_aggregation_type     = ""
      metric_interval_upper_bound = ""
      scaling_adjustment          = ""
      scaling_identifier          = ""
    }
  ]
}

variable "scaling_policy_up" {
  description = <<EOF
  List of scaling policy settings including:
  adjustment_type
  cooldown
  metric_aggregation_type
  metric_interval_lower_bound
  scaling_adjustment
  scaling_identifier
  EOF
  type = list(object({
    adjustment_type             = any
    cooldown                    = any
    metric_aggregation_type     = any
    metric_interval_lower_bound = any
    scaling_adjustment          = any
    scaling_identifier          = any
  }))
  default = [
    {
      adjustment_type             = ""
      cooldown                    = ""
      metric_aggregation_type     = ""
      metric_interval_lower_bound = ""
      scaling_adjustment          = ""
      scaling_identifier          = ""
    }
  ]
}

#######################
# ECS Service variables
#######################

variable "create_ecs_service" {
  description = "Whether to create an ECS service"
  type        = bool
  default     = false
}

variable "create_fargate_service" {
  description = "Whether to create a Fargate service"
  type        = bool
  default     = false
}

variable "cluster_arn" {
  description = "The arn of the ecs/fargate cluster"
  type        = any
  default     = ""
}

variable "container_definition" {
  description = "The rendered task definition"
  type        = any
  default     = ""
}

variable "cpu" {
  description = "The number of cpu units that the container needs"
  type        = number
  default     = 0
}

variable "desired_count" {
  description = "The desired number of containers to be running"
  type        = number
  default     = 1
}

variable "enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
  type        = bool
  default     = true
}

variable "execution_role_arn" {
  description = <<EOF
  The Amazon Resource Name (ARN) of the task execution role that grants the Amazon ECS container agent permission to
  make AWS API calls on your behalf
  EOF
  type        = any
  default     = ""
}

variable "health_check_grace_period_seconds" {
  description = "Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown"
  type        = number
  default     = 0
}

variable "launch_type" {
  description = "The launch type on which to run your service. Valid options are EC2 | FARGATE"
  type        = string
  default     = ""
}

variable "load_balancer" {
  description = "The load balancer configuration for the ecs service. Settings are: target_group_arn, container_name, container_port"
  type = list(object({
    target_group_arn = string
    container_name   = string
    container_port   = number
  }))
  default = []
}

variable "max_capacity" {
  description = "The max capacity of the scalable target"
  type        = number
  default     = null
}

variable "memory" {
  description = "The amount (in MiB) of memory to present to the container"
  type        = number
  default     = 0
}

variable "min_capacity" {
  description = "The min capacity of the scalable target"
  type        = number
  default     = null
}

variable "network_configuration" {
  description = "The network configuration for the ecs service. Settings are: subnets, security_groups"
  type = list(object({
    assign_public_ip = any
    subnets          = any
    security_groups  = any
  }))
  default = []
}

variable "network_mode" {
  description = "The Docker networking mode to use for the containers in the task. Valid options are none | bridge | awsvpc | host"
  type        = string
  default     = "awsvpc"
}

variable "platform_version" {
  description = "The platform version on which to run your service. Only applicable for launch_type set to FARGATE"
  type        = string
  default     = "1.4.0"
}

variable "service_name" {
  description = "The name of the ecs service"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = any
  default     = {}
}

variable "task_role_arn" {
  description = "The IAM role that allows the containers in the task permission to call the AWS APIs"
  type        = string
  default     = ""
}

variable "volume" {
  description = "The volume configuration for the ecs task definition. Settings are: name, file_system_id, transit_encryption, access_point_id, iam"
  type        = list(any)
  default     = []
}
