variable "key_name" {
  description = "The name of the key"
  type        = string
}

variable "public_key" {
  description = "The public ssh key"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = any
  default     = {}
}
