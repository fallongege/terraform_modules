output "key_pair_arn" {
  description = "The arn of the ssh key pair"
  value       = aws_key_pair.key-pair.arn
}

output "key_pair_id" {
  description = "The id of the ssh key pair"
  value       = aws_key_pair.key-pair.key_pair_id
}

output "key_pair_name" {
  description = "The name of the ssh key pair"
  value       = aws_key_pair.key-pair.key_name
}

