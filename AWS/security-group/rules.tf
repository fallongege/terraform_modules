################
# Ingress
################
resource "aws_security_group_rule" "ingress_with_source_security_group_id" {
  count = var.create_ingress_with_source_security_group_id ? length(var.ingress_with_source_security_group_id) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "ingress"

  source_security_group_id = var.ingress_with_source_security_group_id[count.index]["source_security_group_id"]
  description = lookup(
    var.ingress_with_source_security_group_id[count.index],
    "description",
    "Ingress Rule"
  )

  from_port = lookup(
    var.ingress_with_source_security_group_id[count.index],
    "from_port"
  )
  to_port = lookup(
    var.ingress_with_source_security_group_id[count.index],
    "to_port"
  )
  protocol = lookup(
    var.ingress_with_source_security_group_id[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "ingress_with_cidr_blocks" {
  count = var.create_ingress_with_cidr_blocks ? length(var.ingress_with_cidr_blocks) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "ingress"

  cidr_blocks = split(
    ",",
    lookup(
      var.ingress_with_cidr_blocks[count.index],
      "cidr_blocks",
      join(",", var.ingress_cidr_blocks),
    ),
  )
  description = lookup(
    var.ingress_with_cidr_blocks[count.index],
    "description",
    "Ingress Rule",
  )

  from_port = lookup(
    var.ingress_with_cidr_blocks[count.index],
    "from_port"
  )
  to_port = lookup(
    var.ingress_with_cidr_blocks[count.index],
    "to_port"
  )
  protocol = lookup(
    var.ingress_with_cidr_blocks[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "ingress_with_ipv6_cidr_blocks" {
  count = var.create_ingress_with_ipv6_cidr_blocks ? length(var.ingress_with_ipv6_cidr_blocks) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "ingress"

  ipv6_cidr_blocks = split(
    ",",
    lookup(
      var.ingress_with_ipv6_cidr_blocks[count.index],
      "ipv6_cidr_blocks",
      join(",", var.ingress_ipv6_cidr_blocks),
    ),
  )
  description = lookup(
    var.ingress_with_ipv6_cidr_blocks[count.index],
    "description",
    "Ingress Rule",
  )

  from_port = lookup(
    var.ingress_with_ipv6_cidr_blocks[count.index],
    "from_port"
  )
  to_port = lookup(
    var.ingress_with_ipv6_cidr_blocks[count.index],
    "to_port"
  )
  protocol = lookup(
    var.ingress_with_ipv6_cidr_blocks[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "ingress_with_self" {
  count = var.create_ingress_with_self ? length(var.ingress_with_self) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "ingress"

  self = lookup(var.ingress_with_self[count.index], "self", true)
  description = lookup(
    var.ingress_with_self[count.index],
    "description",
    "Ingress Rule",
  )

  from_port = lookup(
    var.ingress_with_self[count.index],
    "from_port"
  )
  to_port = lookup(
    var.ingress_with_self[count.index],
    "to_port"
  )
  protocol = lookup(
    var.ingress_with_self[count.index],
    "protocol"
  )
}

################
# Egress
################
resource "aws_security_group_rule" "egress_with_source_security_group_id" {
  count = var.create_egress_with_source_security_group_id ? length(var.egress_with_source_security_group_id) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "egress"

  source_security_group_id = var.egress_with_source_security_group_id[count.index]["source_security_group_id"]
  description = lookup(
    var.egress_with_source_security_group_id[count.index],
    "description",
    "Egress Rule",
  )

  from_port = lookup(
    var.egress_with_source_security_group_id[count.index],
    "from_port"
  )
  to_port = lookup(
    var.egress_with_source_security_group_id[count.index],
    "to_port"
  )
  protocol = lookup(
    var.egress_with_source_security_group_id[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "egress_with_cidr_blocks" {
  count = var.create_egress_with_cidr_blocks ? length(var.egress_with_cidr_blocks) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "egress"

  cidr_blocks = split(
    ",",
    lookup(
      var.egress_with_cidr_blocks[count.index],
      "cidr_blocks",
      join(",", var.egress_cidr_blocks),
    ),
  )
  description = lookup(
    var.egress_with_cidr_blocks[count.index],
    "description",
    "Egress Rule",
  )

  from_port = lookup(
    var.egress_with_cidr_blocks[count.index],
    "from_port"
  )
  to_port = lookup(
    var.egress_with_cidr_blocks[count.index],
    "to_port"
  )
  protocol = lookup(
    var.egress_with_cidr_blocks[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "egress_with_ipv6_cidr_blocks" {
  count = var.create_egress_with_ipv6_cidr_blocks ? length(var.egress_with_ipv6_cidr_blocks) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "egress"

  ipv6_cidr_blocks = split(
    ",",
    lookup(
      var.egress_with_ipv6_cidr_blocks[count.index],
      "ipv6_cidr_blocks",
      join(",", var.egress_ipv6_cidr_blocks),
    ),
  )
  description = lookup(
    var.egress_with_ipv6_cidr_blocks[count.index],
    "description",
    "Egress Rule",
  )

  from_port = lookup(
    var.egress_with_ipv6_cidr_blocks[count.index],
    "from_port"
  )
  to_port = lookup(
    var.egress_with_ipv6_cidr_blocks[count.index],
    "to_port"
  )
  protocol = lookup(
    var.egress_with_ipv6_cidr_blocks[count.index],
    "protocol"
  )
}

resource "aws_security_group_rule" "egress_with_self" {
  count = var.create_egress_with_self ? length(var.egress_with_self) : 0

  security_group_id = aws_security_group.sg[0].id
  type              = "egress"

  self = lookup(var.egress_with_self[count.index], "self", true)
  description = lookup(
    var.egress_with_self[count.index],
    "description",
    "Egress Rule",
  )

  from_port = lookup(
    var.egress_with_self[count.index],
    "from_port"
  )
  to_port = lookup(
    var.egress_with_self[count.index],
    "to_port"
  )
  protocol = lookup(
    var.egress_with_self[count.index],
    "protocol"
  )
}
