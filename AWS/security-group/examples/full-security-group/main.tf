module "sg" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/security-group/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  name = "sg"

  create_sg   = true
  description = "Security group"
  vpc_id      = "vpc-1344934513"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }

  create_ingress_with_cidr_blocks = true
  ingress_with_cidr_blocks = [
    {
      rule        = "http from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "http from public"
    },
    {
      rule        = "https from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "https from public"
    },
    {
      rule        = "3000 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 3000
      to_port     = 3000
      protocol    = "tcp"
      description = "3000 from public"
    },
    {
      rule        = "8000 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 8000
      to_port     = 8000
      protocol    = "tcp"
      description = "8000 from public"
    },
    {
      rule        = "8080 from public"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "8080 from public"
    },
    {
      rule        = "all from VPN"
      cidr_blocks = "10.0.0.0/8"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "all from VPN"
    },
  ]

  create_ingress_with_ipv6_cidr_blocks = true
  ingress_with_ipv6_cidr_blocks = [
    {
      rule             = "http from public"
      ipv6_cidr_blocks = "::/0"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      description      = "http from public"
    },
    {
      rule             = "https from public"
      ipv6_cidr_blocks = "::/0"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      description      = "https from public"
    },
  ]

  create_egress_with_cidr_blocks = true
  egress_with_cidr_blocks = [
    {
      rule        = "All IPv4 traffic"
      cidr_blocks = "0.0.0.0/0"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "All IPv4 traffic"
    }
  ]

  create_egress_with_ipv6_cidr_blocks = true
  egress_with_ipv6_cidr_blocks = [
    {
      rule        = "All IPv6 traffic"
      cidr_blocks = "::/0"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "All IPv6 traffic"
    }
  ]
}
