################
# Global variables
################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Locals
################

variable "name" {
  description = "The name of the security group"
  type        = string
}

################
# Security Group variables
################

variable "create_sg" {
  description = "Whether to create the security group"
  type        = bool
  default     = true
}

variable "description" {
  description = "A description of the purpose of the security group"
  type        = string
}

variable "revoke_rules_on_delete" {
  description = "Whether to revoke all of the Security Groups attached ingress and egress rules before deleting the rule itself"
  type        = bool
  default     = false
}

variable "vpc_id" {
  description = "The ID of the VPC where to create the security group"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to the load balancer"
  type        = map(string)
}

################
# Security Group Rules - Ingress With Source Security Group ID Variables
################

variable "create_ingress_with_source_security_group_id" {
  description = "Whether to create an ingress security group rule with a source security group id"
  type        = bool
  default     = false
}

variable "ingress_with_source_security_group_id" {
  description = "List of ingress rules to create where 'source_security_group_id' is used"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Ingress With CIDR Block Variables
################

variable "create_ingress_with_cidr_blocks" {
  description = "Whether to create an ingress security group rule with a CIDR Block"
  type        = bool
  default     = false
}

variable "ingress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all ingress rules"
  type        = list(string)
  default     = []
}

variable "ingress_with_cidr_blocks" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Ingress With IPv6 CIDR Block Variables
################

variable "create_ingress_with_ipv6_cidr_blocks" {
  description = "Whether to create an ingress security group rule with an IPv6 CIDR Block"
  type        = bool
  default     = false
}

variable "ingress_ipv6_cidr_blocks" {
  description = "List of IPv6 CIDR ranges to use on all ingress rules"
  type        = list(string)
  default     = []
}

variable "ingress_with_ipv6_cidr_blocks" {
  description = "List of ingress rules to create where 'ipv6_cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Ingress With Self Variables
################

variable "create_ingress_with_self" {
  description = "Whether to create an ingress security group rule with 'self'"
  type        = bool
  default     = false
}

variable "ingress_with_self" {
  description = "List of ingress rules to create where 'self' is defined"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Egress With Source Security Group ID Variables
################

variable "create_egress_with_source_security_group_id" {
  description = "Whether to create an egress security group rule with a source security group id"
  type        = bool
  default     = false
}

variable "egress_with_source_security_group_id" {
  description = "List of egress rules to create where 'source_security_group_id' is used"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Egress With CIDR Block Variables
################

variable "create_egress_with_cidr_blocks" {
  description = "Whether to create an egress security group rule with a CIDR Block"
  type        = bool
  default     = false
}

variable "egress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all egress rules"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "egress_with_cidr_blocks" {
  description = "List of computed egress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}

################
# Security Group Rules - Egress With IPv6 CIDR Block Variables
################

variable "create_egress_with_ipv6_cidr_blocks" {
  description = "Whether to create an egress security group rule with an IPv6 CIDR Block"
  type        = bool
  default     = false
}

variable "egress_with_ipv6_cidr_blocks" {
  description = "List of egress rules to create where 'ipv6_cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}

variable "egress_ipv6_cidr_blocks" {
  description = "List of IPv6 CIDR ranges to use on all egress rules"
  type        = list(string)
  default     = ["::/0"]
}

################
# Security Group Rules - Egress With Self Variables
################

variable "create_egress_with_self" {
  description = "Whether to create an egress security group rule with 'self'"
  type        = bool
  default     = false
}

variable "egress_with_self" {
  description = "List of egress rules to create where 'self' is defined"
  type        = list(map(string))
  default     = []
}
