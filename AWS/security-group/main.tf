resource "aws_security_group" "sg" {
  count = var.create_sg ? 1 : 0

  name                   = "${local.name}-sg"
  description            = var.description
  revoke_rules_on_delete = var.revoke_rules_on_delete
  vpc_id                 = var.vpc_id

  tags = merge(
    {
      "Name" = "${local.name}-sg"
    },
    var.tags,
  )
}
