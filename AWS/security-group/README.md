## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### description

Description: A description of the purpose of the security group

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### name

Description: The name of the security group

Type: `string`

### tags

Description: A map of tags to add to the load balancer

Type: `map(string)`

### vpc\_id

Description: The ID of the VPC where to create the security group

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### create\_egress\_with\_cidr\_blocks

Description: Whether to create an egress security group rule with a CIDR Block

Type: `bool`

Default: `false`

### create\_egress\_with\_ipv6\_cidr\_blocks

Description: Whether to create an egress security group rule with an IPv6 CIDR Block

Type: `bool`

Default: `false`

### create\_egress\_with\_self

Description: Whether to create an egress security group rule with 'self'

Type: `bool`

Default: `false`

### create\_egress\_with\_source\_security\_group\_id

Description: Whether to create an egress security group rule with a source security group id

Type: `bool`

Default: `false`

### create\_ingress\_with\_cidr\_blocks

Description: Whether to create an ingress security group rule with a CIDR Block

Type: `bool`

Default: `false`

### create\_ingress\_with\_ipv6\_cidr\_blocks

Description: Whether to create an ingress security group rule with an IPv6 CIDR Block

Type: `bool`

Default: `false`

### create\_ingress\_with\_self

Description: Whether to create an ingress security group rule with 'self'

Type: `bool`

Default: `false`

### create\_ingress\_with\_source\_security\_group\_id

Description: Whether to create an ingress security group rule with a source security group id

Type: `bool`

Default: `false`

### create\_sg

Description: Whether to create the security group

Type: `bool`

Default: `true`

### egress\_cidr\_blocks

Description: List of IPv4 CIDR ranges to use on all egress rules

Type: `list(string)`

Default:

```json
[
  "0.0.0.0/0"
]
```

### egress\_ipv6\_cidr\_blocks

Description: List of IPv6 CIDR ranges to use on all egress rules

Type: `list(string)`

Default:

```json
[
  "::/0"
]
```

### egress\_with\_cidr\_blocks

Description: List of computed egress rules to create where 'cidr\_blocks' is used

Type: `list(map(string))`

Default: `[]`

### egress\_with\_ipv6\_cidr\_blocks

Description: List of egress rules to create where 'ipv6\_cidr\_blocks' is used

Type: `list(map(string))`

Default: `[]`

### egress\_with\_self

Description: List of egress rules to create where 'self' is defined

Type: `list(map(string))`

Default: `[]`

### egress\_with\_source\_security\_group\_id

Description: List of egress rules to create where 'source\_security\_group\_id' is used

Type: `list(map(string))`

Default: `[]`

### ingress\_cidr\_blocks

Description: List of IPv4 CIDR ranges to use on all ingress rules

Type: `list(string)`

Default: `[]`

### ingress\_ipv6\_cidr\_blocks

Description: List of IPv6 CIDR ranges to use on all ingress rules

Type: `list(string)`

Default: `[]`

### ingress\_with\_cidr\_blocks

Description: List of ingress rules to create where 'cidr\_blocks' is used

Type: `list(map(string))`

Default: `[]`

### ingress\_with\_ipv6\_cidr\_blocks

Description: List of ingress rules to create where 'ipv6\_cidr\_blocks' is used

Type: `list(map(string))`

Default: `[]`

### ingress\_with\_self

Description: List of ingress rules to create where 'self' is defined

Type: `list(map(string))`

Default: `[]`

### ingress\_with\_source\_security\_group\_id

Description: List of ingress rules to create where 'source\_security\_group\_id' is used

Type: `list(map(string))`

Default: `[]`

### revoke\_rules\_on\_delete

Description: Whether to revoke all of the Security Groups attached ingress and egress rules before deleting the rule itself

Type: `bool`

Default: `false`

## Outputs

The following outputs are exported:

### security\_group\_description

Description: The description of the security group

### security\_group\_id

Description: The ID of the security group

### security\_group\_name

Description: The name of the security group

### security\_group\_owner\_id

Description: The owner ID

### security\_group\_vpc\_id

Description: The VPC ID of the security group

