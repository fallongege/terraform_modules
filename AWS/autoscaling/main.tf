resource "aws_launch_configuration" "launch-configuration" {
  count = var.create_lc ? 1 : 0

  name_prefix = "${local.name_prefix}-${var.name}-"

  associate_public_ip_address = var.associate_public_ip_address
  ebs_optimized               = var.ebs_optimized
  enable_monitoring           = var.enable_monitoring
  iam_instance_profile        = var.iam_instance_profile
  image_id                    = var.image_id
  instance_type               = var.instance_type
  key_name                    = var.key_name
  placement_tenancy           = var.spot_price == "" ? var.placement_tenancy : ""
  security_groups             = var.security_groups
  spot_price                  = var.spot_price
  user_data                   = var.user_data
  user_data_base64            = var.user_data_base64

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      iops                  = lookup(ebs_block_device.value, "iops", null)
      no_device             = lookup(ebs_block_device.value, "no_device", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_device
    content {
      device_name  = ephemeral_block_device.value.device_name
      virtual_name = ephemeral_block_device.value.virtual_name
    }
  }

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

####################
# Autoscaling group
####################
resource "aws_autoscaling_group" "autoscaling-group" {
  count = var.create_asg ? 1 : 0

  name_prefix = "${join("-", compact([coalesce(var.asg_name, var.name),
  var.recreate_asg_when_lc_changes ? element(concat(random_pet.asg_name.*.id, [""]), 0) : "", ], ), )}-"

  default_cooldown          = var.default_cooldown
  desired_capacity          = var.desired_capacity
  enabled_metrics           = var.enabled_metrics
  force_delete              = var.force_delete
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  launch_configuration      = var.create_lc ? element(concat(aws_launch_configuration.launch-configuration.*.name, [""]), 0) : var.launch_configuration
  load_balancers            = var.load_balancers
  max_instance_lifetime     = var.max_instance_lifetime
  max_size                  = var.max_size
  metrics_granularity       = var.metrics_granularity
  min_elb_capacity          = var.min_elb_capacity
  min_size                  = var.min_size
  placement_group           = var.placement_group
  protect_from_scale_in     = var.protect_from_scale_in
  service_linked_role_arn   = var.service_linked_role_arn
  suspended_processes       = var.suspended_processes
  target_group_arns         = var.target_group_arns
  termination_policies      = var.termination_policies
  vpc_zone_identifier       = var.vpc_zone_identifier
  wait_for_capacity_timeout = var.wait_for_capacity_timeout
  wait_for_elb_capacity     = var.wait_for_elb_capacity

  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = var.name
        "propagate_at_launch" = true
      },
    ],
    var.tags,
    local.tags_asg_format,
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "random_pet" "asg_name" {
  count = var.recreate_asg_when_lc_changes ? 1 : 0

  separator = "-"
  length    = 2

  keepers = {
    # Generate a new pet name each time we switch launch configuration
    lc_name = var.create_lc ? element(concat(aws_launch_configuration.launch-configuration.*.name, [""]), 0) : var.launch_configuration
  }
}
