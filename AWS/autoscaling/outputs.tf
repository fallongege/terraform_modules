locals {
  launch_configuration_id   = var.launch_configuration == "" && var.create_lc ? concat(aws_launch_configuration.launch-configuration.*.id, [""])[0] : var.launch_configuration
  launch_configuration_name = var.launch_configuration == "" && var.create_lc ? concat(aws_launch_configuration.launch-configuration.*.name, [""])[0] : ""

  autoscaling_group_id                        = concat(aws_autoscaling_group.autoscaling-group.*.id, [""])[0]
  autoscaling_group_name                      = concat(aws_autoscaling_group.autoscaling-group.*.name, [""])[0]
  autoscaling_group_arn                       = concat(aws_autoscaling_group.autoscaling-group.*.arn, [""])[0]
  autoscaling_group_min_size                  = concat(aws_autoscaling_group.autoscaling-group.*.min_size, [""])[0]
  autoscaling_group_max_size                  = concat(aws_autoscaling_group.autoscaling-group.*.max_size, [""])[0]
  autoscaling_group_desired_capacity          = concat(aws_autoscaling_group.autoscaling-group.*.desired_capacity, [""])[0]
  autoscaling_group_default_cooldown          = concat(aws_autoscaling_group.autoscaling-group.*.default_cooldown, [""])[0]
  autoscaling_group_health_check_grace_period = concat(aws_autoscaling_group.autoscaling-group.*.health_check_grace_period, [""])[0]
  autoscaling_group_health_check_type         = concat(aws_autoscaling_group.autoscaling-group.*.health_check_type, [""])[0]
  autoscaling_group_availability_zones        = concat(aws_autoscaling_group.autoscaling-group.*.availability_zones, [""])[0]
  autoscaling_group_vpc_zone_identifier       = concat(aws_autoscaling_group.autoscaling-group.*.vpc_zone_identifier, [""])[0]
  autoscaling_group_load_balancers            = concat(aws_autoscaling_group.autoscaling-group.*.load_balancers, [""])[0]
  autoscaling_group_target_group_arns         = concat(aws_autoscaling_group.autoscaling-group.*.target_group_arns, [""])[0]
}

output "launch_configuration_id" {
  description = "The ID of the launch configuration"
  value       = local.launch_configuration_id
}

output "launch_configuration_name" {
  description = "The name of the launch configuration"
  value       = local.launch_configuration_name
}

output "autoscaling_group_id" {
  description = "The autoscaling group id"
  value       = local.autoscaling_group_id
}

output "autoscaling_group_name" {
  description = "The autoscaling group name"
  value       = local.autoscaling_group_name
}

output "autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group"
  value       = local.autoscaling_group_arn
}

output "autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group"
  value       = local.autoscaling_group_min_size
}

output "autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group"
  value       = local.autoscaling_group_max_size
}

output "autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  value       = local.autoscaling_group_desired_capacity
}

output "autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"
  value       = local.autoscaling_group_default_cooldown
}

output "autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health"
  value       = local.autoscaling_group_health_check_grace_period
}

output "autoscaling_group_health_check_type" {
  description = "EC2 or ELB. Controls how health checking is done"
  value       = local.autoscaling_group_health_check_type
}

output "autoscaling_group_availability_zones" {
  description = "The availability zones of the autoscale group"
  value       = local.autoscaling_group_availability_zones
}

output "autoscaling_group_vpc_zone_identifier" {
  description = "The VPC zone identifier"
  value       = local.autoscaling_group_vpc_zone_identifier
}

output "autoscaling_group_load_balancers" {
  description = "The load balancer names associated with the autoscaling group"
  value       = local.autoscaling_group_load_balancers
}

output "autoscaling_group_target_group_arns" {
  description = "List of Target Group ARNs that apply to this AutoScaling Group"
  value       = local.autoscaling_group_target_group_arns
}
