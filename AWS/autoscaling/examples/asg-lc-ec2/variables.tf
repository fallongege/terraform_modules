variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

variable "instance_type" {
  type    = string
  default = "t2.small"
}
