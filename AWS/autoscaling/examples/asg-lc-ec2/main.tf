data "template_file" "userdata" {
  template = file("${path.module}/userdata.tpl")
}

module "asg" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/autoscaling/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  create_lc                   = true
  associate_public_ip_address = true
  iam_instance_profile        = "iam_instance_profile"
  image_id                    = data.aws_ami.amazon-linux-2.id
  instance_type               = var.instance_type
  key_name                    = "ec2_key_pair"
  name                        = "asg"
  root_block_device = [
    {
      delete_on_termination = true
      volume_size           = "30"
      volume_type           = "gp2"
    },
  ]
  security_groups = ["sg-123456789"]
  user_data       = data.template_file.userdata.rendered

  create_asg                   = true
  asg_name                     = "asg"
  desired_capacity             = "1"
  health_check_type            = "EC2"
  max_size                     = "1"
  min_size                     = "1"
  recreate_asg_when_lc_changes = true
  tags = [
    {
      key                 = "Application"
      value               = "asg"
      propagate_at_launch = true
    },
    {
      key                 = "Department"
      value               = "pep-engineering"
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = var.environment
      propagate_at_launch = true
    },
    {
      key                 = "Managed"
      value               = "Terraform"
      propagate_at_launch = true
    },
    {
      key                 = "Team"
      value               = "pep-devops"
      propagate_at_launch = true
    },
  ]
  vpc_zone_identifier = ["vpc-123456789"]
}
