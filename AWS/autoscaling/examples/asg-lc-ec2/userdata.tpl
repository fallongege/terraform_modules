#!/bin/bash

yum update -y
yum install -y amazon-linux-extras wget curl jq python-pip gcc postgresql-devel
pip install pymysql psycopg2
