## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

- null

- random

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### desired\_capacity

Description: The number of Amazon EC2 instances that should be running in the group

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### health\_check\_type

Description: Controls how health checking is done. Values are - EC2 and ELB

Type: `string`

### max\_size

Description: The maximum size of the auto scale group

Type: `string`

### min\_size

Description: The minimum size of the auto scale group

Type: `string`

### name

Description: Creates a unique name beginning with the specified prefix

Type: `string`

### vpc\_zone\_identifier

Description: A list of subnet IDs to launch resources in

Type: `list(string)`

## Optional Inputs

The following input variables are optional (have default values):

### asg\_name

Description: Creates a unique name for autoscaling group beginning with the specified prefix

Type: `string`

Default: `""`

### associate\_public\_ip\_address

Description: Whether to associate a public ip address with an instance in a VPC

Type: `bool`

Default: `false`

### create\_asg

Description: Whether to create autoscaling group

Type: `bool`

Default: `true`

### create\_lc

Description: Whether to create launch configuration

Type: `bool`

Default: `true`

### default\_cooldown

Description: The amount of time, in seconds, after a scaling activity completes before another scaling activity can start

Type: `number`

Default: `300`

### ebs\_block\_device

Description: Additional EBS block devices to attach to the instance

Type: `list(map(string))`

Default: `[]`

### ebs\_optimized

Description: Whether thelaunched EC2 instance is EBS-optimized

Type: `bool`

Default: `false`

### enable\_monitoring

Description: Enables/disables detailed monitoring. This is enabled by default.

Type: `bool`

Default: `true`

### enabled\_metrics

Description: A list of metrics to collect. The allowed values are GroupMinSize, GroupMaxSize, GroupDesiredCapacity, GroupInServiceInstances, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances

Type: `list(string)`

Default:

```json
[
  "GroupMinSize",
  "GroupMaxSize",
  "GroupDesiredCapacity",
  "GroupInServiceInstances",
  "GroupPendingInstances",
  "GroupStandbyInstances",
  "GroupTerminatingInstances",
  "GroupTotalInstances"
]
```

### ephemeral\_block\_device

Description: Customize Ephemeral (also known as 'Instance Store') volumes on the instance

Type: `list(map(string))`

Default: `[]`

### force\_delete

Description: Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling

Type: `bool`

Default: `false`

### health\_check\_grace\_period

Description: Time (in seconds) after instance comes into service before checking health

Type: `number`

Default: `300`

### iam\_instance\_profile

Description: The IAM instance profile to associate with launched instances

Type: `string`

Default: `""`

### image\_id

Description: The EC2 image ID to launch

Type: `string`

Default: `""`

### instance\_type

Description: The size of instance to launch

Type: `string`

Default: `""`

### key\_name

Description: The key name that should be used for the instance

Type: `string`

Default: `""`

### launch\_configuration

Description: The name of the launch configuration to use (if it is created outside of this module)

Type: `string`

Default: `""`

### load\_balancers

Description: A list of elastic load balancer names to add to the autoscaling group names

Type: `list(string)`

Default: `[]`

### max\_instance\_lifetime

Description: The maximum amount of time, in seconds, that an instance can be in service, values must be either equal to 0 or between 604800 and 31536000 seconds.

Type: `number`

Default: `0`

### metrics\_granularity

Description: The granularity to associate with the metrics to collect. The only valid value is 1Minute

Type: `string`

Default: `"1Minute"`

### min\_elb\_capacity

Description: Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes

Type: `number`

Default: `0`

### placement\_group

Description: The name of the placement group into which you'll launch your instances, if any

Type: `string`

Default: `""`

### placement\_tenancy

Description: The tenancy of the instance. Valid values are 'default' or 'dedicated'

Type: `string`

Default: `"default"`

### protect\_from\_scale\_in

Description: Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events.

Type: `bool`

Default: `false`

### recreate\_asg\_when\_lc\_changes

Description: Whether to recreate an autoscaling group when launch configuration changes

Type: `bool`

Default: `false`

### root\_block\_device

Description: Customize details about the root block device of the instance

Type: `list(map(string))`

Default: `[]`

### security\_groups

Description: A list of security group IDs to assign to the launch configuration

Type: `list(string)`

Default: `[]`

### service\_linked\_role\_arn

Description: The ARN of the service-linked role that the ASG will use to call other AWS services.

Type: `string`

Default: `""`

### spot\_price

Description: The price to use for reserving spot instances

Type: `string`

Default: `""`

### suspended\_processes

Description: A list of processes to suspend for the AutoScaling Group. The allowed values are Launch, Terminate, HealthCheck, ReplaceUnhealthy, AZRebalance, AlarmNotification, ScheduledActions, AddToLoadBalancer. Note that if you suspend either the Launch or Terminate process types, it can prevent your autoscaling group from functioning properly.

Type: `list(string)`

Default: `[]`

### tags

Description: A list of tag blocks. Each element should have keys named key, value, and propagate\_at\_launch.

Type: `list(map(string))`

Default: `[]`

### tags\_as\_map

Description: A map of tags and values in the same format as other resources accept. This will be converted into the non-standard format that the aws\_autoscaling\_group requires.

Type: `map(string)`

Default: `{}`

### target\_group\_arns

Description: A list of aws\_alb\_target\_group ARNs, for use with Application Load Balancing

Type: `list(string)`

Default: `[]`

### termination\_policies

Description: A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, Default

Type: `list(string)`

Default:

```json
[
  "Default"
]
```

### user\_data

Description: The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see user\_data\_base64 instead.

Type: `string`

Default: `null`

### user\_data\_base64

Description: Can be used instead of user\_data to pass base64-encoded binary data directly. Use this instead of user\_data whenever the value is not a valid UTF-8 string. For example, gzip-encoded user data must be base64-encoded and passed via this argument to avoid corruption.

Type: `string`

Default: `null`

### wait\_for\_capacity\_timeout

Description: A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. (See also Waiting for Capacity below.) Setting this to '0' causes Terraform to skip all Capacity Waiting behavior.

Type: `string`

Default: `"10m"`

### wait\_for\_elb\_capacity

Description: Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load balancers on both create and update operations. Takes precedence over min\_elb\_capacity behavior.

Type: `number`

Default: `null`

## Outputs

The following outputs are exported:

### autoscaling\_group\_arn

Description: The ARN for this AutoScaling Group

### autoscaling\_group\_availability\_zones

Description: The availability zones of the autoscale group

### autoscaling\_group\_default\_cooldown

Description: Time between a scaling activity and the succeeding scaling activity

### autoscaling\_group\_desired\_capacity

Description: The number of Amazon EC2 instances that should be running in the group

### autoscaling\_group\_health\_check\_grace\_period

Description: Time after instance comes into service before checking health

### autoscaling\_group\_health\_check\_type

Description: EC2 or ELB. Controls how health checking is done

### autoscaling\_group\_id

Description: The autoscaling group id

### autoscaling\_group\_load\_balancers

Description: The load balancer names associated with the autoscaling group

### autoscaling\_group\_max\_size

Description: The maximum size of the autoscale group

### autoscaling\_group\_min\_size

Description: The minimum size of the autoscale group

### autoscaling\_group\_name

Description: The autoscaling group name

### autoscaling\_group\_target\_group\_arns

Description: List of Target Group ARNs that apply to this AutoScaling Group

### autoscaling\_group\_vpc\_zone\_identifier

Description: The VPC zone identifier

### launch\_configuration\_id

Description: The ID of the launch configuration

### launch\_configuration\_name

Description: The name of the launch configuration

