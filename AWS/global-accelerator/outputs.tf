output "global_accelerator_id" {
  value = aws_globalaccelerator_accelerator.accelerator.id
}

output "global_accelerator_dns_name" {
  value = aws_globalaccelerator_accelerator.accelerator.dns_name
}

output "global_accelerator_hosted_zone_id" {
  value = aws_globalaccelerator_accelerator.accelerator.hosted_zone_id
}

output "global_accelerator_ip_sets" {
  value = aws_globalaccelerator_accelerator.accelerator.ip_sets
}
