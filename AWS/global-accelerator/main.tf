resource "aws_globalaccelerator_accelerator" "accelerator" {
  name = "${local.name_prefix}-${var.accelerator_name}"

  enabled         = var.enabled
  ip_address_type = var.ip_address_type

  dynamic "attributes" {
    for_each = var.attribute

    content {
      flow_logs_enabled   = lookup(attributes.value, "flow_logs_enabled", null)
      flow_logs_s3_bucket = lookup(attributes.value, "flow_logs_s3_bucket", null)
      flow_logs_s3_prefix = lookup(attributes.value, "flow_logs_s3_prefix", null)
    }
  }

  tags = merge(
    {
      "Name" = "${local.name_prefix}-${var.accelerator_name}"
    },
    var.tags
  )
}

resource "aws_globalaccelerator_listener" "listener" {
  accelerator_arn = aws_globalaccelerator_accelerator.accelerator.id
  client_affinity = var.client_affinity
  protocol        = var.protocol

  dynamic "port_range" {
    for_each = var.ports

    content {
      from_port = lookup(port_range.value, "from_port", null)
      to_port   = lookup(port_range.value, "to_port", null)
    }
  }
}

resource "aws_globalaccelerator_endpoint_group" "endpoint-group" {
  endpoint_group_region         = var.endpoint_group_region
  health_check_path             = var.health_check_path
  health_check_port             = var.health_check_port
  health_check_interval_seconds = var.health_check_interval_seconds
  listener_arn                  = aws_globalaccelerator_listener.listener.id

  dynamic "endpoint_configuration" {
    for_each = var.endpoint

    content {
      client_ip_preservation_enabled = lookup(endpoint_configuration.value, "client_ip_preservation_enabled", null)
      endpoint_id                    = lookup(endpoint_configuration.value, "endpoint_id", null)
      weight                         = lookup(endpoint_configuration.value, "weight", null)
    }
  }
}
