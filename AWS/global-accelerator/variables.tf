##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

###############
# Global Accelerator variables
###############

variable "accelerator_name" {
  description = "The name of the accelerator"
  type        = string
}

variable "attribute" {
  description = "The attributes of the accelerator"
  type        = list(map(string))
  default     = []
}

variable "client_affinity" {
  description = "Direct all requests from a user to the same endpoint. Valid values are NONE, SOURCE_IP. Default: NONE. If NONE, Global Accelerator uses the \"five-tuple\" properties of source IP address, source port, destination IP address, destination port, and protocol to select the hash value. If SOURCE_IP, Global Accelerator uses the \"two-tuple\" properties of source (client) IP address and destination IP address to select the hash value."
  type        = string
  default     = "SOURCE_IP"
}

variable "enabled" {
  description = "Whether the accelerator is enabled"
  type        = bool
  default     = true
}

variable "endpoint" {
  description = "The attributes of the endpoint configuration"
  type        = list(map(any))
  default     = []
}

variable "endpoint_group_region" {
  description = "The name of the AWS Region where the endpoint group is located"
  type        = string
  default     = "us-west-2"
}

variable "health_check_path" {
  description = "If the protocol is HTTP/S, then this specifies the path that is the destination for health check targets. The default value is slash (/). Terraform will only perform drift detection of its value when present in a configuration."
  type        = string
  default     = null
}

variable "health_check_port" {
  description = "The port that AWS Global Accelerator uses to check the health of endpoints that are part of this endpoint group. The default port is the listener port that this endpoint group is associated with. If listener port is a list of ports, Global Accelerator uses the first port in the list. Terraform will only perform drift detection of its value when present in a configuration."
  type        = number
  default     = 80
}

variable "health_check_interval_seconds" {
  description = "The time—10 seconds or 30 seconds—between each health check for an endpoint. The default value is 30"
  type        = number
  default     = 30
}

variable "ip_address_type" {
  description = "The IP address type"
  type        = string
  default     = "IPV4"
}

variable "ports" {
  description = "The list of port ranges for the connections from clients to the accelerator. Fields documented below."
  type        = list(any)
  default     = []
}

variable "protocol" {
  description = "The protocol for the connections from clients to the accelerator. Valid values are TCP, UDP"
  type        = string
  default     = "TCP"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
