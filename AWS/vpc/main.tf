resource "aws_vpc" "vpc" {
  count = var.create_vpc ? 1 : 0

  cidr_block                       = var.cidr_block
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.enable_ipv6
  instance_tenancy                 = var.instance_tenancy

  tags = merge(
    {
      "Name" = "${var.name}-vpc"
    },
    local.vpc_tags
  )
}
