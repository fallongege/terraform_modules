## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### bastion\_subnets

Description: A list of bastion subnets inside the VPC

Type: `list(string)`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### cidr\_block

Description: The CIDR block for the VPC

Type: `string`

### database\_subnets

Description: A list of database subnets inside the VPC

Type: `list(string)`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### internal\_dns\_zone

Description: The name of the internal Route53 hosted zone

Type: `string`

### private\_subnets

Description: A list of private subnets inside the VPC

Type: `list(string)`

### public\_key

Description: The public ssh key

Type: `string`

### public\_subnets

Description: A list of public subnets inside the VPC

Type: `list(string)`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

## Optional Inputs

The following input variables are optional (have default values):

### amazon\_side\_asn

Description: The Autonomous System Number (ASN) for the Amazon side of the gateway. By default the virtual private gateway is created with the current default Amazon ASN.

Type: `string`

Default: `"64512"`

### bastion\_subnet\_suffix

Description: Suffix to append to bastion subnets name

Type: `string`

Default: `"bastion"`

### create\_igw

Description: Whether the IGW should be created

Type: `bool`

Default: `true`

### create\_internal\_dns\_zone

Description: Whether to create an internal Route53 hosted zone

Type: `bool`

Default: `true`

### create\_key\_pair

Description: Whether a key pair is created

Type: `bool`

Default: `true`

### create\_vpc

Description: Whether the VPC should be created

Type: `bool`

Default: `true`

### customer\_gateway\_tags

Description: Additional tags for the Customer Gateway

Type: `map(string)`

Default: `{}`

### customer\_gateways

Description: Maps of Customer Gateway's attributes (BGP ASN and Gateway's Internet-routable external IP address)

Type: `map(map(any))`

Default: `{}`

### database\_subnet\_suffix

Description: Suffix to append to database subnets name

Type: `string`

Default: `"db"`

### enable\_dns\_hostnames

Description: Whether to enable DNS hostnames in the VPC

Type: `bool`

Default: `true`

### enable\_dns\_support

Description: Whether to enable DNS support in the VPC

Type: `bool`

Default: `true`

### enable\_ipv6

Description: Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block.

Type: `bool`

Default: `false`

### enable\_nat\_gateway

Description: Whether a NAT Gateway should be created

Type: `bool`

Default: `true`

### enable\_s3\_endpoint

Description: Whether an S3 VPC endpoints should be created

Type: `bool`

Default: `true`

### enable\_vpn\_gateway

Description: Whether a VPN Gateway should be created

Type: `bool`

Default: `false`

### instance\_tenancy

Description: A tenancy option for instances launched into the VPC

Type: `string`

Default: `"default"`

### map\_public\_ip\_on\_launch

Description: Whether to auto-assign public IP on launch

Type: `bool`

Default: `true`

### private\_subnet\_suffix

Description: Suffix to append to private subnets name

Type: `string`

Default: `"private"`

### public\_subnet\_suffix

Description: Suffix to append to public subnets name

Type: `string`

Default: `"public"`

## Outputs

The following outputs are exported:

### bastion\_subnet\_arns

Description: List of ARNs of bastion subnets

### bastion\_subnets

Description: List of IDs of bastion subnets

### bastion\_subnets\_cidr\_blocks

Description: List of cidr\_blocks of bastion subnets

### cgw\_arns

Description: List of ARNs of Customer Gateway

### cgw\_ids

Description: List of IDs of Customer Gateway

### database\_subnet\_arns

Description: List of ARNs of database subnets

### database\_subnets

Description: List of IDs of database subnets

### database\_subnets\_cidr\_blocks

Description: List of cidr\_blocks of database subnets

### igw\_id

Description: The ID of the Internet Gateway

### internal\_dns\_zone\_id

Description: The ID of the Route53 hosted zone

### internal\_dns\_zone\_name

Description: The name of the Route53 hosted zone

### internal\_dns\_zone\_name\_servers

Description: The name servers of the Route53 hosted zone

### key\_pair

Description: The name of the ssh key

### nat\_eip\_ids

Description: List of allocation ID of Elastic IPs created for AWS NAT Gateway

### nat\_public\_ips

Description: List of public Elastic IPs created for AWS NAT Gateway

### natgw\_ids

Description: List of NAT Gateway IDs

### private\_ipv6\_egress\_route\_ids

Description: List of IDs of the ipv6 egress route.

### private\_nat\_gateway\_route\_ids

Description: List of IDs of the private nat gateway route.

### private\_route\_table\_ids

Description: List of IDs of private route tables

### private\_subnet\_arns

Description: List of ARNs of private subnets

### private\_subnets

Description: List of IDs of private subnets

### private\_subnets\_cidr\_blocks

Description: List of cidr\_blocks of private subnets

### public\_internet\_gateway\_ipv6\_route\_id

Description: ID of the IPv6 internet gateway route.

### public\_internet\_gateway\_route\_id

Description: ID of the internet gateway route.

### public\_route\_table\_ids

Description: List of IDs of public route tables

### public\_subnet\_arns

Description: List of ARNs of public subnets

### public\_subnets

Description: List of IDs of public subnets

### public\_subnets\_cidr\_blocks

Description: List of cidr\_blocks of public subnets

### vgw\_arn

Description: The ARN of the VPN Gateway

### vgw\_id

Description: The ID of the VPN Gateway

### vpc\_arn

Description: The ARN of the VPC

### vpc\_cidr\_block

Description: The CIDR block of the VPC

### vpc\_endpoint\_s3\_id

Description: The ID of VPC endpoint for S3

### vpc\_id

Description: The ID of the VPC

### vpc\_name

Description: The Name of the VPC

