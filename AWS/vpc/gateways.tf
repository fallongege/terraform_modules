################
# Internet Gateway
################
resource "aws_internet_gateway" "igw" {
  count = var.create_vpc && var.create_igw && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.vpc[0].id
  tags = merge(
    {
      "Name" = "${var.name}-igw"
    },
    local.vpc_tags
  )
}

################
# VPN Gateway
################
resource "aws_vpn_gateway" "vpg" {
  count = var.create_vpc && var.enable_vpn_gateway ? 1 : 0

  vpc_id          = aws_vpc.vpc[0].id
  amazon_side_asn = var.amazon_side_asn
  tags = merge(
    {
      "Name" = "${var.name}-vpg"
    },
    local.vpc_tags
  )
}

resource "aws_vpn_gateway_attachment" "vpg" {
  count = var.create_vpc && var.enable_vpn_gateway ? 1 : 0

  vpc_id         = aws_vpc.vpc[0].id
  vpn_gateway_id = element(aws_vpn_gateway.vpg.*.id, count.index)
}

################
# NAT Gateway
################
resource "aws_eip" "ngw-eip" {
  count = var.create_vpc && var.enable_nat_gateway ? local.az_count : 0

  vpc = true

  tags = merge(
    {
      "Name" = "${var.name}-ngw-eip-${element(data.aws_availability_zones.azs.names, count.index)}"
    },
    local.vpc_tags
  )
}

resource "aws_nat_gateway" "ngw" {
  count = var.create_vpc && var.enable_nat_gateway ? local.az_count : 0

  allocation_id = element(aws_eip.ngw-eip.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)

  tags = merge(
    {
      "Name" = "${var.name}-ngw-${element(data.aws_availability_zones.azs.names, count.index)}"
    },
    local.vpc_tags
  )
  depends_on = [aws_internet_gateway.igw]
}

####################
# Customer Gateways
####################
resource "aws_customer_gateway" "cgw" {
  for_each = var.customer_gateways

  bgp_asn    = each.value["bgp_asn"]
  ip_address = each.value["ip_address"]
  type       = "ipsec.1"

  tags = merge(
    {
      Name = format("%s-%s", var.name, each.key)
    },
    local.vpc_tags,
    var.customer_gateway_tags
  )
}
