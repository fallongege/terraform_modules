locals {
  az_count = var.az_count == null ? length(data.aws_availability_zones.azs.names) : var.az_count

  max_subnet_length = max(
    length(var.private_subnets),
    length(var.bastion_subnets),
    length(var.database_subnets),
    length(var.public_subnets),
  )

  vpc_tags = merge(
    var.tags
  )
}
