################
# Bastion subnet
################
resource "aws_subnet" "bastion" {
  count = var.create_vpc && length(var.bastion_subnets) > 0 ? local.az_count : 0

  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = element(concat(var.bastion_subnets, [""]), count.index)
  availability_zone_id    = element(data.aws_availability_zones.azs.zone_ids, count.index)
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = merge(
    {
      "Name" = "${var.name}-${var.bastion_subnet_suffix}-subnet-${element(data.aws_availability_zones.azs.names, count.index)}"
    },
    var.tags,
  )
}

################
# Database subnet
################
resource "aws_subnet" "database" {
  count = var.create_vpc && length(var.database_subnets) > 0 ? local.az_count : 0

  vpc_id               = aws_vpc.vpc[0].id
  cidr_block           = element(concat(var.database_subnets, [""]), count.index)
  availability_zone_id = element(data.aws_availability_zones.azs.zone_ids, count.index)

  tags = merge(
    {
      "Name" = "${var.name}-${var.database_subnet_suffix}-subnet-${element(data.aws_availability_zones.azs.names, count.index)}"
    },
    var.tags,
  )
}

################
# Private subnet
################
resource "aws_subnet" "private" {
  count = var.create_vpc && length(var.private_subnets) > 0 ? local.az_count : 0

  vpc_id               = aws_vpc.vpc[0].id
  cidr_block           = element(concat(var.private_subnets, [""]), count.index)
  availability_zone_id = element(data.aws_availability_zones.azs.zone_ids, count.index)

  tags = merge(
    {
      "Name" = "${var.name}-${var.private_subnet_suffix}-subnet-${element(data.aws_availability_zones.azs.names, count.index)}"
    },
    var.tags,
  )
}

################
# Public subnet
################
resource "aws_subnet" "public" {
  count = var.create_vpc && length(var.public_subnets) > 0 ? local.az_count : 0

  vpc_id                  = aws_vpc.vpc[0].id
  cidr_block              = element(concat(var.public_subnets, [""]), count.index)
  availability_zone_id    = element(data.aws_availability_zones.azs.zone_ids, count.index)
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = merge(
    {
      "Name" = "${var.name}-${var.public_subnet_suffix}-subnet-${element(data.aws_availability_zones.azs.names, count.index)}"
      "kubernetes.io/cluster/eks-${var.environment}" = "shared"
    },
    var.tags,
  )
}
