################
# Global variables
################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Locals
################

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

################
# VPC variables
################

variable "create_vpc" {
  description = "Whether the VPC should be created"
  type        = bool
  default     = true
}

variable "az_count" {
  description = "The number of AZ's to use"
  type        = number
  default     = null
}

variable "cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
}

variable "enable_dns_hostnames" {
  description = "Whether to enable DNS hostnames in the VPC"
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "Whether to enable DNS support in the VPC"
  type        = bool
  default     = true
}

variable "enable_ipv6" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block."
  type        = bool
  default     = false
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type        = string
  default     = "default"
}

variable "name" {
  description = "The name of the VPC"
  type        = string
}

################
# Subnet variables
################

variable "map_public_ip_on_launch" {
  description = "Whether to auto-assign public IP on launch"
  type        = bool
  default     = true
}

variable "bastion_subnet_suffix" {
  description = "Suffix to append to bastion subnets name"
  type        = string
  default     = "bastion"
}

variable "bastion_subnets" {
  description = "A list of bastion subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "database_subnet_suffix" {
  description = "Suffix to append to database subnets name"
  type        = string
  default     = "db"
}

variable "database_subnets" {
  description = "A list of database subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "private_subnet_suffix" {
  description = "Suffix to append to private subnets name"
  type        = string
  default     = "private"
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
}

variable "public_subnet_suffix" {
  description = "Suffix to append to public subnets name"
  type        = string
  default     = "public"
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
}

################
# Gateway variables
################

variable "amazon_side_asn" {
  description = "The Autonomous System Number (ASN) for the Amazon side of the gateway. By default the virtual private gateway is created with the current default Amazon ASN."
  default     = "64512"
}

variable "create_igw" {
  description = "Whether the IGW should be created"
  type        = bool
  default     = true
}

variable "customer_gateway_tags" {
  description = "Additional tags for the Customer Gateway"
  type        = map(string)
  default     = {}
}

variable "customer_gateways" {
  description = "Maps of Customer Gateway's attributes (BGP ASN and Gateway's Internet-routable external IP address)"
  type        = map(map(any))
  default     = {}
}

variable "enable_nat_gateway" {
  description = "Whether a NAT Gateway should be created"
  type        = bool
  default     = true
}

variable "enable_vpn_gateway" {
  description = "Whether a VPN Gateway should be created"
  type        = bool
  default     = false
}

################
# Endpoint variables
################

variable "enable_s3_endpoint" {
  description = "Whether an S3 VPC endpoints should be created"
  type        = bool
  default     = true
}

################
# Key Pair variables
################

variable "create_key_pair" {
  description = "Whether a key pair is created"
  type        = bool
  default     = true
}

variable "public_key" {
  description = "The public ssh key"
  type        = string
  default     = ""
}

################
# Route variables
################

variable "create_custom_routes" {
  description = "Whether to create custom routes"
  type        = bool
  default     = false
}

variable "custom_routes" {
  description = "List of maps of custom routes to create"
  type        = list(map(any))
  default     = [{}]
}

################
# Route53 variables
################

variable "create_internal_dns_zone" {
  description = "Whether to create an internal Route53 hosted zone"
  type        = bool
  default     = true
}

variable "internal_dns_zone" {
  description = "The name of the internal Route53 hosted zone"
  type        = string
  default     = ""
}
