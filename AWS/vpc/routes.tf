################
# Publiс routes
################
resource "aws_route_table" "public" {
  count = var.create_vpc && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.vpc[0].id
  tags = merge(
    {
      "Name" = "${var.name}-${var.public_subnet_suffix}-rtb-${count.index}"
    },
    var.tags
  )
}

resource "aws_route" "public-internet-gateway" {
  count = var.create_vpc && var.create_igw && length(var.public_subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw[0].id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "public-internet-gateway-ipv6" {
  count = var.create_vpc && var.create_igw && var.enable_ipv6 && length(var.public_subnets) > 0 ? 1 : 0

  route_table_id              = aws_route_table.public[0].id
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.igw[0].id
}

#################
# Private routes
#################
resource "aws_route_table" "private" {
  count = var.create_vpc && local.max_subnet_length > 0 ? 1 : 0

  vpc_id = aws_vpc.vpc[0].id
  tags = merge(
    {
      "Name" = "${var.name}-${var.private_subnet_suffix}-rtb-${count.index}"
    },
    var.tags,
  )

  lifecycle {
    ignore_changes = [propagating_vgws]
  }
}

resource "aws_route" "private-ngw" {
  count = var.create_vpc && var.enable_nat_gateway ? 1 : 0

  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.ngw.*.id, count.index)

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private-ngw-ipv6" {
  count = var.create_vpc && var.enable_nat_gateway && var.enable_ipv6 ? local.az_count : 0

  route_table_id              = element(aws_route_table.private.*.id, count.index)
  destination_ipv6_cidr_block = "::/0"
  nat_gateway_id              = element(aws_nat_gateway.ngw.*.id, count.index)

  timeouts {
    create = "5m"
  }
}

#################
# Route table associations
#################
resource "aws_route_table_association" "private" {
  count = var.create_vpc && length(var.private_subnets) > 0 ? local.az_count : 0

  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "database" {
  count = var.create_vpc && length(var.database_subnets) > 0 ? local.az_count : 0

  subnet_id      = element(aws_subnet.database.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "bastion" {
  count = var.create_vpc && length(var.bastion_subnets) > 0 ? local.az_count : 0

  subnet_id      = element(aws_subnet.bastion.*.id, count.index)
  route_table_id = element(aws_route_table.public.*.id, count.index)
}

resource "aws_route_table_association" "public" {
  count = var.create_vpc && length(var.public_subnets) > 0 ? local.az_count : 0

  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = element(aws_route_table.public.*.id, count.index)
}

#################
# Custom Routes
#################
resource "aws_route" "custom_routes" {
  count = var.create_custom_routes ? length(var.custom_routes) : 0

  destination_cidr_block    = lookup(var.custom_routes[count.index], "destination_cidr_block")
  network_interface_id      = lookup(var.custom_routes[count.index], "network_interface_id", null)
  route_table_id            = lookup(var.custom_routes[count.index], "route_table_id")
  vpc_peering_connection_id = lookup(var.custom_routes[count.index], "vpc_peering_connection_id", null)

  timeouts {
    create = "5m"
  }
}
