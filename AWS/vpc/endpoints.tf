######################
# VPC Endpoint for S3
######################
data "aws_vpc_endpoint_service" "s3" {
  count        = var.create_vpc && var.enable_s3_endpoint ? 1 : 0
  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  count = var.create_vpc && var.enable_s3_endpoint ? 1 : 0

  vpc_id       = aws_vpc.vpc[0].id
  service_name = data.aws_vpc_endpoint_service.s3[0].service_name
  tags = merge(
    {
      "Name" = "${var.name}-${data.aws_vpc_endpoint_service.s3[0].service_name}-endpoint"
    },
    local.vpc_tags
  )
}

resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count = var.create_vpc && var.enable_s3_endpoint ? local.az_count : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = element(aws_route_table.private.*.id, count.index)
}

resource "aws_vpc_endpoint_route_table_association" "public_s3" {
  count = var.create_vpc && var.enable_s3_endpoint && length(var.public_subnets) > 0 ? 1 : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = aws_route_table.public[0].id
}
