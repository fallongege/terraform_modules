resource "aws_route53_zone" "internal-dns-zone" {
  count = var.create_vpc && var.create_internal_dns_zone ? 1 : 0

  name = var.internal_dns_zone

  vpc {
    vpc_id = aws_vpc.vpc[0].id
  }

  tags = merge(
    {
      "Name" = var.internal_dns_zone
    },
    var.tags
  )
}
