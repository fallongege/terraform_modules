output "vpc_id" {
  description = "The ID of the VPC"
  value       = concat(aws_vpc.vpc.*.id, [""])[0]
}

output "vpc_arn" {
  description = "The ARN of the VPC"
  value       = concat(aws_vpc.vpc.*.arn, [""])[0]
}

output "vpc_name" {
  description = "The Name of the VPC"
  value       = format("%s", var.name)
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = concat(aws_vpc.vpc.*.cidr_block, [""])[0]
}

output "key_pair" {
  description = "The name of the ssh key"
  value       = concat(aws_key_pair.key-pair.*.key_name, [""])[0]
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = aws_subnet.private.*.id
}

output "private_subnet_arns" {
  description = "List of ARNs of private subnets"
  value       = aws_subnet.private.*.arn
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = aws_subnet.private.*.cidr_block
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public.*.id
}

output "public_subnet_arns" {
  description = "List of ARNs of public subnets"
  value       = aws_subnet.public.*.arn
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = aws_subnet.public.*.cidr_block
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = aws_subnet.database.*.id
}

output "database_subnet_arns" {
  description = "List of ARNs of database subnets"
  value       = aws_subnet.database.*.arn
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value       = aws_subnet.database.*.cidr_block
}

output "bastion_subnets" {
  description = "List of IDs of bastion subnets"
  value       = aws_subnet.bastion.*.id
}

output "bastion_subnet_arns" {
  description = "List of ARNs of bastion subnets"
  value       = aws_subnet.bastion.*.arn
}

output "bastion_subnets_cidr_blocks" {
  description = "List of cidr_blocks of bastion subnets"
  value       = aws_subnet.bastion.*.cidr_block
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = aws_route_table.public.*.id
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = aws_route_table.private.*.id
}

output "public_internet_gateway_route_id" {
  description = "ID of the internet gateway route."
  value       = concat(aws_route.public-internet-gateway.*.id, [""])[0]
}

output "public_internet_gateway_ipv6_route_id" {
  description = "ID of the IPv6 internet gateway route."
  value       = concat(aws_route.public-internet-gateway-ipv6.*.id, [""])[0]
}

output "private_nat_gateway_route_ids" {
  description = "List of IDs of the private nat gateway route."
  value       = aws_route.private-ngw.*.id
}

output "private_ipv6_egress_route_ids" {
  description = "List of IDs of the ipv6 egress route."
  value       = aws_route.private-ngw-ipv6.*.id
}

output "nat_eip_ids" {
  description = "List of allocation ID of Elastic IPs created for AWS NAT Gateway"
  value       = aws_eip.ngw-eip.*.id
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = aws_eip.ngw-eip.*.public_ip
}

output "natgw_ids" {
  description = "List of NAT Gateway IDs"
  value       = aws_nat_gateway.ngw.*.id
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = concat(aws_internet_gateway.igw.*.id, [""])[0]
}

output "cgw_ids" {
  description = "List of IDs of Customer Gateway"
  value       = [for k, v in aws_customer_gateway.cgw : v.id]
}

output "cgw_arns" {
  description = "List of ARNs of Customer Gateway"
  value       = [for k, v in aws_customer_gateway.cgw : v.arn]
}

output "vgw_id" {
  description = "The ID of the VPN Gateway"
  value = concat(
    aws_vpn_gateway.vpg.*.id,
    aws_vpn_gateway_attachment.vpg.*.vpn_gateway_id,
    [""],
  )[0]
}

output "vgw_arn" {
  description = "The ARN of the VPN Gateway"
  value       = concat(aws_vpn_gateway.vpg.*.arn, [""])[0]
}

output "vpc_endpoint_s3_id" {
  description = "The ID of VPC endpoint for S3"
  value       = concat(aws_vpc_endpoint.s3.*.id, [""])[0]
}

output "internal_dns_zone_id" {
  description = "The ID of the Route53 hosted zone"
  value       = concat(aws_route53_zone.internal-dns-zone.*.id, [""])[0]
}

output "internal_dns_zone_name" {
  description = "The name of the Route53 hosted zone"
  value       = concat(aws_route53_zone.internal-dns-zone.*.name, [""])[0]
}

output "internal_dns_zone_name_servers" {
  description = "The name servers of the Route53 hosted zone"
  value       = concat(aws_route53_zone.internal-dns-zone.*.name_servers, [""])[0]
}
