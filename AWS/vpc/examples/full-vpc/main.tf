module "vpc" {
  source            = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/vpc/?ref=main"
  bastion_subnets   = split(",", lookup(var.bastion_subnets, var.environment))
  business_unit     = var.business_unit
  cidr_block        = lookup(var.cidr_block, var.environment)
  database_subnets  = split(",", lookup(var.database_subnets, var.environment))
  environment       = var.environment
  internal_dns_zone = "${var.business_unit}.internal"
  private_subnets   = split(",", lookup(var.private_subnets, var.environment))
  public_key        = var.public_key
  public_subnets    = split(",", lookup(var.public_subnets, var.environment))

  tags = {
    Application = "vpc"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
