resource "aws_key_pair" "key-pair" {
  count = var.create_key_pair ? 1 : 0

  key_name   = "${var.name}-${aws_vpc.vpc[0].id}"
  public_key = var.public_key

  tags = merge(
    {
      "Name" = "${var.name}-${aws_vpc.vpc[0].id}"
    },
    var.tags
  )
}
