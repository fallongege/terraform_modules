locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  cluster_identifier              = "${local.name_prefix}-${var.cluster_identifier}-cluster"
  documentdb_parameter_group_name = "${local.name_prefix}-${var.documentdb_parameter_group_name}"
  documentdb_subnet_group_name    = "${local.name_prefix}-${var.documentdb_subnet_group_name}"
  instance_identifier             = "${local.name_prefix}-${var.instance_identifier}-instance"
}
