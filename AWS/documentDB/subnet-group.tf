resource "aws_docdb_subnet_group" "document-db" {
  name = local.documentdb_subnet_group_name

  description = local.documentdb_subnet_group_name
  subnet_ids  = var.subnet_ids

  tags = merge(
    {
      "Name" = local.documentdb_subnet_group_name
    },
    var.tags
  )
}
