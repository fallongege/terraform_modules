##################
# Global variables
##################
variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

######################
# DocumentDB variables
######################

variable "apply_immediately" {
  description = "Whether any cluster modifications are applied immediately, or during the next maintenance window"
  type        = bool
  default     = true
}

variable "availability_zones" {
  description = "A list of vailability zone IDs. The maximum number of AZ's that can be specified is 3"
  type        = list(string)
  default     = []
}

variable "backup_retention_period" {
  description = "The number of days to retain backups for"
  type        = number
  default     = 7
}

variable "cluster_identifier" {
  description = "The identifier (name) of the cluster"
  type        = string
  default     = ""
}

variable "cluster_family" {
  description = "The family of the DocumentDB cluster parameter group. For more details, see https://docs.aws.amazon.com/documentdb/latest/developerguide/db-cluster-parameter-group-create.html"
  type        = string
  default     = "docdb3.6"
}

variable "cluster_parameters" {
  description = "List of DB parameters to apply"
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = []
}

variable "cluster_size" {
  description = "Number of DB instances to create in the cluster"
  type        = number
  default     = 3
}

variable "documentdb_parameter_group_name" {
  description = "The family of the DocumentDB cluster parameter group. For more details, see https://docs.aws.amazon.com/documentdb/latest/developerguide/db-cluster-parameter-group-create.html"
  type        = string
  default     = ""
}

variable "documentdb_subnet_group_name" {
  description = "The name of the DocumentDB subnet group"
  type        = string
  default     = ""
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: audit, profiler"
  type        = list(string)
  default     = []
}

variable "engine" {
  description = "The name of the database engine to be used for this DB cluster. Defaults to `docdb`. Valid values: `docdb`"
  type        = string
  default     = "docdb"
}

variable "engine_version" {
  description = "The version number of the database engine to use"
  type        = string
  default     = "3.6.0"
}

variable "instance_class" {
  description = "The instance class to use. For more details, see https://docs.aws.amazon.com/documentdb/latest/developerguide/db-instance-classes.html#db-instance-class-specs"
  type        = string
  default     = "db.t3.medium"
}

variable "instance_identifier" {
  description = "The identifier (name) of the instance(s)"
  type        = string
  default     = ""
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying `kms_key_id`, `storage_encrypted` needs to be set to `true`"
  type        = any
  default     = ""
}

variable "local_exec_command" {
  description = "The local exec command that is to be executed on documentDB cluster creation, if needed"
  type        = any
  default     = ""
}

variable "master_username" {
  description = "Required unless a snapshot_identifier is provided. Username for the master DB user"
  type        = string
  default     = ""
}

variable "master_password" {
  description = "Required unless a snapshot_identifier is provided. Password for the master DB user"
  type        = string
  default     = ""
}

variable "port" {
  description = "The DocumentDB port"
  type        = number
  default     = 27017
}

variable "preferred_backup_window" {
  description = "Daily time range during which the backups happen"
  type        = string
  default     = "07:00-09:00"
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB cluster is deleted"
  type        = bool
  default     = false
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot"
  type        = string
  default     = ""
}

variable "storage_encrypted" {
  description = "Specifies whether the DB cluster is encrypted"
  type        = bool
  default     = false
}

variable "subnet_ids" {
  description = "List of VPC subnet IDs to place DocumentDB instances in"
  type        = any
  default     = []
}

variable "tags" {
  description = "A map of tags to add to the DocumentDB resources"
  type        = map(string)
  default     = {}
}

variable "vpc_security_group_ids" {
  description = "List of Security Groups to be allowed to connect to the DocumentDB cluster"
  type        = list(string)
  default     = []
}
