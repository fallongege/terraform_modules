output "documentdb_arn" {
  description = "The ARN of the DocumentDB cluster"
  value       = join("", aws_docdb_cluster.document-db.*.arn)
}

output "documentdb_cluster_name" {
  description = "The Cluster Identifier of the DocumentDB cluster"
  value       = join("", aws_docdb_cluster.document-db.*.cluster_identifier)
}

output "documentdb_endpoint" {
  value       = join("", aws_docdb_cluster.document-db.*.endpoint)
  description = "The Endpoint of the DocumentDB cluster"
}

output "documentdb_port" {
  description = "The port of the DocumentDB cluster"
  value       = join("", aws_docdb_cluster.document-db.*.port)
}

output "documentdb_reader_endpoint" {
  description = "The read-only endpoint of the DocumentDB cluster"
  value       = join("", aws_docdb_cluster.document-db.*.reader_endpoint)
}

output "documentdb_username" {
  description = "Username for the primary DocumentDB user"
  value       = join("", aws_docdb_cluster.document-db.*.master_username)
}
