module "documentdb-secret" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/secrets-manager/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  description = "The username and password of the documentdb cluster"
  kms_key_id  = "some-kms-key-id"
  secret_name = "documentdb-secret"
}

module "documentdb-sg" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/security-group/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  name = "documentdb"

  create_sg   = true
  description = "Security group that is attached to the documentdb cluster"
  vpc_id      = "vpc-123456789"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }

  create_ingress_with_cidr_blocks = true
  ingress_with_cidr_blocks = [
    {
      rule        = "All from vpc cidr"
      cidr_blocks = "10.0.0.0/16"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "all from vpc cidr"
    },
    {
      rule        = "all from VPN"
      cidr_blocks = "10.0.0.0/8"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "all from VPN"
    },
  ]

  create_egress_with_cidr_blocks = true
  egress_with_cidr_blocks = [
    {
      rule        = "Allow all egress traffic to vpc cidr"
      cidr_blocks = "10.0.0.0/16"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      description = "Allow all egress traffic to vpc cidr"
    }
  ]
}

locals {
  documentdb_master_username = lookup(var.database_username, format("documentdb%s%s", "-", var.environment))
}

module "documentdb" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/documentDB/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  availability_zones              = slice(data.aws_availability_zones.azs.names, 0, 3)
  backup_retention_period         = lookup(var.backup_retention_period, var.environment)
  cluster_identifier              = "documentdb"
  cluster_size                    = lookup(var.documentdb_cluster_size, var.environment)
  documentdb_parameter_group_name = "docdb36"
  documentdb_subnet_group_name    = "documentdb-subnet-group"
  engine_version                  = "3.6.0"
  instance_class                  = lookup(var.documentDB_instance_class, var.environment)
  instance_identifier             = "documentdb"
  local_exec_command              = <<EOT
      DB_PASS=$(openssl rand -base64 16)
      DB_USER=${local.documentdb_master_username}
      SECRET_STRING="{\"username\":\"$${DB_USER}\",\"password\":\"$${DB_PASS}\"}"
      assume_role=$(aws sts assume-role --role-arn arn:aws:iam::${var.aws_account_id}:role/Devops-Admin --role-session-name ${var.business_unit}-${var.environment})
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
      unset AWS_SESSION_TOKEN
      export AWS_ACCESS_KEY_ID=$(echo $assume_role | jq -r '.Credentials.AccessKeyId')
      export AWS_SECRET_ACCESS_KEY=$(echo $assume_role | jq -r '.Credentials.SecretAccessKey')
      export AWS_SESSION_TOKEN=$(echo $assume_role | jq -r '.Credentials.SessionToken')
      aws secretsmanager put-secret-value --secret-id ${module.documentdb-secret.secret_name} --region ${var.aws_region} --secret-string $SECRET_STRING
      aws docdb modify-db-cluster --db-cluster-identifier ${local.name_prefix}-documentdb-cluster --master-user-password $DB_PASS --apply-immediately --region ${var.aws_region}
    EOT
  master_username                 = local.documentdb_master_username
  master_password                 = "password"
  subnet_ids                      = "subnet-13435845"
  tags = {
    Application = "microservices"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  vpc_security_group_ids = [module.documentdb-sg.security_group_id]
}
