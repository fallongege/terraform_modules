variable "aws_account_id" {
  default = "925863785861"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

######################
# Database Variables
######################
variable "backup_retention_period" {
  description = "A map of the environments speciyfing the number of days to keep database backups"
  type        = map(number)

  default = {
    stg  = 7
    prod = 14
  }
}

variable "deletion_protection" {
  description = "A map of the environments speciyfing whether the cluster has deletion protectionf enabled"
  type        = map(bool)

  default = {
    stg  = false
    prod = true
  }
}

######################
# DocumentDB Variables
######################
variable "documentdb_cluster_size" {
  description = "A map of the environments speciyfing the number of instances in the documentDB cluster"
  type        = map(number)

  default = {
    stg  = 2
    prod = 3
  }
}

variable "documentDB_instance_class" {
  description = "A map of the environments specifying the documentDB instance class"
  type        = map(string)

  default = {
    stg  = "db.t3.medium"
    prod = "db.t3.medium"
  }
}

variable "database_username" {
  description = "A map of the database usernames per database per environment. MySQL limitation is 16 characters"
  type        = map(string)

  default = {
    aurora-mysql-stg     = "AuroraMsStgAdmin"
    aurora-mysql-prod    = "AuroraMsPrdAdmin"
    aurora-postgres-stg  = "AuroraPgStgAdmin"
    aurora-postgres-prod = "AuroraPgPrdAdmin"
    documentdb-stg       = "DocDbStgAdmin"
    documentdb-prod      = "DocDbPrdAdmin"
  }
}
