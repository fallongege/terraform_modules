resource "aws_docdb_cluster_parameter_group" "document-db" {
  name = local.documentdb_parameter_group_name

  description = local.documentdb_parameter_group_name
  family      = var.cluster_family

  dynamic "parameter" {
    for_each = var.cluster_parameters
    content {
      apply_method = lookup(parameter.value, "apply_method", null)
      name         = parameter.value.name
      value        = parameter.value.value
    }
  }

  tags = merge(
    {
      "Name" = local.documentdb_parameter_group_name
    },
    var.tags
  )
}
