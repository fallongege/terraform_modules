resource "aws_docdb_cluster" "document-db" {
  cluster_identifier = local.cluster_identifier

  apply_immediately               = var.apply_immediately
  availability_zones              = var.availability_zones
  backup_retention_period         = var.backup_retention_period
  db_subnet_group_name            = join("", aws_docdb_subnet_group.document-db.*.name)
  db_cluster_parameter_group_name = join("", aws_docdb_cluster_parameter_group.document-db.*.name)
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  engine                          = var.engine
  engine_version                  = var.engine_version
  final_snapshot_identifier       = "${local.cluster_identifier}-final-snapshot-${random_id.snapshot_identifier.hex}"
  kms_key_id                      = var.kms_key_id
  master_username                 = var.master_username
  master_password                 = var.master_password
  port                            = var.port
  preferred_backup_window         = var.preferred_backup_window
  skip_final_snapshot             = var.skip_final_snapshot
  storage_encrypted               = var.storage_encrypted
  snapshot_identifier             = var.snapshot_identifier
  vpc_security_group_ids          = [join("", var.vpc_security_group_ids)]

  tags = merge(
    {
      "Name" = local.cluster_identifier
    },
    var.tags
  )

  lifecycle {
    ignore_changes = [master_password]
  }

  provisioner "local-exec" {
    command     = var.local_exec_command
    interpreter = ["/bin/bash", "-c"]
  }
}

resource "aws_docdb_cluster_instance" "document-db" {
  count = var.cluster_size

  identifier = "${local.instance_identifier}-${count.index}"

  apply_immediately  = var.apply_immediately
  cluster_identifier = join("", aws_docdb_cluster.document-db.*.id)
  instance_class     = var.instance_class
  engine             = var.engine

  tags = merge(
    {
      "Name" = local.instance_identifier
    },
    var.tags
  )
}

resource "random_id" "snapshot_identifier" {
  keepers = {
    id = local.cluster_identifier
  }

  byte_length = 4
}
