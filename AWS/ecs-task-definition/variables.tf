##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

###############################
# ECS Task Definition variables
###############################

variable "command" {
  description = "A string array representing the command that the container runs to determine if it is healthy"
  type        = list(string)
  default     = []
}

variable "container_port" {
  description = "The port that the container is running on"
  type        = number
  default     = 0
}

variable "depends_on_task" {
  description = "The dependencies defined for container startup and shutdown"
  type        = any
  default     = ""
}

variable "disable_networking" {
  description = "When this parameter is true, networking is disabled within the container"
  type        = bool
  default     = false
}

variable "dns_servers" {
  description = "A list of DNS servers that are presented to the container"
  type        = list(string)
  default     = []
}

variable "docker_labels" {
  description = "A key/value map of labels to add to the container"
  type        = map(string)
  default     = {}
}

variable "docker_repo" {
  description = "The name of the docker repository where the image is stored"
  type        = string
  default     = ""
}

variable "docker_security_options" {
  description = "A list of strings to provide custom labels for SELinux and AppArmor multi-level security systems"
  type        = list(map(string))
  default     = []
}

variable "entrypoint" {
  description = "The entry point that is passed to the container"
  type        = list(string)
  default     = []
}

variable "environment_files" {
  description = "A list of files containing the environment variables to pass to a container"
  type        = list(map(string))
  default     = []
}

variable "environment_variables" {
  description = "The environment variables to pass to a container"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "essential" {
  description = <<EOF
  If the essential parameter of a container is marked as true, and that container fails or stops for any reason,
  all other containers that are part of the task are stopped. If the essential parameter of a container is marked
  as false, then its failure does not affect the rest of the containers in a task.
  EOF
  type        = bool
  default     = true
}

variable "extra_hosts" {
  description = "A list of hostnames and IP address mappings to append to the /etc/hosts file on the container"
  type        = list(map(string))
  default     = []
}

variable "firelens_configuration" {
  description = "The FireLens configuration for the container"
  type        = any
  default     = []
}

variable "health_check" {
  description = "The container health check command and associated configuration parameters for the container"
  type        = list(string)
  default     = []
}

variable "host_port" {
  description = "The port number on the container instance to reserve for your container."
  type        = number
  default     = 0
}

variable "hostname" {
  description = "The hostname to use for your container"
  type        = string
  default     = ""
}

variable "image_tag" {
  description = "The tag of the docker image used to start the container"
  type        = string
  default     = ""
}

variable "interactive" {
  description = <<EOF
  When this parameter is true, this allows you to deploy containerized applications
  that require stdin or a tty to be allocated.
  EOF
  type        = bool
  default     = true
}

variable "ipc_mode" {
  description = <<EOF
  The IPC resource namespace to use for the containers in the task. The valid values are host, task, or none.
  If host is specified, then all containers within the tasks that specified the host IPC mode on the same container
  instance share the same IPC resources with the host Amazon EC2 instance. If task is specified, all containers within
  the specified task share the same IPC resources. If none is specified, then IPC resources within the containers of a
  task are private and not shared with other containers in a task or on the container instance.
  EOF
  type        = string
  default     = "none"
}

variable "links" {
  description = "Whether to allow containers to communicate with each other without the need for port mappings"
  type        = list(string)
  default     = []
}

variable "linux_parameters" {
  description = "Linux-specific options that are applied to the container, such as KernelCapabilities."
  type        = any
  default     = {}
}

variable "log_driver" {
  description = <<EOF
  The log driver to use for the container.
  Valid values: "awslogs","fluentd","gelf","json-file","journald","logentries",
  "splunk","syslog","awsfirelens
  If using Fargate launch type, only supported value is awslogs.
  EOF
  type        = string
  default     = "awslogs"
}

variable "log_options" {
  description = "The configuration options to send to the `log_driver`"
  type = object({
    awslogs-region        = any
    awslogs-group         = any
    awslogs-stream-prefix = any
  })
  default = {
    "awslogs-region"        = "us-west-2"
    "awslogs-group"         = "default"
    "awslogs-stream-prefix" = "default"
  }
}

variable "mount_points" {
  description = "The mount points for data volumes in your container"
  type        = list(map(string))
  default     = []
}

variable "pid_mode" {
  description = <<EOF
  The process namespace to use for the containers in the task. The valid values are host or task.
  If host is specified, then all containers within the tasks that specified the host PID mode on the same container
  instance share the same process namespace with the host Amazon EC2 instance. If task is specified,
  all containers within the specified task share the same process namespace.
  EOF
  type        = string
  default     = "task"
}

variable "placement_constraints" {
  description = "The contstraints that customize how Amazon ECS places tasks"
  type        = list(string)
  default     = []
}

variable "privileged" {
  description = "When this parameter is true, the container is given elevated privileges on the host container instance"
  type        = bool
  default     = false
}

variable "protocol" {
  description = "The protocol used for the port mapping. Valid values are: tcp | udp"
  type        = string
  default     = "tcp"
}

variable "proxy_configuration" {
  description = "The configuration details for the App Mesh proxy."
  type        = any
  default     = []
}

variable "pseudo_terminal" {
  description = "When this parameter is true, a TTY is allocated"
  type        = bool
  default     = false
}

variable "read_only_root_file_system" {
  description = "When this parameter is true, the container is given read-only access to its root file system"
  type        = bool
  default     = false
}

variable "repository_credentials" {
  description = "The credentials that are used to authenticate with the docker repo"
  type        = map(any)
  default     = {}
}

variable "resource_requirements" {
  description = "The type and amount of a resource to assign to a container"
  type        = list(string)
  default     = []
}

variable "secrets" {
  description = "An object representing the secret to expose to your container"
  type        = list(map(string))
  default     = []
}

variable "service_name" {
  description = "The name of the ecs service"
  type        = string
  default     = ""
}

variable "start_timeout" {
  description = "Time duration (in seconds) to wait before giving up on resolving dependencies for a container"
  type        = number
  default     = 180
}

variable "stop_timeout" {
  description = "Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on its own"
  type        = number
  default     = 30
}

variable "system_controls" {
  description = "A list of namespaced kernel parameters to set in the container"
  type        = list(map(string))
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "ulimits" {
  description = "A list of ulimits to set in the container"
  type        = list(string)
  default     = []
}

variable "user" {
  description = "The user name to use inside the container."
  type        = string
  default     = ""
}

variable "volumes" {
  description = "A list of volumes to be passed to the Docker daemon on a container instance"
  type        = list(map(string))
  default     = []
}

variable "volumes_from" {
  description = "Data volumes to mount from another container"
  type        = map(string)
  default     = {}
}

variable "working_directory" {
  description = "The working directory in which to run commands inside the container"
  type        = string
  default     = ""
}
