variable "aws_region" {
  default = "us-west-2"
}

variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}
