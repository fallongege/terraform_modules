module "task-definition" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/ecs-task-definition/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Task Definition arguments
  container_port = 80
  docker_repo    = "docker_org/docker_repo"
  environment_variables = [
    {
      name  = "ENVIRONMENT",
      value = var.environment
    },
    {
      name  = "EXAMPLE_VAR",
      value = "foo"
    },
  ]
  host_port = 80
  image_tag = "235facad"
  log_options = {
    "awslogs-region"        = var.aws_region
    "awslogs-group"         = module.cloudwatch-logs.cloudwatch_log_group_name
    "awslogs-stream-prefix" = var.cloudwatch_stream_prefix
  }
  repository_credentials = {
    "credentialsParameter" = module.secret.secret_arn
  }
  secrets = [
    {
      valueFrom = module.secret.secret_arn
      name      = "SECRET"
    }
  ]
  service_name = "container"
  tags = {
    Application = "container"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}