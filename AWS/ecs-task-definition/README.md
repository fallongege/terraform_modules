## Requirements

No requirements.

## Providers

No provider.

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### command

Description: A string array representing the command that the container runs to determine if it is healthy

Type: `list(string)`

Default: `[]`

### container\_port

Description: The port that the container is running on

Type: `number`

Default: `0`

### depends\_on\_task

Description: The dependencies defined for container startup and shutdown

Type: `any`

Default: `""`

### disable\_networking

Description: When this parameter is true, networking is disabled within the container

Type: `bool`

Default: `false`

### dns\_servers

Description: A list of DNS servers that are presented to the container

Type: `list(string)`

Default: `[]`

### docker\_labels

Description: A key/value map of labels to add to the container

Type: `map(string)`

Default: `{}`

### docker\_repo

Description: The name of the docker repository where the image is stored

Type: `string`

Default: `""`

### docker\_security\_options

Description: A list of strings to provide custom labels for SELinux and AppArmor multi-level security systems

Type: `list(map(string))`

Default: `[]`

### entrypoint

Description: The entry point that is passed to the container

Type: `list(string)`

Default: `[]`

### environment\_files

Description: A list of files containing the environment variables to pass to a container

Type: `list(map(string))`

Default: `[]`

### environment\_variables

Description: The environment variables to pass to a container

Type:

```hcl
list(object({
    name  = string
    value = string
  }))
```

Default: `[]`

### essential

Description:   If the essential parameter of a container is marked as true, and that container fails or stops for any reason,  
  all other containers that are part of the task are stopped. If the essential parameter of a container is marked  
  as false, then its failure does not affect the rest of the containers in a task.

Type: `bool`

Default: `true`

### extra\_hosts

Description: A list of hostnames and IP address mappings to append to the /etc/hosts file on the container

Type: `list(map(string))`

Default: `[]`

### firelens\_configuration

Description: The FireLens configuration for the container

Type: `any`

Default: `[]`

### health\_check

Description: The container health check command and associated configuration parameters for the container

Type: `list(string)`

Default: `[]`

### host\_port

Description: The port number on the container instance to reserve for your container.

Type: `number`

Default: `0`

### hostname

Description: The hostname to use for your container

Type: `string`

Default: `""`

### image\_tag

Description: The tag of the docker image used to start the container

Type: `string`

Default: `""`

### interactive

Description:   When this parameter is true, this allows you to deploy containerized applications  
  that require stdin or a tty to be allocated.

Type: `bool`

Default: `true`

### ipc\_mode

Description:   The IPC resource namespace to use for the containers in the task. The valid values are host, task, or none.  
  If host is specified, then all containers within the tasks that specified the host IPC mode on the same container  
  instance share the same IPC resources with the host Amazon EC2 instance. If task is specified, all containers within  
  the specified task share the same IPC resources. If none is specified, then IPC resources within the containers of a  
  task are private and not shared with other containers in a task or on the container instance.

Type: `string`

Default: `"none"`

### links

Description: Whether to allow containers to communicate with each other without the need for port mappings

Type: `list(string)`

Default: `[]`

### linux\_parameters

Description: Linux-specific options that are applied to the container, such as KernelCapabilities.

Type: `any`

Default: `{}`

### log\_driver

Description:   The log driver to use for the container.  
  Valid values: "awslogs","fluentd","gelf","json-file","journald","logentries",
  "splunk","syslog","awsfirelens  
  If using Fargate launch type, only supported value is awslogs.

Type: `string`

Default: `"awslogs"`

### log\_options

Description: The configuration options to send to the `log_driver`

Type:

```hcl
object({
    awslogs-region        = any
    awslogs-group         = any
    awslogs-stream-prefix = any
  })
```

Default:

```json
{
  "awslogs-group": "default",
  "awslogs-region": "us-west-2",
  "awslogs-stream-prefix": "default"
}
```

### mount\_points

Description: The mount points for data volumes in your container

Type: `list(map(string))`

Default: `[]`

### pid\_mode

Description:   The process namespace to use for the containers in the task. The valid values are host or task.  
  If host is specified, then all containers within the tasks that specified the host PID mode on the same container  
  instance share the same process namespace with the host Amazon EC2 instance. If task is specified,  
  all containers within the specified task share the same process namespace.

Type: `string`

Default: `"task"`

### placement\_constraints

Description: The contstraints that customize how Amazon ECS places tasks

Type: `list(string)`

Default: `[]`

### privileged

Description: When this parameter is true, the container is given elevated privileges on the host container instance

Type: `bool`

Default: `false`

### protocol

Description: The protocol used for the port mapping. Valid values are: tcp \| udp

Type: `string`

Default: `"tcp"`

### proxy\_configuration

Description: The configuration details for the App Mesh proxy.

Type: `any`

Default: `[]`

### pseudo\_terminal

Description: When this parameter is true, a TTY is allocated

Type: `bool`

Default: `false`

### read\_only\_root\_file\_system

Description: When this parameter is true, the container is given read-only access to its root file system

Type: `bool`

Default: `false`

### repository\_credentials

Description: The credentials that are used to authenticate with the docker repo

Type: `map(any)`

Default: `{}`

### resource\_requirements

Description: The type and amount of a resource to assign to a container

Type: `list(string)`

Default: `[]`

### secrets

Description: An object representing the secret to expose to your container

Type: `list(map(string))`

Default: `[]`

### service\_name

Description: The name of the ecs service

Type: `string`

Default: `""`

### start\_timeout

Description: Time duration (in seconds) to wait before giving up on resolving dependencies for a container

Type: `number`

Default: `180`

### stop\_timeout

Description: Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on its own

Type: `number`

Default: `30`

### system\_controls

Description: A list of namespaced kernel parameters to set in the container

Type: `list(map(string))`

Default: `[]`

### tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

### ulimits

Description: A list of ulimits to set in the container

Type: `list(string)`

Default: `[]`

### user

Description: The user name to use inside the container.

Type: `string`

Default: `""`

### volumes

Description: A list of volumes to be passed to the Docker daemon on a container instance

Type: `list(map(string))`

Default: `[]`

### volumes\_from

Description: Data volumes to mount from another container

Type: `map(string)`

Default: `{}`

### working\_directory

Description: The working directory in which to run commands inside the container

Type: `string`

Default: `""`

## Outputs

The following outputs are exported:

### json

Description: JSON encoded container definitions for use with other terraform resources such as aws\_ecs\_task\_definition.

### json\_map

Description: JSON encoded container definitions for use with other terraform resources such as aws\_ecs\_task\_definition.

