## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### aws\_region

Description: The aws region that is being deployed into

Type: `string`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### acceleration\_status

Description: (Optional) Sets the accelerate configuration of an existing bucket. Can be Enabled or Suspended.

Type: `string`

Default: `null`

### acl

Description: (Optional) The canned ACL to apply. Defaults to 'private'.

Type: `string`

Default: `"private"`

### bucket

Description: (Optional, Forces new resource) The name of the bucket. If omitted, Terraform will assign a random, unique name.

Type: `string`

Default: `""`

### bucket\_prefix

Description: (Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix. Conflicts with bucket.

Type: `string`

Default: `null`

### cors\_rule

Description: List of maps containing rules for Cross-Origin Resource Sharing.

Type: `list(any)`

Default: `[]`

### create\_bucket

Description: Whether an S3 bucket should be created

Type: `bool`

Default: `false`

### force\_destroy

Description: (Optional, Default:false ) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable.

Type: `bool`

Default: `false`

### lifecycle\_rule

Description: List of maps containing configuration of object lifecycle management.

Type: `any`

Default: `[]`

### logging

Description: Map containing access bucket logging configuration.

Type: `map(string)`

Default: `{}`

### object\_key

Description: The S3 key where the object will be uploaded

Type: `string`

Default: `""`

### object\_lock\_configuration

Description: Map containing S3 object locking configuration.

Type: `any`

Default: `{}`

### object\_source

Description: The path where the file exists locally

Type: `string`

Default: `""`

### policy

Description: (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide.

Type: `string`

Default: `null`

### replication\_configuration

Description: Map containing cross-region replication configuration.

Type: `any`

Default: `{}`

### request\_payer

Description: (Optional) Specifies who should bear the cost of Amazon S3 data transfer. Can be either BucketOwner or Requester. By default, the owner of the S3 bucket would incur the costs of any data transfer. See Requester Pays Buckets developer guide for more information.

Type: `string`

Default: `null`

### s3\_bucket\_name

Description: The name of the s3 bucket where the object is being uploaded

Type: `string`

Default: `""`

### server\_side\_encryption\_configuration

Description: Map containing server-side encryption configuration.

Type: `any`

Default: `{}`

### tags

Description: (Optional) A mapping of tags to assign to the bucket.

Type: `map(string)`

Default: `{}`

### upload\_object

Description: Wheter to upload an object to s3

Type: `bool`

Default: `false`

### versioning

Description: Map containing versioning configuration.

Type: `map(string)`

Default: `{}`

### website

Description: Map containing static web-site hosting or redirect configuration. Valid parameters are: error\_document, index\_document, redirect\_all\_requests\_to, routing\_rules

Type: `map(string)`

Default: `{}`

## Outputs

The following outputs are exported:

### s3\_bucket\_arn

Description: The ARN of the bucket. Will be of format arn:aws:s3:::bucketname.

### s3\_bucket\_bucket\_domain\_name

Description: The bucket domain name. Will be of format bucketname.s3.amazonaws.com.

### s3\_bucket\_bucket\_regional\_domain\_name

Description: The bucket region-specific domain name. The bucket domain name including the region name, please refer here for format. Note: The AWS CloudFront allows specifying S3 region-specific endpoint when creating S3 origin, it will prevent redirect issues from CloudFront to S3 Origin URL.

### s3\_bucket\_hosted\_zone\_id

Description: The Route 53 Hosted Zone ID for this bucket's region.

### s3\_bucket\_id

Description: The name of the bucket.

### s3\_bucket\_region

Description: The AWS region this bucket resides in.

### s3\_bucket\_website\_domain

Description: The domain of the website endpoint, if the bucket is configured with a website. If not, this will be an empty string. This is used to create Route 53 alias records.

### s3\_bucket\_website\_endpoint

Description: The website endpoint, if the bucket is configured with a website. If not, this will be an empty string.

### s3\_object\_key

Description: The key where the object is uploaded

