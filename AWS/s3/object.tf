resource "aws_s3_bucket_object" "object" {
  count = var.upload_object ? 1 : 0

  bucket = var.s3_bucket_name
  etag   = filemd5(var.object_source)
  key    = var.object_key
  source = var.object_source
}
