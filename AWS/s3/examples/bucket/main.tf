module "devops-s3-bucket" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/s3/?ref=main"

  aws_region    = var.aws_region
  business_unit = var.business_unit
  environment   = var.environment

  create_bucket = true
  bucket        = "pmcdevops"
  tags = {
    Application = "devops"
    Department  = "pep-engineering"
    Environment = var.environment
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
