resource "aws_s3_bucket_policy" "policy" {
  count = var.create_bucket && var.attach_policy ? 1 : 0

  bucket = var.s3_bucket_policy_attach
  policy = var.policy
}
