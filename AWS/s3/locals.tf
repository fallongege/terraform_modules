locals {
  name_prefix = "tf-${var.business_unit}-${var.environment}"

  bucket_name = length(keys(var.website)) == 0 ? "${local.name_prefix}-${var.bucket}-${var.aws_region}" : "${local.name_prefix}-${var.bucket}.${var.domain}"
}
