####################
# Listener variables
####################

variable "create_http_listener" {
  description = "Whether an http listener should be created on the load balancer"
  type        = bool
  default     = false
}

variable "create_https_listener" {
  description = "Whether an https listener should be created on the load balancer"
  type        = bool
  default     = false
}

variable "create_listener_rule" {
  description = "Whether to create a load balancer listener rule"
  type        = bool
  default     = false
}

variable "extra_ssl_certs" {
  description = "A list of maps describing any extra SSL certificates to apply to the HTTPS listeners. Required key/values: certificate_arn, https_listener_index (the index of the listener within https_listeners which the cert applies toward)."
  type        = list(map(string))
  default     = []
}

variable "http_tcp_listeners" {
  description = <<EOF
  A list of maps describing the HTTP listeners or TCP ports for this ALB.
  Required key/values: port, protocol.
  Optional key/values: target_group_index (defaults to http_tcp_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "https_listeners" {
  description = <<EOF
  A list of maps describing the HTTPS listeners for this ALB.
  Required key/values: port, certificate_arn.
  Optional key/values: ssl_policy (defaults to ELBSecurityPolicy-2016-08),
                       target_group_index (defaults to https_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "listener_arn" {
  description = "The ARN of the load balancer listener"
  type        = string
  default     = ""
}

variable "listener_rules" {
  description = <<EOF
  A list of maps describing the Listener Rules for this ALB.
  Required key/values: actions, conditions.
  Optional key/values: priority, https_listener_index (default to https_listeners[count.index])
  EOF
  type        = any
  default     = []
}

variable "listener_ssl_policy_default" {
  description = "The security policy if using HTTPS externally on the load balancer"
  type        = string
  default     = "ELBSecurityPolicy-2016-08"
}

variable "load_balancer_arn" {
  description = "The arn of the LB"
  type        = string
}

variable "target_group_arn" {
  description = "The arn of the target group"
  type        = any
  default     = ""
}
