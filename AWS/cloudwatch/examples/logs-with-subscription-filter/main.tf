module "cloudwatch-logs" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/cloudwatch/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Cloudwatch Logs arguments
  create_log_group = true
  cloudwatch_log_group = [
    {
      cloudwatch_retention     = lookup(var.cloudwatch_retention, var.environment)
      cloudwatch_stream_prefix = var.cloudwatch_stream_prefix
    }
  ]
  log_group_tags = {
    Application = "api"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
  service_name = "api"

  # Cloudwatch log subscription filter arguments
  create_log_subscription_filter = true
  log_subscription_filter = [
    {
      name            = "tyk-gateway"
      destination_arn = "arn:aws:lambda:us-west-2:123456789:function:some-lambda-function"
      filter_pattern  = ""
      log_group_name  = module.cloudwatch-logs.cloudwatch_log_group_name
    }
  ]
}
