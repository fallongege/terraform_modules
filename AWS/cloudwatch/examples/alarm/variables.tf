variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

variable "metric_name" {
  default = "CPUUtilization"
}

variable "scale_down_threshold" {
  description = "The threshold at which the CloudWatch Alarm is triggered to scale up the container's running count"
  default     = 40
}

variable "scale_up_threshold" {
  description = "The threshold at which the CloudWatch Alarm is triggered to scale down the container's running count"
  default     = 80
}
