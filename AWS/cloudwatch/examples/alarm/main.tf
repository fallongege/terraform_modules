module "cloudwatch-alarm" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/cloudwatch/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Cloudwatch Alarm arguments
  service_name = "subscriber-authorization-api"

  create_metric_alarm = true
  cloudwatch_alarm = [
    {
      alarm_identifier    = "up"
      alarm_actions       = [element(split(",", module.api-service.fargate_autoscaling_policy_up_arn), 0)]
      comparison_operator = "GreaterThanOrEqualToThreshold"
      evaluation_periods  = "1"
      metric_name         = var.metric_name
      namespace           = "AWS/ECS"
      period              = "300"
      statistic           = "Average"
      threshold           = var.scale_up_threshold
    },
    {
      alarm_identifier    = "down"
      alarm_actions       = [element(split(",", module.api-service.fargate_autoscaling_policy_down_arn), 1)]
      comparison_operator = "LessThanOrEqualToThreshold"
      evaluation_periods  = "1"
      metric_name         = var.metric_name
      namespace           = "AWS/ECS"
      period              = "300"
      statistic           = "Average"
      threshold           = var.scale_down_threshold
    },
  ]
}
