module "cloudwatch-logs" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/cloudwatch/?ref=main"

  # Global arguments
  business_unit = var.business_unit
  environment   = var.environment

  # Cloudwatch Logs arguments
  create_log_group = true
  cloudwatch_log_group = [
    {
      cloudwatch_retention     = lookup(var.cloudwatch_retention, var.environment)
      cloudwatch_stream_prefix = var.cloudwatch_stream_prefix
    }
  ]
  log_group_tags = {
    Application = "microservices"
    Department  = "pmc-audience-marketing"
    Environment = var.environment
    Managed     = "Terraform"
    Team        = "pep-subscriptions"
  }
  service_name = "api"
}
