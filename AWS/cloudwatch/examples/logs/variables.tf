variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

variable "cloudwatch_retention" {
  description = "The amount of time, in days, that CloudWatch Logs will be stored for"
  type        = map(number)

  default = {
    stg  = 3
    prod = 1
  }
}

variable "cloudwatch_stream_prefix" {
  description = "The prefix of the CloudWatch Logs Group"
  default     = "/aws/fargate/"
}
