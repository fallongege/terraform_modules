resource "aws_cloudwatch_log_group" "cloudwatch-log-group" {
  count = var.create_log_group ? length(var.cloudwatch_log_group) : 0

  name = "${lookup(var.cloudwatch_log_group[count.index], "cloudwatch_stream_prefix")}tf-${var.business_unit}-${var.environment}-${var.service_name}"

  retention_in_days = lookup(var.cloudwatch_log_group[count.index], "cloudwatch_retention")

  tags = merge(
    {
      "Name" = "${lookup(var.cloudwatch_log_group[count.index], "cloudwatch_stream_prefix")}tf-${var.business_unit}-${var.environment}-${var.service_name}"
    },
    var.log_group_tags
  )
}

resource "aws_cloudwatch_log_subscription_filter" "cloudwatch-log-subscription-filter" {
  count = var.create_log_subscription_filter ? length(var.log_subscription_filter) : 0

  name = "tf-${var.business_unit}-${var.environment}-${lookup(var.log_subscription_filter[count.index], "name")}"

  destination_arn = lookup(var.log_subscription_filter[count.index], "destination_arn")
  filter_pattern  = lookup(var.log_subscription_filter[count.index], "filter_pattern")
  log_group_name  = lookup(var.log_subscription_filter[count.index], "log_group_name")
}
