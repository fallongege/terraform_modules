resource "aws_cloudwatch_metric_alarm" "cloudwatch-alarm" {
  count = var.create_metric_alarm ? length(var.cloudwatch_alarm) : 0

  alarm_name = "tf-${var.business_unit}-${var.environment}-${var.service_name}-${lookup(var.cloudwatch_alarm[count.index], "metric_name")}-${lookup(var.cloudwatch_alarm[count.index], "alarm_identifier")}"

  actions_enabled                       = var.actions_enabled
  alarm_actions                         = lookup(var.cloudwatch_alarm[count.index], "alarm_actions")
  alarm_description                     = "Alarm is triggered when ${lookup(var.cloudwatch_alarm[count.index], "namespace")} ${lookup(var.cloudwatch_alarm[count.index], "metric_name")} is ${lookup(var.cloudwatch_alarm[count.index], "comparison_operator")} ${lookup(var.cloudwatch_alarm[count.index], "threshold")}"
  comparison_operator                   = lookup(var.cloudwatch_alarm[count.index], "comparison_operator")
  datapoints_to_alarm                   = var.datapoints_to_alarm
  evaluate_low_sample_count_percentiles = var.evaluate_low_sample_count_percentiles
  evaluation_periods                    = lookup(var.cloudwatch_alarm[count.index], "evaluation_periods")
  insufficient_data_actions             = var.insufficient_data_actions
  metric_name                           = lookup(var.cloudwatch_alarm[count.index], "metric_name")
  namespace                             = lookup(var.cloudwatch_alarm[count.index], "namespace")
  ok_actions                            = var.ok_actions
  period                                = lookup(var.cloudwatch_alarm[count.index], "period")
  statistic                             = lookup(var.cloudwatch_alarm[count.index], "statistic")
  threshold                             = lookup(var.cloudwatch_alarm[count.index], "threshold")
  treat_missing_data                    = var.treat_missing_data
  unit                                  = var.unit

  dimensions = var.dimensions

  tags = merge(
    {
      "Name" = "tf-${var.business_unit}-${var.environment}-${var.service_name}-${lookup(var.cloudwatch_alarm[count.index], "metric_name")}-${lookup(var.cloudwatch_alarm[count.index], "alarm_identifier")}"
    },
    var.alarm_tags
  )
}
