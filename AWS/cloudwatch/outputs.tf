##########################
# Cloudwatch Alarm outputs
##########################

output "cloudwatch_metric_alarm_arn" {
  description = "The ARN of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_metric_alarm.cloudwatch-alarm.*.arn)
}

output "cloudwatch_metric_alarm_id" {
  description = "The ID of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_metric_alarm.cloudwatch-alarm.*.id)
}

#########################
# Cloudwatch Logs outputs
#########################

output "cloudwatch_log_group_arn" {
  description = "The ARN of the Cloudwatch log group"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.arn)
}

output "cloudwatch_log_group_id" {
  description = "The ID of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.id)
}

output "cloudwatch_log_group_name" {
  description = "The Name of the Cloudwatch metric alarm"
  value       = join("", aws_cloudwatch_log_group.cloudwatch-log-group.*.name)
}

output "cloudwatch_log_group_prefix" {
  description = "The Prefix of the Cloudwatch metric alarm"
  value       = length(aws_cloudwatch_log_group.cloudwatch-log-group) > 0 ? aws_cloudwatch_log_group.cloudwatch-log-group[0].id : null
}

###########################
# Cloudwatch Events outputs
###########################

output "cloudwatch_event_rule_arn" {
  description = "The ARN of the Cloudwatch event rule"
  value       = join("", aws_cloudwatch_event_rule.cloudwatch-event-rule.*.arn)
}

output "cloudwatch_event_rule_id" {
  description = "The ID of the Cloudwatch event rule"
  value       = join("", aws_cloudwatch_event_rule.cloudwatch-event-rule.*.id)
}

output "cloudwatch_event_rule_name" {
  description = "The Name of the Cloudwatch event rule"
  value       = join("", aws_cloudwatch_event_rule.cloudwatch-event-rule.*.name)
}

output "cloudwatch_event_target_arn" {
  description = "The ARN of the Cloudwatch event target"
  value       = join("", aws_cloudwatch_event_target.cloudwatch-event-target.*.arn)
}

output "cloudwatch_event_target_id" {
  description = "The ID of the Cloudwatch event target"
  value       = join("", aws_cloudwatch_event_target.cloudwatch-event-target.*.id)
}
