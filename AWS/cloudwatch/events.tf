resource "aws_cloudwatch_event_rule" "cloudwatch-event-rule" {
  count = var.create_event_rule ? length(var.cloudwatch_event_rule) : 0

  name = lookup(var.cloudwatch_event_rule[count.index], "name")

  description         = lookup(var.cloudwatch_event_rule[count.index], "description")
  event_pattern       = lookup(var.cloudwatch_event_rule[count.index], "event_pattern", null)
  is_enabled          = lookup(var.cloudwatch_event_rule[count.index], "is_enabled", true)
  role_arn            = lookup(var.cloudwatch_event_rule[count.index], "role_arn", null)
  schedule_expression = lookup(var.cloudwatch_event_rule[count.index], "schedule_expression", null)

  tags = merge(
    {
      "Name" = lookup(var.cloudwatch_event_rule[count.index], "name")
    },
    var.event_rule_tags
  )
}

resource "aws_cloudwatch_event_target" "cloudwatch-event-target" {
  count = var.create_event_target ? 1 : 0

  arn       = var.cloudwatch_event_target_arn
  rule      = var.cloudwatch_event_rule_name
  target_id = var.cloudwatch_event_target_id
}
