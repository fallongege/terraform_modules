##################
# Global variables
##################
variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

################
# Locals
################

variable "service_name" {
  description = "The name of the service that is being monitored by the Cloudwatch alarm"
  type        = string
  default     = ""
}

############################
# Cloudwatch Alarm variables
############################

variable "create_metric_alarm" {
  description = "Whether to create the Cloudwatch metric alarm"
  type        = bool
  default     = false
}

variable "actions_enabled" {
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state"
  type        = bool
  default     = true
}

variable "alarm_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "cloudwatch_alarm" {
  description = <<EOF
  List of Cloudwatch alarm settings including: alarm_identifier, alarm_actions, comparison_operator, evaluation_periods,
  metric_name, namespace, period, statistic, threshold
  EOF
  type = list(object({
    alarm_identifier    = any
    alarm_actions       = any
    comparison_operator = any
    evaluation_periods  = any
    metric_name         = any
    namespace           = any
    period              = any
    statistic           = any
    threshold           = any
  }))
  default = [
    {
      alarm_identifier    = ""
      alarm_actions       = ""
      comparison_operator = ""
      evaluation_periods  = ""
      metric_name         = ""
      namespace           = ""
      period              = ""
      statistic           = ""
      threshold           = ""
    }
  ]
}

variable "datapoints_to_alarm" {
  description = "The number of datapoints that must be breaching to trigger the alarm"
  type        = number
  default     = null
}

variable "dimensions" {
  description = "The dimensions for the alarm's associated metric"
  type        = any
  default     = null
}

variable "evaluate_low_sample_count_percentiles" {
  description = <<EOF
  Used only for alarms based on percentiles. If you specify ignore, the alarm state will not change during periods with
  too few data points to be statistically significant. If you specify evaluate or omit this parameter, the alarm will
  always be evaluated and possibly change state no matter how many data points are available. The following values are
  supported: ignore | evaluate
  EOF
  type        = string
  default     = null
}

variable "insufficient_data_actions" {
  description = "The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state"
  type        = list(string)
  default     = null
}

variable "ok_actions" {
  description = "The list of actions to execute when this alarm transitions into an OK state from any other state"
  type        = list(string)
  default     = null
}

variable "treat_missing_data" {
  description = "Sets how the alarm handles missing data points. Valid options are: missing | ignore | breaching | notBreaching"
  type        = string
  default     = "missing"
}

variable "unit" {
  description = "The unit for the alarm's associated metric"
  type        = string
  default     = null
}

############################
# Cloudwatch Logs variables
############################

variable "create_log_group" {
  description = "Whether to create the Cloudwatch log group"
  type        = bool
  default     = false
}

variable "create_log_subscription_filter" {
  description = "Whether to create a Cloudwatch log subscription filter"
  type        = bool
  default     = false
}

variable "cloudwatch_log_group" {
  description = "List of Cloudwatch log group settings including: cloudwatch_retention, cloudwatch_stream_prefix"
  type        = list(map(string))
  default     = []
}

variable "log_group_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "log_subscription_filter" {
  description = "List of Cloudwatch log subscription filter settings including: name, destination_arn, filter_pattern, log_group_name"
  type        = list(map(string))
  default     = []
}

############################
# Cloudwatch Event variables
############################

variable "create_event_rule" {
  description = "Whether to create the Cloudwatch event rule"
  type        = bool
  default     = false
}

variable "cloudwatch_event_rule" {
  description = <<EOF
  List of Cloudwatch event rule settings including: name, description, event_bus_name, event_pattern,
  is_enabled, role_arn, schedule_expression
  EOF
  type        = list(map(string))
  default     = []
}

variable "create_event_target" {
  description = "Whether to create the Cloudwatch event target"
  type        = bool
  default     = false
}

variable "cloudwatch_event_rule_name" {
  description = "The name of the cloudwatch event rule"
  type        = string
  default     = ""
}

variable "cloudwatch_event_target_arn" {
  description = "The ARN associated of the target"
  type        = string
  default     = ""
}

variable "cloudwatch_event_target_id" {
  description = "The unique target assignment ID. If missing, will generate a random, unique id"
  type        = string
  default     = ""
}

variable "event_rule_tags" {
  description = "A map of tags to add to event rules"
  type        = map(string)
  default     = {}
}
