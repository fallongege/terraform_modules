## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### actions\_enabled

Description: Indicates whether or not actions should be executed during any changes to the alarm's state

Type: `bool`

Default: `true`

### alarm\_tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

### cloudwatch\_alarm

Description:   List of Cloudwatch alarm settings including: alarm\_identifier, alarm\_actions, comparison\_operator, evaluation\_periods,  
  metric\_name, namespace, period, statistic, threshold

Type:

```hcl
list(object({
    alarm_identifier    = any
    alarm_actions       = any
    comparison_operator = any
    evaluation_periods  = any
    metric_name         = any
    namespace           = any
    period              = any
    statistic           = any
    threshold           = any
  }))
```

Default:

```json
[
  {
    "alarm_actions": "",
    "alarm_identifier": "",
    "comparison_operator": "",
    "evaluation_periods": "",
    "metric_name": "",
    "namespace": "",
    "period": "",
    "statistic": "",
    "threshold": ""
  }
]
```

### cloudwatch\_log\_group

Description: List of Cloudwatch log group settings including: cloudwatch\_retention, cloudwatch\_stream\_prefix

Type: `list(map(string))`

Default: `[]`

### create\_log\_group

Description: Whether to create the Cloudwatch log group

Type: `bool`

Default: `false`

### create\_log\_subscription\_filter

Description: Whether to create a Cloudwatch log subscription filter

Type: `bool`

Default: `false`

### create\_metric\_alarm

Description: Whether to create the Cloudwatch metric alarm

Type: `bool`

Default: `false`

### datapoints\_to\_alarm

Description: The number of datapoints that must be breaching to trigger the alarm

Type: `number`

Default: `null`

### dimensions

Description: The dimensions for the alarm's associated metric

Type: `any`

Default: `null`

### evaluate\_low\_sample\_count\_percentiles

Description:   Used only for alarms based on percentiles. If you specify ignore, the alarm state will not change during periods with  
  too few data points to be statistically significant. If you specify evaluate or omit this parameter, the alarm will  
  always be evaluated and possibly change state no matter how many data points are available. The following values are  
  supported: ignore \| evaluate

Type: `string`

Default: `null`

### insufficient\_data\_actions

Description: The list of actions to execute when this alarm transitions into an INSUFFICIENT\_DATA state

Type: `list(string)`

Default: `null`

### log\_group\_tags

Description: A map of tags to add to all resources

Type: `map(string)`

Default: `{}`

### log\_subscription\_filter

Description: List of Cloudwatch log subscription filter settings including: name, destination\_arn, filter\_pattern, log\_group\_name

Type: `list(map(string))`

Default: `[]`

### ok\_actions

Description: The list of actions to execute when this alarm transitions into an OK state from any other state

Type: `list(string)`

Default: `null`

### service\_name

Description: The name of the service that is being monitored by the Cloudwatch alarm

Type: `string`

Default: `""`

### treat\_missing\_data

Description: Sets how the alarm handles missing data points. Valid options are: missing \| ignore \| breaching \| notBreaching

Type: `string`

Default: `"missing"`

### unit

Description: The unit for the alarm's associated metric

Type: `string`

Default: `null`

## Outputs

The following outputs are exported:

### cloudwatch\_log\_group\_arn

Description: The ARN of the Cloudwatch log group

### cloudwatch\_log\_group\_id

Description: The ID of the Cloudwatch metric alarm

### cloudwatch\_log\_group\_name

Description: The Name of the Cloudwatch metric alarm

### cloudwatch\_log\_group\_prefix

Description: The Prefix of the Cloudwatch metric alarm

### cloudwatch\_metric\_alarm\_arn

Description: The ARN of the Cloudwatch metric alarm

### cloudwatch\_metric\_alarm\_id

Description: The ID of the Cloudwatch metric alarm

