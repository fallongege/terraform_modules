output "lambda_function_arn" {
  description = "The ARN of the Lambda Function"
  value       = element(concat(aws_lambda_function.lambda.*.arn, [""]), 0)
}

output "lambda_function_invoke_arn" {
  description = "The Invoke ARN of the Lambda Function"
  value       = element(concat(aws_lambda_function.lambda.*.invoke_arn, [""]), 0)
}

output "lambda_function_name" {
  description = "The name of the Lambda Function"
  value       = element(concat(aws_lambda_function.lambda.*.function_name, [""]), 0)
}
