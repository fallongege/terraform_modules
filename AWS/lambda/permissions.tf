resource "aws_lambda_permission" "lambda-permission" {
  count = var.create_lambda_permission ? length(var.lambda_permission) : 0

  action        = lookup(var.lambda_permission[count.index], "action")
  function_name = lookup(var.lambda_permission[count.index], "function_name")
  principal     = lookup(var.lambda_permission[count.index], "principal")
  source_arn    = lookup(var.lambda_permission[count.index], "source_arn")
  statement_id  = lookup(var.lambda_permission[count.index], "statement_id", null)
}
