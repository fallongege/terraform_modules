## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

No required input.

## Optional Inputs

The following input variables are optional (have default values):

### create\_lambda\_permission

Description: Whether to create a Lambda permissions

Type: `bool`

Default: `false`

### lambda\_permission

Description: List of lambda permission settings including: action, function\_name, principal, source\_arn, statement\_id

Type: `list(map(string))`

Default: `[]`

## Outputs

No output.

