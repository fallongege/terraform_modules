module "lambda-permission" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/lambda/?ref=main"

  # Lambda Permission arguments
  create_lambda_permission = true
  lambda_permission = [
    {
      action        = "lambda:InvokeFunction"
      function_name = "arn:aws:lambda:us-west-2:123456789:function:some-lambda-function"
      principal     = "logs.${var.aws_region}.amazonaws.com"
      source_arn    = module.cloudwatch-logs.cloudwatch_log_group_arn
    }
  ]
}
