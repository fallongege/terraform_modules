## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### associate\_alb

Description: Whether to associate an ALB to the WAF

Type: `any`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

### waf\_rule\_name

Description: The name of the WAF rule. This name can't contain any dashes or underscores

Type: `any`

### waf\_rule\_type

Description:   The type of prediate in a rule. Valid values are: IPMatch \| ByteMatch \| SqlInjectionMatch \| GeoMatch \| SizeConstraint \| XssMatch \| RegexMatch

Type: `any`

### waf\_web\_acl\_name

Description: The name of the WAF web ACL. This name can't contain any dashes or underscores.

Type: `any`

## Optional Inputs

The following input variables are optional (have default values):

### alb\_arn

Description: The arn of the ALB that the WAF is attached to

Type: `string`

Default: `""`

### byte\_match\_set

Description:   The byte match set. Defines the conditions of the byte match set. Required parameters are:  
  field\_to\_match\_data - Only required when the value of the variable: field\_to\_match\_type is HEADER or SINGLE\_QUERY\_ARG  
  field\_to\_match\_type - The part of the web request that you want AWS WAF to search for a specified string.  
                        Valid values are:  
                        URI \| QUERY\_STRING \| HEADER \| METHOD \| BODY \| SINGLE\_QUERY\_ARG \| ALL\_QUERY\_ARGS  
  positional\_constraint - The part of the web request that you want WAF to match. Valid values are:  
                          EXACTLY \| STARTS\_WITH \| ENDS\_WITH \| CONTAINS \| CONTAINS\_WORD  
  target\_string       - The maximum length of the value is 50 bytes  
  text\_transformation - The formatting way for web request

Type: `list(map(string))`

Default: `[]`

### create\_waf

Description: Whether to create WAF web ACL

Type: `bool`

Default: `true`

## Outputs

No output.

