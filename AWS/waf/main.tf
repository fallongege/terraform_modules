resource "aws_wafregional_byte_match_set" "byte_set" {
  count = var.create_waf ? length(var.byte_match_set) : 0

  name = "tf-${var.business_unit}-${var.environment}-string-match-${count.index}"

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 0)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 1)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 2)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 3)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 4)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 5)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 6)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 7)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 8)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }

  byte_match_tuples {
    positional_constraint = lookup(var.byte_match_set[count.index], "positional_constraint")
    target_string         = element(split(",", lookup(var.byte_match_set[count.index], "target_string")), 9)
    text_transformation   = lookup(var.byte_match_set[count.index], "text_transformation")

    field_to_match {
      data = lookup(var.byte_match_set[count.index], "field_to_match_data")
      type = lookup(var.byte_match_set[count.index], "field_to_match_type")
    }
  }
}

resource "aws_wafregional_rule" "waf_rule" {
  count = var.create_waf ? length(var.byte_match_set) : 0

  name = "Tf${var.business_unit}${var.environment}${var.waf_rule_name}Rule${count.index}"

  metric_name = "Tf${var.business_unit}${var.environment}${var.waf_rule_name}Rule${count.index}"

  predicate {
    type    = var.waf_rule_type
    data_id = aws_wafregional_byte_match_set.byte_set[count.index].id
    negated = false
  }
}

resource "aws_wafregional_web_acl" "waf_acl" {
  count = var.create_waf ? 1 : 0

  name = "Tf${var.business_unit}${var.environment}${var.waf_web_acl_name}"

  metric_name = "Tf${var.business_unit}${var.environment}${var.waf_web_acl_name}"

  default_action {
    type = "ALLOW"
  }

  dynamic "rule" {
    for_each = aws_wafregional_rule.waf_rule.*.id

    content {
      priority = 1 + rule.key
      rule_id  = rule.value
      type     = "REGULAR"

      action {
        type = "BLOCK"
      }
    }
  }
}

resource "aws_wafregional_web_acl_association" "waf_alb" {
  count = var.associate_alb ? 1 : 0

  resource_arn = var.alb_arn
  web_acl_id   = aws_wafregional_web_acl.waf_acl[0].id
}
