##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

###############
# WAF variables
###############

variable "create_waf" {
  description = "Whether to create WAF web ACL"
  default     = true
}

variable "alb_arn" {
  description = "The arn of the ALB that the WAF is attached to"
  default     = ""
}

variable "associate_alb" {
  description = "Whether to associate an ALB to the WAF"
}

variable "byte_match_set" {
  description = <<EOF
  The byte match set. Defines the conditions of the byte match set. Required parameters are:
  field_to_match_data - Only required when the value of the variable: field_to_match_type is HEADER or SINGLE_QUERY_ARG
  field_to_match_type - The part of the web request that you want AWS WAF to search for a specified string.
                        Valid values are:
                        URI | QUERY_STRING | HEADER | METHOD | BODY | SINGLE_QUERY_ARG | ALL_QUERY_ARGS
  positional_constraint - The part of the web request that you want WAF to match. Valid values are:
                          EXACTLY | STARTS_WITH | ENDS_WITH | CONTAINS | CONTAINS_WORD
  target_string       - The maximum length of the value is 50 bytes
  text_transformation - The formatting way for web request
  EOF
  type        = list(map(string))
  default     = []
}

variable "waf_rule_name" {
  description = "The name of the WAF rule. This name can't contain any dashes or underscores"
}

variable "waf_rule_type" {
  description = <<EOF
  The type of prediate in a rule. Valid values are: IPMatch | ByteMatch | SqlInjectionMatch | GeoMatch | SizeConstraint | XssMatch | RegexMatch
  EOF
}

variable "waf_web_acl_name" {
  description = "The name of the WAF web ACL. This name can't contain any dashes or underscores."
}
