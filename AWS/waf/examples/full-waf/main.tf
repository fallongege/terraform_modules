module "waf" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/waf/?ref=main"

  business_unit = "pmc"
  environment   = "stg"

  alb_arn       = module.alb.lb_arn
  associate_alb = true
  byte_match_set = [
    {
      field_to_match_data   = "user-agent"
      field_to_match_type   = "HEADER"
      positional_constraint = "CONTAINS"
      target_string         = "Scrapy,Riverbot,test3"
      text_transformation   = "NONE"
    },
    {
      field_to_match_data   = "user-agent"
      field_to_match_type   = "HEADER"
      positional_constraint = "CONTAINS"
      target_string         = "test"
      text_transformation   = "NONE"
    },
  ]
  waf_rule_name    = "UserAgent"
  waf_rule_type    = "ByteMatch"
  waf_web_acl_name = "TEST"
}
