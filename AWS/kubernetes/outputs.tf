output "cluster_id" {
  description = "The name/id of the EKS cluster. Will block on cluster creation until the cluster is really ready"
  value       = aws_eks_cluster.main.id
  depends_on  = [aws_eks_cluster.main]
}

output "nithin_role" {
  value = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/nithin"
}

output "eduardo_role" {
  value = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/eduardo"
}

output "nodes_role" {
  value = aws_iam_role.eks-nodes-role.arn
}

output "kubectl_role" {
  value = aws_iam_role.kubectl-role.arn
}
