resource "aws_eks_node_group" "node_group" {
  count           = var.node_groups
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "${var.cluster-name}-nodes-${count.index}"
  node_role_arn   = aws_iam_role.eks-nodes-role.arn
  subnet_ids      = [element(var.subnet_ids, count.index)]
  instance_types  = [var.instance_type]

  scaling_config {
    desired_size = var.min_nodes
    max_size     = var.max_nodes
    min_size     = var.min_nodes
  }

  tags = {
    Name        = "${var.cluster-name}-node-${count.index}"
    Brand       = "PMC"
    Department  = "PEP"
    Team        = "DevOps"
    Managed     = "Terraform"
    Application = "Kubernetes"
    cost-center = "shared"
    Environment = var.environment
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEC2ContainerRegistryReadOnly
  ]

  lifecycle {
    # We need to ignore the desired_size because that's changing by the Kubernetes autoscaler
    ignore_changes = [scaling_config.0.desired_size]
  }
}
