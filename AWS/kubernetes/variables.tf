variable "cluster-name" {
  type = string
}
variable "vpc_id" {
  type = string
}
variable "subnet_ids" {
  type = list(string)
}
variable "node_groups" {
  type    = number
  default = 2
}
variable "min_nodes" {
  type    = number
  default = 1
}
variable "max_nodes" {
  type    = number
  default = 3
}
variable "environment" {
  type = string
}

variable "business_unit" {
  type = string
}
variable "instance_type" {
  default = "t3.medium"
}
