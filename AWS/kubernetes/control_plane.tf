## Security group
resource "aws_security_group" "cluster" {
  name        = "${var.cluster-name}-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.business_unit}-${var.environment}"
    Brand       = "PMC"
    Department  = "PEP"
    Team        = "DevOps"
    Managed     = "Terraform"
    Application = "Kubernetes"
    cost-center = "shared"
    Environment = var.environment
  }
}

## EKS Cluster
resource "aws_eks_cluster" "main" {
  name     = var.cluster-name
  role_arn = aws_iam_role.eks-role.arn
  version  = "1.17"

  vpc_config {
    security_group_ids = [aws_security_group.cluster.id]
    subnet_ids         = var.subnet_ids
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks-main-role-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-main-role-AmazonEKSServicePolicy
  ]
}
