# Allocate Elastic IP to connect to the loadbalancer in Kubernetes
resource "aws_eip" "eks-lb" {
  vpc = true
  tags = {
    Name        = "IP for ${var.environment} Kubernetes loadbalancer"
    Brand       = "PMC"
    Department  = "PEP"
    Team        = "DevOps"
    Managed     = "Terraform"
    Application = "Kubernetes"
    cost-center = "shared"
    Environment = var.environment
  }
}
