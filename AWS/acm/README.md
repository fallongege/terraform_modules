### Module Limitations
This module has been written in a way that makes it usable for our pmc use case where our hosted zones are in a different
aws account (pmc-it) and our resources are deployed in another account. This module requires that the IAM user/role that
you are using to deploy has the ability to also assume a role/user in the pmc-it account in order to create the ACM 
validation DNS records. 

## Requirements

The following requirements are needed by this module:

- aws (2.70.0)

- aws (2.70.0)

## Providers

The following providers are used by this module:

- aws.default (2.70.0 2.70.0)

- aws.pmc-it (2.70.0 2.70.0)

## Required Inputs

The following input variables are required:

### aws\_account\_id

Description: The aws account id that is being deployed into

Type: `string`

### aws\_region

Description: The aws region that is being deployed into

Type: `string`

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### certificate\_transparency\_logging\_preference

Description: Specifies whether certificate details should be added to a certificate transparency log

Type: `bool`

Default: `true`

### create\_certificate

Description: Whether to create ACM certificate

Type: `bool`

Default: `true`

### dns\_ttl

Description: The TTL of DNS recursive resolvers to cache information about this record.

Type: `number`

Default: `60`

### domain\_name

Description: A domain name for which the certificate should be issued

Type: `string`

Default: `""`

### subject\_alternative\_names

Description: A list of domains that should be SANs in the issued certificate

Type: `list(string)`

Default: `[]`

### tags

Description: A mapping of tags to assign to the resource

Type: `map(string)`

Default: `{}`

### validate\_certificate

Description: Whether to validate certificate by creating Route53 record

Type: `bool`

Default: `true`

### validation\_allow\_overwrite\_records

Description: Whether to allow overwrite of Route53 records

Type: `bool`

Default: `true`

### validation\_method

Description: Which method to use for validation. DNS or EMAIL are valid, NONE can be used for certificates that were imported into ACM and then into Terraform.

Type: `string`

Default: `"DNS"`

### wait\_for\_validation

Description: Whether to wait for the validation to complete

Type: `bool`

Default: `true`

### zone\_id

Description: The ID of the hosted zone to contain this record.

Type: `string`

Default: `""`

## Outputs

The following outputs are exported:

### acm\_certificate\_arn

Description: The ARN of the ACM certificate

### acm\_certificate\_domain\_validation\_options

Description: A list of attributes to feed into other resources to complete certificate validation. Can have more than one element, e.g. if SANs are defined. Only set if DNS-validation was used.

### acm\_certificate\_validation\_emails

Description: A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used.

### distinct\_domain\_names

Description: List of distinct domains names used for the validation.

### validation\_domains

Description: List of distinct domain validation options. This is useful if subject alternative names contain wildcards.

### validation\_route53\_record\_fqdns

Description: List of FQDNs built using the zone domain and name.
