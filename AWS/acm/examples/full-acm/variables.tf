variable "aws_account_id" {
  default = "925863785861"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "business_unit" {
  default = "pmc"
}

variable "environment" {
  default = "stg"
}

variable "pmc_domain_name" {
  description = "A map of the pmc domain names"
  type        = map(string)

  default = {
    stg  = "pmcdev.io"
    prod = "pmc.com"
  }
}
