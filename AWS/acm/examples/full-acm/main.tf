locals {
  pmc_domain_name = lookup(var.pmc_domain_name, var.environment)
  pmc_zone_id = {
    stg  = "Z1F912Z1W3JCBZ" // pmcdev.io
    prod = "Z2UNEV68ZLOFA6" // pmc.com
  }
}

module "acm" {
  source = "../_modules/AWS/acm"

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  business_unit  = var.business_unit
  environment    = var.environment

  domain_name = local.pmc_domain_name
  zone_id     = lookup(local.pmc_zone_id, var.environment)

  subject_alternative_names = [
    "*.${local.pmc_domain_name}",
  ]

  wait_for_validation = true

  tags = {
    Name        = local.pmc_domain_name
    Application = "tyk"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
