provider "aws" {
  region      = var.aws_region
  max_retries = "50"
  alias = "prod"
}

provider "aws" {
  region      = var.aws_region
  max_retries = "50"
  alias = "stg"
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
