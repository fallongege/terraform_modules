## Requirements

No requirements.

## Providers

The following providers are used by this module:

- aws

## Required Inputs

The following input variables are required:

### business\_unit

Description: The name of the business unit. Valid options are sk \| pmc

Type: `string`

### environment

Description: The name of the environment that the resources belong to

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### associate\_with\_private\_ip

Description: A user specified primary or secondary private IP address to associate with the Elastic IP address. If no private IP address is specified, the Elastic IP address is associated with the primary private IP address.

Type: `list(string)`

Default: `[]`

### customer\_owned\_ipv4\_pool

Description: The ID of a customer-owned address pool. For more on customer owned IP addressed

Type: `list(string)`

Default: `[]`

### eip\_count

Description: The number of Elastic IP's to create

Type: `number`

Default: `1`

### eip\_name

Description: The name of the EIP

Type: `list(string)`

Default: `[]`

### instance\_id

Description: The EC2 instance ID

Type: `list(string)`

Default: `[]`

### network\_interface

Description: The Network interface ID to associate with

Type: `list(string)`

Default: `[]`

### public\_ipv4\_pool

Description: The EC2 IPv4 address pool identifier or amazon. This option is only available for VPC EIPs.

Type: `list(string)`

Default: `[]`

### tags

Description: A map of tags to assign to the resource. Tags can only be applied to EIPs in a VPC.

Type: `map(string)`

Default: `{}`

### vpc

Description: Boolean if the EIP is in a VPC or not.

Type: `bool`

Default: `true`

## Outputs

The following outputs are exported:

### eip\_allocation\_id

Description: n/a

### eip\_private\_dns

Description: n/a

### eip\_private\_ip

Description: n/a

### eip\_public\_dns

Description: n/a

### eip\_public\_ip

Description: n/a

