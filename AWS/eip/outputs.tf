output "eip_allocation_id" {
  value = aws_eip.eip.*.id
}

output "eip_private_ip" {
  value = aws_eip.eip.*.private_ip
}

output "eip_private_dns" {
  value = aws_eip.eip.*.private_dns
}

output "eip_public_ip" {
  value = aws_eip.eip.*.public_ip
}

output "eip_public_dns" {
  value = aws_eip.eip.*.public_dns
}
