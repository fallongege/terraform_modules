module "eip" {
  source = "git::git@github.com:penske-media-corp/terraform.git//_modules/AWS/eip/?ref=main"

  business_unit = var.business_unit
  environment   = var.environment

  eip_count = 1
  eip_name  = ["eip"]
  vpc       = true
  tags = {
    Application = "eip"
    Department  = "pep-engineering"
    Environment = "${var.environment}"
    Managed     = "Terraform"
    Team        = "pep-devops"
  }
}
