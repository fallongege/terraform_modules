resource "aws_eip" "eip" {
  count = var.eip_count

  associate_with_private_ip = element(concat(var.associate_with_private_ip, [""]), count.index)
  customer_owned_ipv4_pool  = element(concat(var.customer_owned_ipv4_pool, [""]), count.index)
  instance                  = element(concat(var.instance_id, [""]), count.index)
  network_interface         = element(concat(var.network_interface, [""]), count.index)
  public_ipv4_pool          = element(concat(var.public_ipv4_pool, [""]), count.index)
  vpc                       = var.vpc

  tags = merge(
    {
      "Name" = "${local.name_prefix}-${var.eip_name}-eip-${count.index}"
    },
    var.tags
  )
}
