##################
# Global variables
##################

variable "business_unit" {
  description = "The name of the business unit. Valid options are sk | pmc"
  type        = string
}

variable "environment" {
  description = "The name of the environment that the resources belong to"
  type        = string
}

###############
# EIP variables
###############

variable "eip_count" {
  description = "The number of Elastic IP's to create"
  type        = number
  default     = 1
}

variable "associate_with_private_ip" {
  description = "A user specified primary or secondary private IP address to associate with the Elastic IP address. If no private IP address is specified, the Elastic IP address is associated with the primary private IP address."
  type        = list(string)
  default     = []
}

variable "customer_owned_ipv4_pool" {
  description = "The ID of a customer-owned address pool. For more on customer owned IP addressed"
  type        = list(string)
  default     = []
}

variable "eip_name" {
  description = "The name of the EIP"
  type        = string
}

variable "instance_id" {
  description = "The EC2 instance ID"
  type        = list(string)
  default     = []
}

variable "network_interface" {
  description = "The Network interface ID to associate with"
  type        = list(string)
  default     = []
}

variable "public_ipv4_pool" {
  description = "The EC2 IPv4 address pool identifier or amazon. This option is only available for VPC EIPs."
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "A map of tags to assign to the resource. Tags can only be applied to EIPs in a VPC."
  type        = map(string)
  default     = {}
}

variable "vpc" {
  description = "Boolean if the EIP is in a VPC or not."
  type        = bool
  default     = true
}
