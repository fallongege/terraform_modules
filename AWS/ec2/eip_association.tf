resource "aws_eip_association" "eip_assoc" {
  count = var.create_eip_association ? 1 : 0

  allocation_id        = var.eip_allocation_id
  instance_id          = var.instance_id
  network_interface_id = var.network_interface_id
}
