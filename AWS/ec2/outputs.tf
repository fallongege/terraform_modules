output "instance_arn" {
  description = "The ARN of the instance"
  value       = try(aws_instance.instance[0].arn, "")
}

output "instance_id" {
  description = "The ID of the instance"
  value       = try(aws_instance.instance[0].id, "")
}

output "instance_network_interface" {
  description = "The Network Interface of the instance"
  value       = try(aws_instance.instance[0].network_interface, "")
}

output "instance_private_ip" {
  description = "The Private IP of the instance"
  value       = try(aws_instance.instance[0].private_ip, "")
}

output "interface_private_ip" {
  description = "The Private IP of the interface"
  value       = try(aws_network_interface.interface.*.private_ip, "")
}
