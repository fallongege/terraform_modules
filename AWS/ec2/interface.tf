resource "aws_network_interface" "interface" {
  count = var.create_interface ? length(var.network_interfaces) : 0

  description       = lookup(var.network_interfaces[count.index], "description", null)
  private_ips       = lookup(var.network_interfaces[count.index], "private_ips")
  security_groups   = lookup(var.network_interfaces[count.index], "security_groups")
  source_dest_check = lookup(var.network_interfaces[count.index], "source_dest_check", null)
  subnet_id         = lookup(var.network_interfaces[count.index], "subnet_id")

  attachment {
    device_index = lookup(var.network_interfaces[count.index], "device_index", 1)
    instance     = lookup(var.network_interfaces[count.index], "instance")
  }

  tags = merge({ "Name" = lookup(var.network_interfaces[count.index], "description", null) }, var.tags)
}
