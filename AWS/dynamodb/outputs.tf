output "dynamodb_table_arn" {
  description = "dynamodb table arn"
  value       = aws_dynamodb_table.encrypted_table.*.arn
}

output "dynamodb_table_name" {
  description = "dynamodb table name"
  value       = aws_dynamodb_table.encrypted_table.*.name
}

output "dynamodb_table_id" {
  description = "dynamodb table id"
  value       = aws_dynamodb_table.encrypted_table.*.id
}
