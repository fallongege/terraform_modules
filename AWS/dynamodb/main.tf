resource "aws_dynamodb_table" "encrypted_table" {
  count = var.dynamodb_enabled && var.enable_server_side_encryption ? 1 : 0

  name           = var.dynamodb_table_name
  billing_mode   = var.billing_mode
  hash_key       = var.hash_key
  read_capacity  = var.billing_mode == "PROVISIONED" ? var.read_capacity : null
  write_capacity = var.billing_mode == "PROVISIONED" ? var.write_capacity : null

  dynamic "server_side_encryption" {
    for_each = var.server_side_encryption

    content {
      enabled     = lookup(server_side_encryption.value, "enabled", true)
      kms_key_arn = lookup(server_side_encryption.value, "kms_key_arn", "alias/aws/dynamodb")
    }
  }

  point_in_time_recovery {
    enabled = var.enable_point_in_time_recovery
  }

  dynamic "attribute" {
    for_each = var.attribute

    content {
      name = lookup(attribute.value, "name", null)
      type = lookup(attribute.value, "type", null)
    }
  }

  tags = merge(
    {
      "Name" = var.dynamodb_table_name
    },
    var.tags
  )
}
