variable "dynamodb_enabled" {
  description = "Whether to create a dynamodb table"
  type        = bool
  default     = true
}

variable "attribute" {
  description = "List of nested attribute definitions. Only required for hash_key and range_key attributes"
  type = list(object({
    name = string
    type = string
  }))
}

variable "billing_mode" {
  description = "Controls how you are charged for read and write throughput and how you manage capacity. The valid values are PROVISIONED and PAY_PER_REQUEST"
  type        = string
  default     = "PROVISIONED"
}

variable "dynamodb_table_name" {
  description = "The name of the dynamodb table"
  type        = string
}

variable "enable_point_in_time_recovery" {
  description = "Whether to enable dynamodb point-in-time recovery"
  type        = bool
  default     = true
}

variable "enable_server_side_encryption" {
  description = "Whether to enable server side encryption on the dynamodb table"
  type        = bool
  default     = true
}

variable "hash_key" {
  description = "The attribute to use as the hash (partition) key. Must also be defined as an attribute"
  type        = string
}

variable "read_capacity" {
  description = "The number of read units for this table. If the billing_mode is PROVISIONED, this field is required."
  type        = number
  default     = 5
}

variable "server_side_encryption" {
  description = "Encryption at rest options. AWS DynamoDB tables are automatically encrypted at rest with an AWS owned Customer Master Key if this argument isn't specified."
  type = list(object({
    enabled     = bool
    kms_key_arn = string
  }))
  default = [
    {
      enabled     = true
      kms_key_arn = ""
    }
  ]
}

variable "tags" {
  description = "A map of tags to add to the dynamob resources"
  type        = map(string)
  default     = {}
}

variable "write_capacity" {
  description = "The number of write units for this table. If the billing_mode is PROVISIONED, this field is required."
  type        = number
  default     = 5
}
